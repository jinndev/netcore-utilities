﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;

namespace JinnDev.Utilities.Utilities.UnitTests
{
    [TestClass]
    public class HtmlTests
    {
        [TestMethod]
        public void ParseHTML()
        {
            var longHTML = @"<!DOCTYPE html>
<html class=""client-nojs"" lang=""en"" dir=""ltr"">
<head>
<meta charset=""UTF-8""/>
<title>Template:Blocks/content – Official Minecraft Wiki</title>
<script>document.documentElement.className = document.documentElement.className.replace( /(^|\s)client-nojs(\s|$)/, ""$1client-js$2"" );</script>
<script>(window.RLQ=window.RLQ||[]).push(function(){mw.config.set({""wgCanonicalNamespace"":""Template"",""wgCanonicalSpecialPageName"":false,""wgNamespaceNumber"":10,""wgPageName"":""Template:Blocks/content"",""wgTitle"":""Blocks/content"",""wgCurRevisionId"":1683616,""wgRevisionId"":1683616,""wgArticleId"":2475,""wgIsArticle"":true,""wgIsRedirect"":false,""wgAction"":""view"",""wgUserName"":null,""wgUserGroups"":[""*""],""wgCategories"":[""Pages using DynamicPageList dplreplace parser function"",""Pages with missing sprites"",""Ajax loaded pages""],""wgBreakFrames"":true,""wgPageContentLanguage"":""en"",""wgPageContentModel"":""wikitext"",""wgSeparatorTransformTable"":["""",""""],""wgDigitTransformTable"":["""",""""],""wgDefaultDateFormat"":""dmy"",""wgMonthNames"":["""",""January"",""February"",""March"",""April"",""May"",""June"",""July"",""August"",""September"",""October"",""November"",""December""],""wgMonthNamesShort"":["""",""Jan"",""Feb"",""Mar"",""Apr"",""May"",""Jun"",""Jul"",""Aug"",""Sep"",""Oct"",""Nov"",""Dec""],""wgRelevantPageName"":""Template:Blocks/content"",""wgRelevantArticleId"":2475,""wgRequestId"":""6d6808885b4893e385258973"",""wgCSPNonce"":false,""wgIsProbablyEditable"":false,""wgRelevantPageIsProbablyEditable"":false,""wgRestrictionEdit"":[""autoconfirmed""],""wgRestrictionMove"":[""autoconfirmed""],""dsSiteKey"":""aa746ead3a810cdf7dd11b4779c53009"",""wgMFDisplayWikibaseDescriptions"":{""search"":false,""nearby"":false,""watchlist"":false,""tagline"":false},""wgCollapsibleVectorEnabledModules"":{""collapsiblenav"":true,""experiments"":true},""wgVisualEditor"":{""pageLanguageCode"":""en"",""pageLanguageDir"":""ltr"",""pageVariantFallbacks"":""en"",""usePageImages"":true,""usePageDescriptions"":false},""wgVisualEditorToolbarScrollOffset"":0,""wgVisualEditorUnsupportedEditParams"":[""undo"",""undoafter"",""veswitched""],""wgEditSubmitButtonLabelPublish"":false});mw.loader.state({""ext.gadget.dungeonsWiki"":""ready"",""ext.gadget.earthWiki"":""ready"",""ext.gadget.site-styles"":""ready"",""ext.gadget.sound-styles"":""ready"",""site.styles"":""ready"",""noscript"":""ready"",""user.styles"":""ready"",""user"":""ready"",""user.options"":""ready"",""user.tokens"":""loading"",""mediawiki.legacy.shared"":""ready"",""mediawiki.legacy.commonPrint"":""ready"",""ext.siteGlobals.styles"":""ready"",""ext.visualEditor.desktopArticleTarget.noscript"":""ready"",""skins.vector.styles.responsive"":""ready"",""ext.social.styles"":""ready"",""mediawiki.skinning.interface"":""ready"",""skins.vector.styles"":""ready"",""skins.z.hydra.light.styles"":""ready"",""skins.hydra.googlefont.styles"":""ready"",""skins.hydra.netbar"":""ready"",""skins.hydra.footer"":""ready"",""skins.hydra.advertisements.styles"":""ready""});mw.loader.implement(""user.tokens@0tffind"",function($,jQuery,require,module){/*@nomin*/mw.user.tokens.set({""editToken"":""+\\"",""patrolToken"":""+\\"",""watchToken"":""+\\"",""csrfToken"":""+\\""});
});RLPAGEMODULES=[""ext.helios.logout.scripts"",""site"",""mediawiki.page.startup"",""mediawiki.page.ready"",""mediawiki.searchSuggest"",""ext.track.scripts"",""ext.crusher.tables"",""ext.gadget.refTooltip"",""ext.gadget.site"",""ext.gadget.sound"",""ext.gadget.spriteEditLoader"",""ext.gadget.purge"",""ext.gadget.protectionLocks"",""ext.gadget.ImageForeignUseCheck"",""ext.siteGlobals.scripts"",""ext.collapsiblevector.collapsibleNav"",""ext.visualEditor.desktopArticleTarget.init"",""ext.visualEditor.targetLoader"",""skins.vector.js"",""skins.hydra.advertisements.js"",""skins.hydra.footer.js"",""ext.social.scripts""];mw.loader.load(RLPAGEMODULES);});</script>
<link rel=""stylesheet"" href=""/load.php?lang=en&amp;modules=ext.siteGlobals.styles%7Cext.social.styles%7Cext.visualEditor.desktopArticleTarget.noscript%7Cmediawiki.legacy.commonPrint%2Cshared%7Cmediawiki.skinning.interface%7Cskins.hydra.advertisements.styles%7Cskins.hydra.footer%2Cnetbar%7Cskins.hydra.googlefont.styles%7Cskins.vector.styles%7Cskins.vector.styles.responsive%7Cskins.z.hydra.light.styles&amp;only=styles&amp;skin=hydra""/>
<script async="""" src=""/load.php?lang=en&amp;modules=startup&amp;only=scripts&amp;skin=hydra""></script>
<meta name=""ResourceLoaderDynamicStyles"" content=""""/>
<link rel=""stylesheet"" href=""/load.php?lang=en&amp;modules=ext.gadget.dungeonsWiki%2CearthWiki%2Csite-styles%2Csound-styles&amp;only=styles&amp;skin=hydra""/>
<link rel=""stylesheet"" href=""/load.php?lang=en&amp;modules=site.styles&amp;only=styles&amp;skin=hydra""/>
<noscript><link rel=""stylesheet"" href=""/load.php?lang=en&amp;modules=noscript&amp;only=styles&amp;skin=hydra""/></noscript>
<meta name=""generator"" content=""MediaWiki 1.33.3""/>
<meta name=""description""/>
<meta name=""keywords"" content=""""/>
<meta property=""og:title"" content=""Template:Blocks/content""/>
<meta property=""og:type"" content=""website""/>
<meta property=""og:image"" content=""https://minecraft.gamepedia.com/media/minecraft.gamepedia.com/b/bc/Wiki.png?version=8dcd2d0ecba9fbe6ecb884e9d0c11e3f""/>
<meta property=""og:url"" content=""https://minecraft.gamepedia.com/Template:Blocks/content""/>
<meta property=""og:site_name"" content=""Minecraft Wiki""/>
<meta name=""viewport"" content=""width=device-width, initial-scale=1""/>
<link rel=""apple-touch-icon"" href=""""/>
<link rel=""shortcut icon"" href=""https://minecraft.gamepedia.com/media/minecraft.gamepedia.com/6/64/Favicon.ico?version=ed57d32751506dd14279b7164eb8e848&amp;version=ed57d32751506dd14279b7164eb8e848""/>
<link rel=""search"" type=""application/opensearchdescription+xml"" href=""/opensearch_desc.php"" title=""Minecraft Wiki (en)""/>
<link rel=""EditURI"" type=""application/rsd+xml"" href=""https://minecraft.gamepedia.com/api.php?action=rsd""/>
<link rel=""license"" href=""//creativecommons.org/licenses/by-nc-sa/3.0/""/>
<link rel=""alternate"" type=""application/atom+xml"" title=""Minecraft Wiki Atom feed"" href=""/index.php?title=Special:RecentChanges&amp;feed=atom""/>
<link rel=""canonical"" href=""https://minecraft.gamepedia.com/Template:Blocks/content""/>
<meta itemprop=""author"" content=""Gamepedia"" />
<!--[if lt IE 9]><script src=""/load.php?lang=en&amp;modules=html5shiv&amp;only=scripts&amp;skin=hydra&amp;sync=1""></script><![endif]-->
</head>
<body class=""mediawiki ltr sitedir-ltr mw-hide-empty-elt ns-10 ns-subject page-Template_Blocks_content rootpage-Template_Blocks skin-hydra action-view site-minecraft-gamepedia show-ads"">
<script type=""application/ld+json"">
{
    ""@context"": ""http://schema.org/"",
    ""@type"": ""Article"",
    ""name"": ""Template:Blocks/content"",
    ""headline"": ""Template:Blocks/content"",
    ""image"": {
        ""@type"": ""ImageObject"",
        ""url"": ""https://static.wikia.nocookie.net/minecraft_gamepedia/images/b/bc/Wiki.png/revision/latest?cb=20200503182357?version=8dcd2d0ecba9fbe6ecb884e9d0c11e3f"",
        ""width"": ""160"",
        ""height"": ""132""
    },
    ""author"": {
        ""@type"": ""Organization"",
        ""name"": ""Minecraft Wiki""
    },
    ""publisher"": {
        ""@type"": ""Organization"",
        ""name"": ""Gamepedia"",
        ""logo"": {
            ""@type"": ""ImageObject"",
            ""url"": ""https://minecraft.gamepedia.com/skins/Hydra/images/icons/gp-pub-logo.png""
        },
        ""sameAs"": [
            ""https://twitter.com/CurseGamepedia"", ""https://www.facebook.com/CurseGamepedia"", ""https://www.twitch.tv/gamepedia/videos""
        ]
    },
    ""potentialAction"": {
        ""@type"": ""SearchAction"",
        ""target"": ""https://minecraft.gamepedia.com/index.php?title=Special:Search&search={search_term}"",
        ""query-input"": ""required name=search_term""
    },
    ""datePublished"": ""2020-09-08T10:52:46Z"",
    ""dateModified"": ""2020-09-08T10:52:46Z"",
    ""mainEntityOfPage"": ""https://minecraft.gamepedia.com/Template:Blocks/content""
}
</script>
<script>
var version = 'v76.3.3';
var host = 'https://static.wikia.nocookie.net/fandom-ae-assets/platforms/' + version + '/gamepedia';
var jsScript = document.createElement('script');
var cssLink = document.createElement('link');
 
jsScript.id = 'ae3.bundle';
jsScript.src = host + '/main.bundle.js';
jsScript.async = true;
jsScript.type = 'text/javascript';
 
cssLink.id = 'ae3.styles';
cssLink.href = host + '/styles.css';
cssLink.type = 'text/css';
cssLink.rel = 'stylesheet';
 
document.head.appendChild(jsScript);
document.head.appendChild(cssLink);
 
if (Math.max(document.documentElement.clientHeight, window.innerHeight || 0) < 800) {
    document.addEventListener(""DOMContentLoaded"", function(event) {
        var element = document.getElementById(""cdm-zone-06"");
        if (element) {
            element.parentNode.removeChild(element);
        }
    });
}
 
// LEGACY PART
 
window.isMobileResponsive = false;
window.factorem = window.factorem || {};
window.factorem.slotSizes =  [[[728,90],[980,150],[980,250],[970,150],[970,250]],[[300,250],[300,600]],[[300,250]],[[728,90]],[],[[300,250]],[],[]];
factorem.moatFrequency=""1.0"";
factorem.sandboxPolicy=""mobile"";
</script>

<!-- Google Tag Manager K -->
<script type=""text/javascript"">(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-NR933TZ');</script>
<!-- End Google Tag Manager K -->

<script type=""text/javascript"">!function(){var t=window.SambaTV=window.SambaTV||[];if(!t.track){if(t.invoked)return void(window.console&&window.console.error&&window.console.error(""Samba Metrics snippet included twice.""));t.invoked=!0,t.methods=[""track"",""Impression"",""Purchase"",""Register"",""Click"",""Login"",""Register""],t.factory=function(e){return function(){var r=Array.prototype.slice.call(arguments);return r.unshift(e),t.push(r),t}};for(var e=0;e<t.methods.length;e++){var r=t.methods[e];t[r]=t.factory(r)}t.load=function(t){var e=document.createElement(""script"");e.type=""text/javascript"",e.async=!0,e.src=(""https:""===document.location.protocol?""https://"":""http://"")+""tag.mtrcs.samba.tv/v3/tag/""+t+""/sambaTag.js"";var r=document.getElementsByTagName(""script"")[0];r.parentNode.insertBefore(e,r)},t.attrs||(t.attrs={}),t.SNIPPET_VERSION=""1.0.1"",t.load(""wikia/fandom-gamepedia"")}}();</script>
<script>
window.wgAdDriverPagesWithoutAds = [
  ""Tutorials/Mob_farm""
];
</script>	<script type=""text/javascript"">
		(function(i, s, o, g, r, a, m) {
			i['GoogleAnalyticsObject'] = r;
			i[r] = i[r] || function() {
				(i[r].q = i[r].q || []).push(arguments)
			}

			, i[r].l = 1 * new Date();
			a = s.createElement(o),
				m = s.getElementsByTagName(o)[0];
			a.async = 1;
			a.src = g;
			m.parentNode.insertBefore(a, m)
		})(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');
		ga('create', 'UA-35871056-4', 'auto', 'tracker0', []);
		ga('create', 'UA-1045810-36', 'auto', 'tracker1', []);
		ga('tracker0.send', 'pageview');

		if (window.cdnprovider) {
			ga(
				'tracker1.send',
				'pageview',
				{
					'dimension1':  window.cdnprovider
				}
			);
		} else {
			ga('tracker1.send', 'pageview');
		}

	</script>

<div id=""netbar"">
	<div class=""netbar-flex"">
		<div class=""netbar-box left logo""><a href=""https://www.gamepedia.com"">Gamepedia</a></div>
				<div class=""netbar-box left""><a href=""https://support.gamepedia.com/"">Help</a></div>
												<div class=""netbar-box left officialwiki""><a href=""/index.php?title=Special:AllSites&amp;filter=official""><img src=""/skins/Hydra/images/netbar/official-wiki.svg"" width=""90""></a></div>
						<div class=""netbar-spacer"">&nbsp;</div>
						<div class=""netbar-box right""><a href=""https://fandomauth.gamepedia.com/signin?redirect=https%3A%2F%2Fminecraft.gamepedia.com%2FTemplate%3ABlocks%2Fcontent"" id=""login-link"" class=""aqua-link"">Sign In</a></div>
					<div class=""netbar-box right""><a href=""https://fandomauth.gamepedia.com/register?redirect=https%3A%2F%2Fminecraft.gamepedia.com%2FTemplate%3ABlocks%2Fcontent"" id=""register-link"" class=""aqua-link"">Register</a></div>
						</div>
</div>
	<div id=""global-wrapper"" class=""with-siderail"">
		<div id=""mw-page-base"" class=""noprint""></div>
		<div id=""mw-head-base"" class=""noprint""></div>
		<div id=""pageWrapper"">
			<div id=""content"" class=""mw-body"" role=""main"" itemprop=""articleBody"">
				<a id=""top""></a>
								<!-- ATF Leaderboard -->
								<div id=""atflb"">
					<div id='cdm-zone-01'></div>				</div>
								<!-- /ATF Leaderboard -->
				<div class=""mw-indicators mw-body-content"">
</div>
<h1 id=""firstHeading"" class=""firstHeading"" lang=""en"" itemprop=""name"">Template:Blocks/content</h1>				<div id=""bodyContent"" class=""mw-body-content"">
					<div id=""siteSub"" class=""noprint"">From Minecraft Wiki</div>					<div id=""contentSub""><span class=""subpages"">&lt; <a href=""/Template:Blocks"" title=""Template:Blocks"">Template:Blocks</a></span></div>
										<div id=""jump-to-nav"" class=""mw-jump"">
						Jump to:						<a href=""#mw-head"">navigation</a>, 						<a href=""#p-search"">search</a>
					</div>
					<div id=""mw-content-text"" lang=""en"" dir=""ltr"" class=""mw-content-ltr""><div class=""mw-parser-output""><table class=""navbox hlist collapsible"">
<tbody><tr>
<th class=""navbox-top"" colspan=""2""><span class=""navbox-title""><a href=""/Block"" title=""Block"">Blocks</a></span>
</th></tr>
<tr>
<th>Natural
</th>
<td>
<ul><li><a href=""/Bedrock"" title=""Bedrock""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-320px -816px""></span><span class=""sprite-text"">Bedrock</span></span></a></li>
<li><a href=""/Gravel"" title=""Gravel""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-208px -832px""></span><span class=""sprite-text"">Gravel</span></span></a></li>
<li><a href=""/Magma_Block"" title=""Magma Block""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-240px -832px""></span><span class=""sprite-text"">Magma Block</span></span></a></li>
<li><a href=""/Obsidian"" title=""Obsidian""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-320px -832px""></span><span class=""sprite-text"">Obsidian</span></span></a></li></ul>
<table>
<tbody><tr>
<th>Overworld
</th>
<td>
<ul><li><a href=""/Clay_(block)"" class=""mw-redirect"" title=""Clay (block)""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-336px -864px""></span><span class=""sprite-text"">Clay</span></span></a>
<ul><li><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-144px -880px""></span> <span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-320px -224px""></span> <span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-304px -224px""></span> <span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-304px -240px""></span> <span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-368px -208px""></span> <a href=""/Terracotta"" title=""Terracotta""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-336px -224px""></span><span class=""sprite-text"">Terracotta</span></span></a></li></ul></li>
<li><a href=""/Coarse_Dirt"" title=""Coarse Dirt""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-368px -304px""></span><span class=""sprite-text"">Coarse Dirt</span></span></a></li>
<li><a href=""/Dirt"" title=""Dirt""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-32px -0px""></span><span class=""sprite-text"">Dirt</span></span></a></li>
<li><a href=""/Grass_Block"" title=""Grass Block""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-192px -832px""></span><span class=""sprite-text"">Grass Block</span></span></a></li>
<li><a href=""/Ice"" title=""Ice""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-224px -832px""></span><span class=""sprite-text"">Ice</span></span></a>
<ul><li><a href=""/Packed_Ice"" title=""Packed Ice""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-336px -832px""></span><span class=""sprite-text"">Packed</span></span></a></li>
<li><a href=""/Blue_Ice"" title=""Blue Ice""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-0px -528px""></span><span class=""sprite-text"">Blue</span></span></a></li></ul></li>
<li><a href=""/Mycelium"" title=""Mycelium""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-272px -832px""></span><span class=""sprite-text"">Mycelium</span></span></a></li>
<li><a href=""/Podzol"" title=""Podzol""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-256px -256px""></span><span class=""sprite-text"">Podzol</span></span></a></li>
<li><a href=""/Sand"" title=""Sand""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-80px -848px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-16px -848px""></span><span class=""sprite-text"">Sand</span></span></span></span></a></li>
<li><a href=""/Sandstone"" title=""Sandstone""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-112px -848px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-48px -848px""></span><span class=""sprite-text"">Sandstone</span></span></span></span></a></li>
<li><a href=""/Snow"" title=""Snow""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-176px -848px""></span><span class=""sprite-text"">Snow</span></span></a>
<ul><li><a href=""/Snow_Block"" title=""Snow Block""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-160px -848px""></span><span class=""sprite-text"">Block</span></span></a></li></ul></li>
<li><a href=""/Stone"" title=""Stone""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-288px -816px""></span><span class=""sprite-text"">Stone</span></span></a>
<ul><li><a href=""/Granite"" title=""Granite""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-176px -832px""></span><span class=""sprite-text"">Granite</span></span></a></li>
<li><a href=""/Diorite"" title=""Diorite""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-96px -832px""></span><span class=""sprite-text"">Diorite</span></span></a></li>
<li><a href=""/Andesite"" title=""Andesite""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-304px -816px""></span><span class=""sprite-text"">Andesite</span></span></a></li></ul></li></ul>
<table>
<tbody><tr>
<th>Ore
</th>
<td>
<ul><li><a href=""/Coal_Ore"" title=""Coal Ore""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-320px -912px""></span><span class=""sprite-text"">Coal Ore</span></span></a></li>
<li><a href=""/Diamond_Ore"" title=""Diamond Ore""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-336px -912px""></span><span class=""sprite-text"">Diamond Ore</span></span></a></li>
<li><a href=""/Emerald_Ore"" title=""Emerald Ore""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-352px -912px""></span><span class=""sprite-text"">Emerald Ore</span></span></a></li>
<li><a href=""/Gold_Ore"" title=""Gold Ore""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-368px -912px""></span><span class=""sprite-text"">Gold Ore</span></span></a></li>
<li><a href=""/Iron_Ore"" title=""Iron Ore""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-0px -928px""></span><span class=""sprite-text"">Iron Ore</span></span></a></li>
<li><a href=""/Lapis_Lazuli_Ore"" title=""Lapis Lazuli Ore""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-16px -928px""></span><span class=""sprite-text"">Lapis Lazuli Ore</span></span></a></li>
<li><a href=""/Redstone_Ore"" title=""Redstone Ore""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-48px -928px""></span><span class=""sprite-text"">Redstone Ore</span></span></a></li></ul>
</td></tr></tbody></table>
</td></tr>
<tr>
<th><a href=""/Liquid"" title=""Liquid"">Liquids</a>
</th>
<td>
<ul><li><a href=""/Bubble_Column"" title=""Bubble Column""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-128px -528px""></span><span class=""sprite-text"">Bubble Column</span></span></a></li>
<li><a href=""/Lava"" title=""Lava""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-336px -48px""></span><span class=""sprite-text"">Lava</span></span></a></li>
<li><a href=""/Water"" title=""Water""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-352px -48px""></span><span class=""sprite-text"">Water</span></span></a></li></ul>
</td></tr>
<tr>
<th>Gases
</th>
<td>
<ul><li><a href=""/Air"" title=""Air""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-240px -16px""></span><span class=""sprite-text"">Air</span></span></a></li></ul>
<table>
<tbody><tr>
<th><a href=""/Java_Edition"" title=""Java Edition""><i>Java Edition</i></a> only
</th>
<td>
<ul><li><a href=""/Cave_Air"" class=""mw-redirect"" title=""Cave Air""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-240px -16px""></span><span class=""sprite-text"">Cave Air</span></span></a></li>
<li><a href=""/Void_Air"" class=""mw-redirect"" title=""Void Air""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-240px -16px""></span><span class=""sprite-text"">Void Air</span></span></a></li></ul>
</td></tr></tbody></table>
</td></tr>
<tr>
<th><a href=""/The_Nether"" title=""The Nether"">The Nether</a>
</th>
<td>
<ul><li><a href=""/Basalt"" title=""Basalt""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-320px -1392px""></span><span class=""sprite-text"">Basalt</span></span></a></li>
<li><a href=""/Blackstone"" title=""Blackstone""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-208px -1456px""></span><span class=""sprite-text"">Blackstone</span></span></a></li>
<li><a href=""/Glowstone"" title=""Glowstone""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-160px -832px""></span><span class=""sprite-text"">Glowstone</span></span></a></li>
<li><a href=""/Netherrack"" title=""Netherrack""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-304px -832px""></span><span class=""sprite-text"">Netherrack</span></span></a></li>
<li><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-272px -1392px""></span> <a href=""/Nylium"" title=""Nylium""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-0px -1408px""></span><span class=""sprite-text"">Nylium</span></span></a></li>
<li><a href=""/Soul_Fire"" class=""mw-redirect"" title=""Soul Fire""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-352px -1424px""></span><span class=""sprite-text"">Soul Fire</span></span></a></li>
<li><a href=""/Soul_Sand"" title=""Soul Sand""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-208px -848px""></span><span class=""sprite-text"">Soul Sand</span></span></a></li>
<li><a href=""/Soul_Soil"" title=""Soul Soil""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-368px -1392px""></span><span class=""sprite-text"">Soul Soil</span></span></a></li></ul>
<table>
<tbody><tr>
<th>Ore
</th>
<td>
<ul><li><a href=""/Ancient_Debris"" title=""Ancient Debris""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-128px -1392px""></span><span class=""sprite-text"">Ancient Debris</span></span></a></li>
<li><a href=""/Nether_Gold_Ore"" title=""Nether Gold Ore""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-144px -1456px""></span><span class=""sprite-text"">Nether Gold Ore</span></span></a></li>
<li><a href=""/Nether_Quartz_Ore"" title=""Nether Quartz Ore""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-32px -928px""></span><span class=""sprite-text"">Nether Quartz Ore</span></span></a></li></ul>
</td></tr></tbody></table>
</td></tr>
<tr>
<th><a href=""/The_End"" title=""The End"">The End</a>
</th>
<td>
<ul><li><a href=""/End_Stone"" title=""End Stone""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-128px -832px""></span><span class=""sprite-text"">End Stone</span></span></a></li></ul>
</td></tr></tbody></table>
</td></tr>
<tr>
<th><a href=""/Structures"" class=""mw-redirect"" title=""Structures"">Structures</a>
</th>
<td>
<ul><li><a href=""/Bone_Block"" title=""Bone Block""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-336px -816px""></span><span class=""sprite-text"">Bone Block</span></span></a></li>
<li><a href=""/Crying_Obsidian"" title=""Crying Obsidian""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-64px -1440px""></span><span class=""sprite-text"">Crying Obsidian</span></span></a></li>
<li><a href=""/Iron_Bars"" title=""Iron Bars""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-224px -896px""></span><span class=""sprite-text"">Iron Bars</span></span></a></li></ul>
<table>
<tbody><tr>
<th><a href=""/Overworld"" title=""Overworld"">Overworld</a>
</th>
<td>
<ul><li><a href=""/Banner"" title=""Banner""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-96px -1120px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-96px -320px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-320px -704px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-352px -704px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-0px -720px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-48px -720px""></span><span class=""sprite-text"">Banner</span></span></span></span></span></span></span></span></span></span></span></span></a></li>
<li><a href=""/Bee_Nest"" class=""mw-redirect"" title=""Bee Nest""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-320px -1264px""></span><span class=""sprite-text"">Bee Nest</span></span></a></li>
<li><a href=""/Bookshelf"" title=""Bookshelf""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-272px -928px""></span><span class=""sprite-text"">Bookshelf</span></span></a></li>
<li><a href=""/Bricks"" title=""Bricks""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-288px -928px""></span><span class=""sprite-text"">Bricks</span></span></a></li>
<li><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-160px -352px""></span> <span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-0px -736px""></span> <span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-16px -768px""></span> <span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-48px -736px""></span> <a href=""/Carpet"" title=""Carpet""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-80px -736px""></span><span class=""sprite-text"">Carpet</span></span></a></li>
<li><a href=""/Carved_Pumpkin"" class=""mw-redirect"" title=""Carved Pumpkin""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-0px -1152px""></span><span class=""sprite-text"">Carved Pumpkin</span></span></a></li>
<li><a href=""/Cobblestone"" title=""Cobblestone""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-32px -832px""></span><span class=""sprite-text"">Cobblestone</span></span></a></li>
<li><a href=""/Mossy_Cobblestone"" title=""Mossy Cobblestone""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-256px -832px""></span><span class=""sprite-text"">Mossy Cobblestone</span></span></a></li>
<li><a href=""/Wall"" title=""Wall""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-208px -784px""></span><span class=""sprite-text"">Walls</span></span></a></li>
<li><a href=""/Cobweb"" title=""Cobweb""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-48px -832px""></span><span class=""sprite-text"">Cobweb</span></span></a></li>
<li><a href=""/End_Portal_Frame"" title=""End Portal Frame""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-320px -1216px""></span><span class=""sprite-text"">End Portal Frame</span></span></a></li>
<li><a href=""/Oak_Fence"" class=""mw-redirect"" title=""Oak Fence""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-48px -960px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-336px -944px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-64px -960px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-0px -960px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-304px -944px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-368px -944px""></span><span class=""sprite-text"">Fences</span></span></span></span></span></span></span></span></span></span></span></span></a></li>
<li><a href=""/Fire"" title=""Fire""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-304px -48px""></span><span class=""sprite-text"">Fire</span></span></a></li>
<li><a href=""/Flower_Pot"" title=""Flower Pot""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-192px -896px""></span><span class=""sprite-text"">Flower Pot</span></span></a></li>
<li><a href=""/Glass"" title=""Glass""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-32px -1008px""></span><span class=""sprite-text"">Glass</span></span></a>
<ul><li><a href=""/Glass_Pane"" title=""Glass Pane""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-64px -144px""></span><span class=""sprite-text"">Pane</span></span></a></li></ul></li>
<li><a href=""/Grass_Path"" title=""Grass Path""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-272px -320px""></span><span class=""sprite-text"">Grass Path</span></span></a></li>
<li><a href=""/Glazed_Terracotta"" title=""Glazed Terracotta""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-112px -448px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-64px -448px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-32px -448px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-144px -448px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-0px -448px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-48px -448px""></span><span class=""sprite-text"">Glazed Terracotta</span></span></span></span></span></span></span></span></span></span></span></span></a></li>
<li><a href=""/Hay_Bale"" title=""Hay Bale""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-144px -1152px""></span><span class=""sprite-text"">Hay Bale</span></span></a></li>
<li><a href=""/Infested_Block"" title=""Infested Block""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-336px -1440px""></span><span class=""sprite-text"">Infested Block</span></span></a></li>
<li><a href=""/Jack_o%27Lantern"" title=""Jack o&#39;Lantern""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-176px -1152px""></span><span class=""sprite-text"">Jack o'Lantern</span></span></a></li>
<li><a href=""/Ladder"" title=""Ladder""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-240px -896px""></span><span class=""sprite-text"">Ladder</span></span></a></li>
<li><a href=""/Planks"" title=""Planks""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-224px -928px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-224px -848px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-240px -928px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-208px -928px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-176px -928px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-192px -928px""></span><span class=""sprite-text"">Planks</span></span></span></span></span></span></span></span></span></span></span></span></a></li>
<li><a href=""/Andesite"" title=""Andesite""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-304px -896px""></span><span class=""sprite-text"">Polished Andesite</span></span></a></li>
<li><a href=""/Diorite"" title=""Diorite""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-320px -896px""></span><span class=""sprite-text"">Polished Diorite</span></span></a></li>
<li><a href=""/Granite"" title=""Granite""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-336px -896px""></span><span class=""sprite-text"">Polished Granite</span></span></a></li>
<li><a href=""/Prismarine"" title=""Prismarine""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-0px -848px""></span><span class=""sprite-text"">Prismarine</span></span></a>
<ul><li><a href=""/Prismarine"" title=""Prismarine""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-208px -960px""></span><span class=""sprite-text"">Bricks</span></span></a></li>
<li><a href=""/Prismarine"" title=""Prismarine""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-128px -960px""></span><span class=""sprite-text"">Dark</span></span></a></li></ul></li>
<li><a href=""/Sea_Lantern"" title=""Sea Lantern""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-0px -320px""></span><span class=""sprite-text"">Sea Lantern</span></span></a></li>
<li><a href=""/Sandstone"" title=""Sandstone""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-80px -832px""></span><span class=""sprite-text"">Cut Sandstone</span></span></a>
<ul><li><a href=""/Sandstone"" title=""Sandstone""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-0px -832px""></span><span class=""sprite-text"">Chiseled</span></span></a></li>
<li><a href=""/Sandstone"" title=""Sandstone""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-128px -848px""></span><span class=""sprite-text"">Smooth</span></span></a></li></ul></li>
<li><a href=""/Smooth_Stone"" title=""Smooth Stone""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-288px -944px""></span><span class=""sprite-text"">Smooth Stone</span></span></a></li>
<li><a href=""/Spawner"" title=""Spawner""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-240px -1216px""></span><span class=""sprite-text"">Spawner</span></span></a>
<ul><li><a href=""/Spawner#Natural_generation"" title=""Spawner""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-112px -1232px""></span><span class=""sprite-text"">Cave Spider</span></span></a></li>
<li><a href=""/Spawner#Natural_generation"" title=""Spawner""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-144px -1232px""></span><span class=""sprite-text"">Silverfish</span></span></a></li>
<li><a href=""/Spawner#Natural_generation"" title=""Spawner""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-160px -1232px""></span><span class=""sprite-text"">Skeleton</span></span></a></li>
<li><a href=""/Spawner#Natural_generation"" title=""Spawner""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-176px -1232px""></span><span class=""sprite-text"">Spider</span></span></a></li>
<li><a href=""/Spawner#Natural_generation"" title=""Spawner""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-192px -1232px""></span><span class=""sprite-text"">Zombie</span></span></a></li></ul></li>
<li><a href=""/Sponge"" title=""Sponge""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-0px -1040px""></span><span class=""sprite-text"">Sponge</span></span></a>
<ul><li><a href=""/Sponge"" title=""Sponge""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-112px -1040px""></span><span class=""sprite-text"">Wet</span></span></a></li></ul></li>
<li><a href=""/Stained_Glass_Pane"" class=""mw-redirect"" title=""Stained Glass Pane""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-256px -368px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-80px -368px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-288px -368px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-64px -368px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-128px -368px""></span><span class=""sprite-text"">Stained Glass Pane</span></span></span></span></span></span></span></span></span></span></a></li>
<li><a href=""/Stone_Bricks"" title=""Stone Bricks""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-176px -960px""></span><span class=""sprite-text"">Stone Bricks</span></span></a>
<ul><li><a href=""/Stone_Bricks"" title=""Stone Bricks""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-112px -960px""></span><span class=""sprite-text"">Cracked</span></span></a></li>
<li><a href=""/Stone_Bricks"" title=""Stone Bricks""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-160px -960px""></span><span class=""sprite-text"">Mossy</span></span></a></li>
<li><a href=""/Stone_Bricks"" title=""Stone Bricks""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-96px -960px""></span><span class=""sprite-text"">Chiseled</span></span></a></li></ul></li>
<li><a href=""/Log"" title=""Log""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-80px -560px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-320px -544px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-128px -560px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-32px -560px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-272px -544px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-368px -544px""></span><span class=""sprite-text"">Stripped Log</span></span></span></span></span></span></span></span></span></span></span></span></a></li>
<li><a href=""/Wood"" title=""Wood""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-112px -864px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-352px -848px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-160px -864px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-64px -864px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-304px -848px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-16px -864px""></span><span class=""sprite-text"">Wood</span></span></span></span></span></span></span></span></span></span></span></span></a>
<ul><li><a href=""/Wood"" title=""Wood""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-80px -560px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-320px -544px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-128px -560px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-32px -560px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-272px -544px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-368px -544px""></span><span class=""sprite-text"">Stripped</span></span></span></span></span></span></span></span></span></span></span></span></a></li></ul></li>
<li><a href=""/Wool"" title=""Wool""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-0px -64px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-32px -176px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-32px -144px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-16px -128px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-32px -160px""></span><span class=""sprite-text"">Wool</span></span></span></span></span></span></span></span></span></span></a></li></ul>
</td></tr>
<tr>
<th><a href=""/The_Nether"" title=""The Nether"">The Nether</a>
</th>
<td>
<ul><li><a href=""/Chain"" title=""Chain""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-80px -1472px""></span><span class=""sprite-text"">Chain</span></span></a></li>
<li><a href=""/Gilded_Blackstone"" title=""Gilded Blackstone""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-368px -1456px""></span><span class=""sprite-text"">Gilded Blackstone</span></span></a></li>
<li><a href=""/Nether_Bricks"" title=""Nether Bricks""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-192px -960px""></span><span class=""sprite-text"">Nether Bricks</span></span></a>
<ul><li><a href=""/Fence"" title=""Fence""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-32px -960px""></span><span class=""sprite-text"">Fence</span></span></a></li></ul></li>
<li><a href=""/Nether_Portal_(block)"" title=""Nether Portal (block)""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-320px -16px""></span><span class=""sprite-text"">Nether Portal</span></span></a></li>
<li><a href=""/Polished_Basalt"" class=""mw-redirect"" title=""Polished Basalt""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-192px -1440px""></span><span class=""sprite-text"">Polished Basalt</span></span></a></li>
<li><a href=""/Polished_Blackstone"" title=""Polished Blackstone""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-304px -1456px""></span><span class=""sprite-text"">Polished Blackstone</span></span></a>
<ul><li><a href=""/Polished_Blackstone_Bricks"" title=""Polished Blackstone Bricks""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-48px -1472px""></span><span class=""sprite-text"">Bricks</span></span></a></li></ul></li>
<li><a href=""/Spawner"" title=""Spawner""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-240px -1216px""></span><span class=""sprite-text"">Spawner</span></span></a>
<ul><li><a href=""/Spawner#Natural_generation"" title=""Spawner""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-96px -1232px""></span><span class=""sprite-text"">Blaze</span></span></a></li>
<li><a href=""/Spawner#Natural_generation"" title=""Spawner""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-96px -1472px""></span><span class=""sprite-text"">Magma Cube</span></span></a></li></ul></li></ul>
</td></tr>
<tr>
<th><a href=""/The_End"" title=""The End"">The End</a>
</th>
<td>
<ul><li><a href=""/Banner"" title=""Banner""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-80px -720px""></span><span class=""sprite-text"">Banner</span></span></a></li>
<li><a href=""/Dragon_Egg"" title=""Dragon Egg""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-112px -160px""></span><span class=""sprite-text"">Dragon Egg</span></span></a></li>
<li><a href=""/End_Gateway_(block)"" title=""End Gateway (block)""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-368px -80px""></span><span class=""sprite-text"">End Gateway</span></span></a></li>
<li><a href=""/End_Portal_(block)"" title=""End Portal (block)""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-368px -80px""></span><span class=""sprite-text"">End Portal</span></span></a></li>
<li><a href=""/End_Rod"" title=""End Rod""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-208px -1216px""></span><span class=""sprite-text"">End Rod</span></span></a></li>
<li><a href=""/End_Stone_Bricks"" title=""End Stone Bricks""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-144px -960px""></span><span class=""sprite-text"">End Stone Bricks</span></span></a></li>
<li><a href=""/Purpur_Block"" title=""Purpur Block""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-208px -944px""></span><span class=""sprite-text"">Purpur Block</span></span></a>
<ul><li><a href=""/Purpur_Block"" title=""Purpur Block""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-224px -944px""></span><span class=""sprite-text"">Pillar</span></span></a></li></ul></li>
<li><a href=""/Stained_Glass"" class=""mw-redirect"" title=""Stained Glass""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-208px -912px""></span><span class=""sprite-text"">Stained Glass</span></span></a></li></ul>
</td></tr></tbody></table>
</td></tr>
<tr>
<th><a href=""/Plants"" title=""Plants"">Flora and fauna</a>
</th>
<td>
<table>
<tbody><tr>
<th>Plants
</th>
<td>
<table>
<tbody><tr>
<th><a href=""/Overworld"" title=""Overworld"">Overworld</a>
</th>
<td>
<ul><li><a href=""/Bamboo"" title=""Bamboo""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-32px -672px""></span><span class=""sprite-text"">Bamboo</span></span></a>
<ul><li><a href=""/Bamboo_Sapling"" class=""mw-redirect"" title=""Bamboo Sapling""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-288px -656px""></span><span class=""sprite-text"">Sapling</span></span></a></li></ul></li>
<li><a href=""/Beetroots"" class=""mw-redirect"" title=""Beetroots""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-224px -1136px""></span><span class=""sprite-text"">Beetroots</span></span></a></li>
<li><a href=""/Cactus"" title=""Cactus""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-48px -944px""></span><span class=""sprite-text"">Cactus</span></span></a></li>
<li><a href=""/Carrots"" class=""mw-redirect"" title=""Carrots""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-320px -1136px""></span><span class=""sprite-text"">Carrots</span></span></a></li>
<li><a href=""/Cocoa"" class=""mw-redirect"" title=""Cocoa""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-64px -1152px""></span><span class=""sprite-text"">Cocoa</span></span></a></li>
<li><a href=""/Dead_Bush"" title=""Dead Bush""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-16px -1120px""></span><span class=""sprite-text"">Dead Bush</span></span></a></li>
<li><a href=""/Fern"" class=""mw-redirect"" title=""Fern""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-32px -1120px""></span><span class=""sprite-text"">Fern</span></span></a>
<ul><li><a href=""/Grass"" title=""Grass""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-64px -1120px""></span><span class=""sprite-text"">Large</span></span></a></li></ul></li>
<li><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-192px -1120px""></span> <span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-304px -1120px""></span> <span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-336px -1120px""></span> <a href=""/Flowers"" class=""mw-redirect"" title=""Flowers""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-336px -656px""></span><span class=""sprite-text"">Flowers</span></span></a></li>
<li><a href=""/Grass"" title=""Grass""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-48px -1120px""></span><span class=""sprite-text"">Grass</span></span></a>
<ul><li><a href=""/Grass"" title=""Grass""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-112px -1120px""></span><span class=""sprite-text"">Tall</span></span></a></li></ul></li>
<li><a href=""/Kelp"" title=""Kelp""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-320px -368px""></span><span class=""sprite-text"">Kelp</span></span></a>
<ul><li><a href=""/Dried_Kelp_Block"" title=""Dried Kelp Block""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-16px -304px""></span><span class=""sprite-text"">Dried, Block</span></span></a></li></ul></li>
<li><a href=""/Leaves"" title=""Leaves""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-0px -1248px""></span><span class=""sprite-text"">Leaves</span></span></a></li>
<li><a href=""/Lily_Pad"" title=""Lily Pad""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-80px -1120px""></span><span class=""sprite-text"">Lily Pad</span></span></a></li>
<li><a href=""/Log"" title=""Log""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-112px -864px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-352px -848px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-160px -864px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-64px -864px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-304px -848px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-16px -864px""></span><span class=""sprite-text"">Log</span></span></span></span></span></span></span></span></span></span></span></span></a></li>
<li><a href=""/Melon"" title=""Melon""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-192px -1152px""></span><span class=""sprite-text"">Melon</span></span></a>
<ul><li><a href=""/Melon_Stem"" class=""mw-redirect"" title=""Melon Stem""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-240px -96px""></span><span class=""sprite-text"">Stem</span></span></a></li></ul></li>
<li><a href=""/Potatoes"" class=""mw-redirect"" title=""Potatoes""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-304px -1152px""></span><span class=""sprite-text"">Potatoes</span></span></a></li>
<li><a href=""/Pumpkin"" title=""Pumpkin""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-368px -1152px""></span><span class=""sprite-text"">Pumpkin</span></span></a>
<ul><li><a href=""/Pumpkin_Stem"" class=""mw-redirect"" title=""Pumpkin Stem""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-240px -96px""></span><span class=""sprite-text"">Stem</span></span></a></li></ul></li>
<li><a href=""/Sapling"" title=""Sapling""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-160px -944px""></span><span class=""sprite-text"">Sapling</span></span></a></li>
<li><a href=""/Seagrass"" title=""Seagrass""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-48px -304px""></span><span class=""sprite-text"">Seagrass</span></span></a>
<ul><li><a href=""/Seagrass"" title=""Seagrass""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-336px -368px""></span><span class=""sprite-text"">Tall</span></span></a></li></ul></li>
<li><a href=""/Sugar_Cane"" title=""Sugar Cane""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-48px -1168px""></span><span class=""sprite-text"">Sugar Cane</span></span></a></li>
<li><a href=""/Sweet_Berry_Bush"" class=""mw-redirect"" title=""Sweet Berry Bush""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-320px -784px""></span><span class=""sprite-text"">Sweet Berry Bush</span></span></a></li>
<li><a href=""/Vines"" title=""Vines""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-128px -1120px""></span><span class=""sprite-text"">Vines</span></span></a></li>
<li><a href=""/Wheat_Seeds"" title=""Wheat Seeds""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-64px -1168px""></span><span class=""sprite-text"">Wheat</span></span></a></li></ul>
</td></tr>
<tr>
<th><a href=""/The_End"" title=""The End"">The End</a>
</th>
<td>
<ul><li><a href=""/Chorus_Plant"" title=""Chorus Plant""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-48px -1152px""></span><span class=""sprite-text"">Chorus Plant</span></span></a>
<ul><li><a href=""/Chorus_Flower"" title=""Chorus Flower""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-16px -1152px""></span><span class=""sprite-text"">Flower</span></span></a></li></ul></li></ul>
</td></tr></tbody></table>
</td></tr>
<tr>
<th>Animals
</th>
<td>
<table>
<tbody><tr>
<td>
<ul><li><a href=""/Turtle_Egg"" title=""Turtle Egg""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-128px -544px""></span><span class=""sprite-text"">Turtle Egg</span></span></a></li>
<li><a href=""/Coral"" title=""Coral""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-192px -304px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-32px -368px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-352px -368px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-0px -384px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-16px -384px""></span><span class=""sprite-text"">Coral</span></span></span></span></span></span></span></span></span></span></a>
<ul><li><a href=""/Coral"" title=""Coral""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-208px -656px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-224px -656px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-240px -656px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-256px -656px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-272px -656px""></span><span class=""sprite-text"">Dead</span></span></span></span></span></span></span></span></span></span></a></li></ul></li>
<li><a href=""/Coral_Block"" title=""Coral Block""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-64px -304px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-224px -304px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-240px -304px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-256px -304px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-0px -336px""></span><span class=""sprite-text"">Coral Block</span></span></span></span></span></span></span></span></span></span></a>
<ul><li><a href=""/Coral_Block"" title=""Coral Block""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-0px -352px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-16px -352px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-32px -352px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-0px -368px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-16px -368px""></span><span class=""sprite-text"">Dead</span></span></span></span></span></span></span></span></span></span></a></li></ul></li>
<li><a href=""/Coral_Fan"" title=""Coral Fan""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-288px -48px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-192px -128px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-304px -160px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-144px -176px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-192px -224px""></span><span class=""sprite-text"">Coral Fan</span></span></span></span></span></span></span></span></span></span></a>
<ul><li><a href=""/Coral_Fan"" title=""Coral Fan""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-240px -560px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-192px -560px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-176px -560px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-208px -560px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-224px -560px""></span><span class=""sprite-text"">Dead</span></span></span></span></span></span></span></span></span></span></a></li></ul></li>
<li><a href=""/Sea_Pickle"" title=""Sea Pickle""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-112px -528px""></span><span class=""sprite-text"">Sea Pickle</span></span></a></li></ul>
</td></tr></tbody></table>
</td></tr>
<tr>
<th>Fungi
</th>
<td>
<table>
<tbody><tr>
<th><a href=""/Overworld"" title=""Overworld"">Overworld</a>
</th>
<td>
<ul><li><a href=""/Mushrooms"" class=""mw-redirect"" title=""Mushrooms""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-240px -1152px""></span><span class=""sprite-text"">Mushrooms</span></span></a>
<ul><li><a href=""/Mushroom_Block"" title=""Mushroom Block""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-208px -1136px""></span><span class=""sprite-text"">Blocks</span></span></a></li></ul></li></ul>
</td></tr>
<tr>
<th><a href=""/The_Nether"" title=""The Nether"">The Nether</a>
</th>
<td>
<ul><li><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-176px -1392px""></span> <a href=""/Fungus"" title=""Fungus""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-192px -1392px""></span><span class=""sprite-text"">Fungus</span></span></a></li>
<li><a href=""/Nether_Sprouts"" title=""Nether Sprouts""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-192px -1424px""></span><span class=""sprite-text"">Nether Sprouts</span></span></a></li>
<li><a href=""/Nether_Wart"" title=""Nether Wart""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-256px -1152px""></span><span class=""sprite-text"">Nether Wart</span></span></a></li>
<li><a href=""/Nether_Wart_Block"" title=""Nether Wart Block""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-304px -1392px""></span><span class=""sprite-text"">Nether Wart Block</span></span></a>
<ul><li><a href=""/Warped_Wart_Block"" class=""mw-redirect"" title=""Warped Wart Block""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-240px -1392px""></span><span class=""sprite-text"">Warped</span></span></a></li></ul></li>
<li><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-176px -1424px""></span> <a href=""/Roots"" title=""Roots""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-208px -1424px""></span><span class=""sprite-text"">Roots</span></span></a></li>
<li><a href=""/Shroomlight"" title=""Shroomlight""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-352px -1392px""></span><span class=""sprite-text"">Shroomlight</span></span></a></li>
<li><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-32px -1408px""></span> <a href=""/Stem"" title=""Stem""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-128px -1408px""></span><span class=""sprite-text"">Stem</span></span></a></li>
<li><a href=""/Twisting_Vines"" title=""Twisting Vines""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-160px -1440px""></span><span class=""sprite-text"">Twisting Vines</span></span></a></li>
<li><a href=""/Weeping_Vines"" title=""Weeping Vines""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-208px -1392px""></span><span class=""sprite-text"">Weeping Vines</span></span></a></li></ul>
</td></tr></tbody></table>
</td></tr></tbody></table>
</td></tr>
<tr>
<th>Manufactured
</th>
<td>
<ul><li><a href=""/Concrete"" title=""Concrete""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-368px -416px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-32px -432px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-16px -432px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-272px -416px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-304px -416px""></span><span class=""sprite-text"">Concrete</span></span></span></span></span></span></span></span></span></span></a>
<ul><li><a href=""/Concrete_Powder"" title=""Concrete Powder""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-240px -432px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-288px -432px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-272px -432px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-144px -432px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-176px -432px""></span><span class=""sprite-text"">Powder</span></span></span></span></span></span></span></span></span></span></a></li></ul></li>
<li><a href=""/Conduit"" title=""Conduit""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-16px -528px""></span><span class=""sprite-text"">Conduit</span></span></a></li>
<li><a href=""/Cracked_Nether_Bricks"" class=""mw-redirect"" title=""Cracked Nether Bricks""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-32px -1472px""></span><span class=""sprite-text"">Cracked Nether Bricks</span></span></a>
<ul><li><a href=""/Nether_Bricks"" title=""Nether Bricks""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-16px -1472px""></span><span class=""sprite-text"">Chiseled</span></span></a></li>
<li><a href=""/Nether_Bricks"" title=""Nether Bricks""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-224px -960px""></span><span class=""sprite-text"">Red</span></span></a></li></ul></li>
<li><a href=""/Cut_Red_Sandstone"" class=""mw-redirect"" title=""Cut Red Sandstone""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-64px -832px""></span><span class=""sprite-text"">Cut Red Sandstone</span></span></a>
<ul><li><a href=""/Sandstone"" title=""Sandstone""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-368px -816px""></span><span class=""sprite-text"">Chiseled</span></span></a></li>
<li><a href=""/Sandstone"" title=""Sandstone""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-64px -848px""></span><span class=""sprite-text"">Smooth</span></span></a></li></ul></li>
<li><a href=""/Honey_block"" class=""mw-redirect"" title=""Honey block""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-288px -1296px""></span><span class=""sprite-text"">Honey block</span></span></a></li>
<li><a href=""/Honeycomb_block"" class=""mw-redirect"" title=""Honeycomb block""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-320px -1296px""></span><span class=""sprite-text"">Honeycomb block</span></span></a></li>
<li><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-32px -1408px""></span> <a href=""/Hyphae"" class=""mw-redirect"" title=""Hyphae""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-128px -1408px""></span><span class=""sprite-text"">Hyphae</span></span></a>
<ul><li><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-64px -1408px""></span> <a href=""/Hyphae"" class=""mw-redirect"" title=""Hyphae""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-96px -1408px""></span><span class=""sprite-text"">Stripped</span></span></a></li></ul></li>
<li><a href=""/Planks"" title=""Planks""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-256px -1408px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-272px -1408px""></span><span class=""sprite-text"">Planks</span></span></span></span></a></li>
<li><a href=""/Slab"" title=""Slab""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-272px -960px""></span><span class=""sprite-text"">Slab</span></span></a></li>
<li><a href=""/Slime_Block"" title=""Slime Block""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-272px -944px""></span><span class=""sprite-text"">Slime Block</span></span></a></li>
<li><a href=""/Stairs"" title=""Stairs""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-240px -976px""></span><span class=""sprite-text"">Stairs</span></span></a></li>
<li><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-64px -1408px""></span> <a href=""/Stripped_Stem"" class=""mw-redirect"" title=""Stripped Stem""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-96px -1408px""></span><span class=""sprite-text"">Stripped Stem</span></span></a></li></ul>
<table>
<tbody><tr>
<th><a href=""/BE"" class=""mw-redirect"" title=""BE"">BE</a> &amp; <i><a href=""/EE"" class=""mw-redirect"" title=""EE"">EE</a></i> only
</th>
<td>
<ul><li><a href=""/Elements"" class=""mw-redirect"" title=""Elements""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-192px -576px""></span><span class=""sprite-text"">Elements</span></span></a></li>
<li><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-112px -240px""></span><a href=""/Hardened_Glass"" title=""Hardened Glass""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-224px -528px""></span><span class=""sprite-text"">Hardened Glass</span></span></a></li>
<li><a href=""/Heat_Block"" title=""Heat Block""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-176px -656px""></span><span class=""sprite-text"">Heat Block</span></span></a></li></ul>
</td></tr></tbody></table>
</td></tr>
<tr>
<th>Mineral blocks
</th>
<td>
<ul><li><a href=""/Block_of_Coal"" title=""Block of Coal""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-64px -928px""></span><span class=""sprite-text"">Block of Coal</span></span></a></li>
<li><a href=""/Block_of_Diamond"" title=""Block of Diamond""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-80px -928px""></span><span class=""sprite-text"">Block of Diamond</span></span></a></li>
<li><a href=""/Block_of_Emerald"" title=""Block of Emerald""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-96px -928px""></span><span class=""sprite-text"">Block of Emerald</span></span></a></li>
<li><a href=""/Block_of_Gold"" title=""Block of Gold""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-112px -928px""></span><span class=""sprite-text"">Block of Gold</span></span></a></li>
<li><a href=""/Block_of_Iron"" title=""Block of Iron""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-128px -928px""></span><span class=""sprite-text"">Block of Iron</span></span></a></li>
<li><a href=""/Block_of_Netherite"" title=""Block of Netherite""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-160px -1392px""></span><span class=""sprite-text"">Block of Netherite</span></span></a></li>
<li><a href=""/Block_of_Quartz"" title=""Block of Quartz""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-48px -1008px""></span><span class=""sprite-text"">Block of Quartz</span></span></a>
<ul><li><a href=""/Block_of_Quartz"" title=""Block of Quartz""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-80px -1008px""></span><span class=""sprite-text"">Chiseled</span></span></a></li>
<li><a href=""/Block_of_Quartz"" title=""Block of Quartz""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-112px -1008px""></span><span class=""sprite-text"">Pillar</span></span></a></li>
<li><a href=""/Block_of_Quartz"" title=""Block of Quartz""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-64px -1008px""></span><span class=""sprite-text"">Smooth</span></span></a></li>
<li><a href=""/Block_of_Quartz"" title=""Block of Quartz""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-192px -1456px""></span><span class=""sprite-text"">Bricks</span></span></a></li></ul></li>
<li><a href=""/Block_of_Redstone"" title=""Block of Redstone""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-160px -928px""></span><span class=""sprite-text"">Block of Redstone</span></span></a></li>
<li><a href=""/Lapis_Lazuli_Block"" title=""Lapis Lazuli Block""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-144px -928px""></span><span class=""sprite-text"">Lapis Lazuli Block</span></span></a></li></ul>
</td></tr>
<tr>
<th>Utility
</th>
<td>
<ul><li><a href=""/Anvil"" title=""Anvil""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-192px -1008px""></span><span class=""sprite-text"">Anvil</span></span></a></li>
<li><a href=""/Barrel"" title=""Barrel""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-48px -672px""></span><span class=""sprite-text"">Barrel</span></span></a></li>
<li><a href=""/Beacon"" title=""Beacon""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-80px -1232px""></span><span class=""sprite-text"">Beacon</span></span></a></li>
<li><a href=""/Beehive"" title=""Beehive""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-240px -1264px""></span><span class=""sprite-text"">Beehive</span></span></a></li>
<li><a href=""/Bed"" title=""Bed""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-64px -1104px""></span><span class=""sprite-text"">Bed</span></span></a></li>
<li><a href=""/Bell"" title=""Bell""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-32px -800px""></span><span class=""sprite-text"">Bell</span></span></a></li>
<li><a href=""/Brewing_Stand"" title=""Brewing Stand""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-256px -1008px""></span><span class=""sprite-text"">Brewing Stand</span></span></a></li>
<li><a href=""/Blast_Furnace"" title=""Blast Furnace""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-96px -672px""></span><span class=""sprite-text"">Blast Furnace</span></span></a></li>
<li><a href=""/Cake"" title=""Cake""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-192px -1216px""></span><span class=""sprite-text"">Cake</span></span></a></li>
<li><a href=""/Campfire"" title=""Campfire""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-0px -800px""></span><span class=""sprite-text"">Campfire</span></span></a></li>
<li><a href=""/Cartography_Table"" title=""Cartography Table""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-144px -672px""></span><span class=""sprite-text"">Cartography Table</span></span></a></li>
<li><a href=""/Cauldron"" title=""Cauldron""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-304px -1008px""></span><span class=""sprite-text"">Cauldron</span></span></a></li>
<li><a href=""/Chest"" title=""Chest""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-336px -80px""></span><span class=""sprite-text"">Chest</span></span></a>
<ul><li><a href=""/Ender_Chest"" title=""Ender Chest""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-160px -1024px""></span><span class=""sprite-text"">Ender</span></span></a></li></ul></li>
<li><a href=""/Crafting_Table"" title=""Crafting Table""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-0px -1024px""></span><span class=""sprite-text"">Crafting Table</span></span></a></li>
<li><a href=""/Enchanting_Table"" title=""Enchanting Table""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-96px -1024px""></span><span class=""sprite-text"">Enchanting Table</span></span></a></li>
<li><a href=""/Fletching_Table"" title=""Fletching Table""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-192px -672px""></span><span class=""sprite-text"">Fletching Table</span></span></a></li>
<li><a href=""/Farmland"" title=""Farmland""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-112px -1152px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-128px -1152px""></span><span class=""sprite-text"">Farmland</span></span></span></span></a></li>
<li><a href=""/Frosted_Ice"" title=""Frosted Ice""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-144px -832px""></span><span class=""sprite-text"">Frosted Ice</span></span></a></li>
<li><a href=""/Furnace"" title=""Furnace""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-16px -944px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-80px -944px""></span><span class=""sprite-text"">Furnace</span></span></span></span></a></li>
<li><a href=""/Grindstone"" title=""Grindstone""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-112px -688px""></span><span class=""sprite-text"">Grindstone</span></span></a></li>
<li><a href=""/Lodestone"" title=""Lodestone""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-160px -1456px""></span><span class=""sprite-text"">Lodestone</span></span></a></li>
<li><a href=""/Mob_head"" class=""mw-redirect"" title=""Mob head""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-224px -1216px""></span><span class=""sprite-text"">Mob Heads</span></span></a>
<ul><li><a href=""/Mob_head"" class=""mw-redirect"" title=""Mob head""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-336px -144px""></span><span class=""sprite-text"">Creeper</span></span></a></li>
<li><a href=""/Mob_head"" class=""mw-redirect"" title=""Mob head""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-224px -240px""></span><span class=""sprite-text"">Dragon</span></span></a></li>
<li><a href=""/Mob_head"" class=""mw-redirect"" title=""Mob head""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-256px -1216px""></span><span class=""sprite-text"">Skeleton</span></span></a></li>
<li><a href=""/Mob_head"" class=""mw-redirect"" title=""Mob head""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-272px -1216px""></span><span class=""sprite-text"">Wither Skeleton</span></span></a></li>
<li><a href=""/Mob_head"" class=""mw-redirect"" title=""Mob head""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-288px -1216px""></span><span class=""sprite-text"">Zombie</span></span></a></li></ul></li>
<li><a href=""/Jukebox"" title=""Jukebox""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-208px -1024px""></span><span class=""sprite-text"">Jukebox</span></span></a></li>
<li><a href=""/Lantern"" title=""Lantern""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-32px -768px""></span><span class=""sprite-text"">Lantern</span></span></a>
<ul><li><a href=""/Soul_Lantern"" class=""mw-redirect"" title=""Soul Lantern""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-112px -1424px""></span><span class=""sprite-text"">Soul</span></span></a></li></ul></li>
<li><a href=""/Lectern"" title=""Lectern""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-176px -688px""></span><span class=""sprite-text"">Lectern</span></span></a></li>
<li><a href=""/Loom"" title=""Loom""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-32px -688px""></span><span class=""sprite-text"">Loom</span></span></a></li>
<li><a href=""/Respawn_Anchor"" title=""Respawn Anchor""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-288px -1440px""></span><span class=""sprite-text"">Respawn Anchor</span></span></a></li>
<li><a href=""/Scaffolding"" title=""Scaffolding""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-240px -688px""></span><span class=""sprite-text"">Scaffolding</span></span></a></li>
<li><a href=""/Smithing_Table"" title=""Smithing Table""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-240px -672px""></span><span class=""sprite-text"">Smithing Table</span></span></a></li>
<li><a href=""/Smoker"" title=""Smoker""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-288px -672px""></span><span class=""sprite-text"">Smoker</span></span></a></li>
<li><a href=""/Stonecutter"" title=""Stonecutter""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-368px -672px""></span><span class=""sprite-text"">Stonecutter</span></span></a></li>
<li><a href=""/Shulker_Box"" title=""Shulker Box""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-224px -384px""></span><span class=""sprite-text"">Shulker Box</span></span></a></li>
<li><a href=""/Sign"" title=""Sign""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-320px -32px""></span><span class=""sprite-text"">Sign</span></span></a></li>
<li><a href=""/TNT"" title=""TNT""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-48px -1040px""></span><span class=""sprite-text"">TNT</span></span></a></li>
<li><a href=""/Torch"" title=""Torch""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-96px -1040px""></span><span class=""sprite-text"">Torch</span></span></a>
<ul><li><a href=""/Soul_Torch"" class=""mw-redirect"" title=""Soul Torch""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-256px -1392px""></span><span class=""sprite-text"">Soul</span></span></a></li></ul></li></ul>
<table>
<tbody><tr>
<th><a href=""/Bedrock_Edition"" title=""Bedrock Edition"">Bedrock Edition</a> only
</th>
<td>
<ul><li><a href=""/Item_Frame"" title=""Item Frame""><span class=""nowrap""><span class=""sprite item-sprite"" style=""background-image:url(/media/f/f5/ItemCSS.png?version=1597474691265);background-position:-0px -528px""></span><span class=""sprite-text"">Item Frame</span></span></a>
<ul><li>as a block</li></ul></li></ul>
</td></tr>
<tr>
<th><a href=""/Education_Edition"" title=""Education Edition""><i>Education Edition</i></a> only
</th>
<td>
<ul><li><a href=""/Chalkboard"" title=""Chalkboard""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-192px -416px""></span><span class=""sprite-text"">Chalkboard</span></span></a></li></ul>
</td></tr>
<tr>
<th><a href=""/BE"" class=""mw-redirect"" title=""BE"">BE</a> &amp; <i><a href=""/EE"" class=""mw-redirect"" title=""EE"">EE</a></i> only
</th>
<td>
<ul><li><a href=""/Compound_Creator"" title=""Compound Creator""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-272px -528px""></span><span class=""sprite-text"">Compound Creator</span></span></a></li>
<li><a href=""/Element_Constructor"" title=""Element Constructor""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-272px -560px""></span><span class=""sprite-text"">Element Constructor</span></span></a></li>
<li><a href=""/Lab_Table"" title=""Lab Table""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-0px -576px""></span><span class=""sprite-text"">Lab Table</span></span></a></li>
<li><a href=""/Material_Reducer"" title=""Material Reducer""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-80px -576px""></span><span class=""sprite-text"">Material Reducer</span></span></a></li>
<li><a href=""/Colored_Torch"" title=""Colored Torch""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-192px -656px""></span><span class=""sprite-text"">Colored Torches</span></span></a></li>
<li><a href=""/Underwater_Torch"" title=""Underwater Torch""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-176px -576px""></span><span class=""sprite-text"">Underwater Torch</span></span></a></li>
<li><a href=""/Underwater_TNT"" title=""Underwater TNT""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-80px -1392px""></span><span class=""sprite-text"">Underwater TNT</span></span></a></li></ul>
</td></tr></tbody></table>
</td></tr>
<tr>
<th><a href=""/Tutorials/Mechanisms"" title=""Tutorials/Mechanisms"">Mechanisms</a>
</th>
<td>
<ul><li><a href=""/Button"" title=""Button""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-304px -192px""></span><span class=""sprite-text"">Button</span></span></a></li>
<li><a href=""/Dispenser"" title=""Dispenser""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-256px -928px""></span><span class=""sprite-text"">Dispenser</span></span></a></li>
<li><a href=""/Daylight_Detector"" title=""Daylight Detector""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-144px -48px""></span><span class=""sprite-text"">Daylight Detector</span></span></a>
<ul><li><a href=""/Daylight_Sensor#As_a_night_time_detector"" class=""mw-redirect"" title=""Daylight Sensor""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-160px -32px""></span><span class=""sprite-text"">Inverted</span></span></a></li></ul></li>
<li><a href=""/Door"" title=""Door""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-192px -880px""></span><span class=""sprite-text"">Doors</span></span></a></li>
<li><a href=""/Dropper"" title=""Dropper""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-192px -944px""></span><span class=""sprite-text"">Dropper</span></span></a></li>
<li><a href=""/Fence_Gate"" title=""Fence Gate""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-288px -1168px""></span><span class=""sprite-text"">Fence Gates</span></span></a></li>
<li><a href=""/Hopper"" title=""Hopper""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-176px -1184px""></span><span class=""sprite-text"">Hopper</span></span></a></li>
<li><a href=""/Lever"" title=""Lever""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-288px -1184px""></span><span class=""sprite-text"">Lever</span></span></a></li>
<li><a href=""/Note_Block"" title=""Note Block""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-224px -1024px""></span><span class=""sprite-text"">Note Block</span></span></a></li>
<li><a href=""/Observer"" title=""Observer""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-304px -1024px""></span><span class=""sprite-text"">Observer</span></span></a></li>
<li><a href=""/Piston"" title=""Piston""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-32px -1200px""></span><span class=""sprite-text"">Piston</span></span></a>
<ul><li><a href=""/Piston"" title=""Piston""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-80px -1200px""></span><span class=""sprite-text"">Sticky</span></span></a></li>
<li><a href=""/Piston"" title=""Piston""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-32px -1232px""></span><span class=""sprite-text"">Head</span></span></a></li>
<li><a href=""/Piston"" title=""Piston""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-272px -128px""></span><span class=""sprite-text"">Moving</span></span></a></li></ul></li>
<li><a href=""/Pressure_Plate"" title=""Pressure Plate""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-304px -1168px""></span><span class=""sprite-text"">Pressure Plates</span></span></a></li>
<li><a href=""/Rail"" title=""Rail""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-224px -1200px""></span><span class=""sprite-text"">Rail</span></span></a>
<ul><li><a href=""/Activator_Rail"" title=""Activator Rail""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-240px -1168px""></span><span class=""sprite-text"">Activator</span></span></a></li>
<li><a href=""/Detector_Rail"" title=""Detector Rail""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-128px -1184px""></span><span class=""sprite-text"">Detector</span></span></a></li>
<li><a href=""/Powered_Rail"" title=""Powered Rail""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-176px -1200px""></span><span class=""sprite-text"">Powered</span></span></a></li></ul></li>
<li><a href=""/Redstone_Dust"" title=""Redstone Dust""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-64px -176px""></span><span class=""sprite-text"">Redstone Dust</span></span></a>
<ul><li><a href=""/Redstone_Comparator"" title=""Redstone Comparator""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-336px -1200px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-160px -1200px""></span><span class=""sprite-text"">Comparator</span></span></span></span></a></li>
<li><a href=""/Redstone_Lamp"" title=""Redstone Lamp""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-352px -1200px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-336px -1184px""></span><span class=""sprite-text"">Lamp</span></span></span></span></a></li>
<li><a href=""/Redstone_Repeater"" title=""Redstone Repeater""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-0px -1216px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-208px -1200px""></span><span class=""sprite-text"">Repeater</span></span></span></span></a></li>
<li><a href=""/Redstone_Torch"" title=""Redstone Torch""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-32px -1216px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-16px -1216px""></span><span class=""sprite-text"">Torch</span></span></span></span></a></li></ul></li>
<li><a href=""/Target"" title=""Target""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-32px -1440px""></span><span class=""sprite-text"">Target</span></span></a></li>
<li><a href=""/Trapdoor"" title=""Trapdoor""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-320px -1168px""></span><span class=""sprite-text"">Trapdoors</span></span></a></li>
<li><a href=""/Trapped_Chest"" title=""Trapped Chest""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-256px -176px""></span><span class=""sprite-text"">Trapped Chest</span></span></a></li>
<li><a href=""/Tripwire_Hook"" title=""Tripwire Hook""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-160px -1216px""></span><span class=""sprite-text"">Tripwire Hook</span></span></a></li>
<li><a href=""/String#Tripwire"" title=""String""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-288px -128px""></span><span class=""sprite-text"">Tripwire</span></span></a></li></ul>
</td></tr>
<tr>
<th><a href=""/Creative"" title=""Creative"">Creative</a> only
</th>
<td>
<ul><li><a href=""/Player_Head"" class=""mw-redirect"" title=""Player Head""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-320px -144px""></span><span class=""sprite-text"">Player Head</span></span></a></li></ul>
<table>
<tbody><tr>
<th><a href=""/Java_Edition"" title=""Java Edition""><i>Java Edition</i></a> only
</th>
<td>
<ul><li><a href=""/Petrified_Oak_Slab"" class=""mw-redirect"" title=""Petrified Oak Slab""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-48px -976px""></span><span class=""sprite-text"">Petrified Oak Slab</span></span></a></li></ul>
</td></tr>
<tr>
<th><a href=""/Bedrock_Edition"" title=""Bedrock Edition"">Bedrock Edition</a> only
</th>
<td>
<ul><li><a href=""/Monster_Spawner"" class=""mw-redirect"" title=""Monster Spawner""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-240px -1216px""></span><span class=""sprite-text"">Empty Monster Spawner</span></span></a></li></ul>
</td></tr>
<tr>
<th><a href=""/BE"" class=""mw-redirect"" title=""BE"">BE</a> &amp; <i><a href=""/EE"" class=""mw-redirect"" title=""EE"">EE</a></i> only
</th>
<td>
<ul><li><a href=""/Allow_and_Deny_Blocks"" class=""mw-redirect"" title=""Allow and Deny Blocks""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-128px -416px""></span><span class=""sprite-text"">Allow</span></span></a></li>
<li><a href=""/Border"" title=""Border""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-256px -288px""></span><span class=""sprite-text"">Border</span></span></a></li>
<li><a href=""/Allow_and_Deny_Blocks"" class=""mw-redirect"" title=""Allow and Deny Blocks""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-144px -416px""></span><span class=""sprite-text"">Deny</span></span></a></li></ul>
</td></tr></tbody></table>
</td></tr>
<tr>
<th><a href=""/Commands"" title=""Commands"">Commands</a> only
</th>
<td>
<ul><li><a href=""/Barrier"" title=""Barrier""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-304px -1216px""></span><span class=""sprite-text"">Barrier</span></span></a></li>
<li><a href=""/Command_Block"" title=""Command Block""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-64px -1184px""></span><span class=""sprite-text"">Command Block</span></span></a>
<ul><li><a href=""/Command_Block"" title=""Command Block""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-48px -1184px""></span><span class=""sprite-text"">Chain</span></span></a></li>
<li><a href=""/Command_Block"" title=""Command Block""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-48px -1216px""></span><span class=""sprite-text"">Repeating</span></span></a></li></ul></li>
<li><a href=""/Jigsaw_Block"" title=""Jigsaw Block""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-48px -768px""></span><span class=""sprite-text"">Jigsaw Block</span></span></a></li>
<li><a href=""/Structure_Block"" title=""Structure Block""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-192px -800px""></span><span class=""sprite-text"">Structure Block</span></span></a></li>
<li><a href=""/Structure_Void"" class=""mw-redirect"" title=""Structure Void""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-48px -1232px""></span><span class=""sprite-text"">Structure Void</span></span></a></li>
<li><a href=""/TNT"" title=""TNT""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-48px -1040px""></span><span class=""sprite-text"">Unstable TNT</span></span></a></li></ul>
<table>
<tbody><tr>
<th><a href=""/Java_Edition"" title=""Java Edition""><i>Java Edition</i></a> only
</th>
<td>
<ul><li><a href=""/Mob_head"" class=""mw-redirect"" title=""Mob head""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-320px -144px""></span><span class=""sprite-text"">Custom Player Head</span></span></a></li></ul>
</td></tr>
<tr>
<th><a href=""/Bedrock_Edition"" title=""Bedrock Edition"">Bedrock Edition</a> only
</th>
<td>
<ul><li><a href=""/Bedrock_Edition_unused_features#Purpur_Blocks"" title=""Bedrock Edition unused features""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-208px -944px""></span><span class=""sprite-text"">Chiseled Purpur</span></span></a></li>
<li><a href=""/Bedrock_Edition_unused_features#Fake_Wood_Slab"" title=""Bedrock Edition unused features""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-48px -976px""></span><span class=""sprite-text"">Fake Wood Slab</span></span></a></li>
<li><a href=""/Light_Block"" title=""Light Block""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-208px -1376px""></span><span class=""sprite-text"">Light Block</span></span></a></li>
<li><a href=""/Stone_Bricks#ID"" title=""Stone Bricks""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-176px -960px""></span><span class=""sprite-text"">Smooth Stone Bricks</span></span></a></li>
<li><a href=""/Bedrock_Edition_unused_features#Purpur_Blocks"" title=""Bedrock Edition unused features""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-208px -944px""></span><span class=""sprite-text"">tile.purpur_block.smooth.name</span></span></a></li></ul>
</td></tr>
<tr>
<th><a href=""/Education_Edition"" title=""Education Edition""><i>Education Edition</i></a> only
</th>
<td>
<ul><li><a href=""/Camera"" title=""Camera""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-272px -80px""></span><span class=""sprite-text"">Camera</span></span></a></li></ul>
</td></tr></tbody></table>
</td></tr>
<tr>
<th>Unused
</th>
<td>
<table>
<tbody><tr>
<th><a href=""/Bedrock_Edition"" title=""Bedrock Edition"">Bedrock Edition</a> only
</th>
<td>
<ul><li><a href=""/Bedrock_Edition_unused_features#Bell-less_Block"" title=""Bedrock Edition unused features""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-256px -1296px""></span><span class=""sprite-text"">Bell Stand</span></span></a></li>
<li><a href=""/Glowing_Obsidian"" title=""Glowing Obsidian""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-368px -160px""></span><span class=""sprite-text"">Glowing Obsidian</span></span></a></li>
<li><a href=""/Invisible_Bedrock"" title=""Invisible Bedrock""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-240px -16px""></span><span class=""sprite-text"">Invisible Bedrock</span></span></a></li>
<li><a href=""/Nether_Reactor_Core"" title=""Nether Reactor Core""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-352px -160px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-96px -896px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-80px -896px""></span><span class=""sprite-text"">Nether Reactor Core</span></span></span></span></span></span></a></li>
<li><a href=""/Bedrock_Edition_unused_features#Smokeless_Campfire"" title=""Bedrock Edition unused features""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-0px -800px""></span><span class=""sprite-text"">Smokeless Campfire</span></span></a></li>
<li><a href=""/Info_update"" title=""Info update""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-320px -176px""></span><span class=""sprite-text"">info_update</span></span></a></li>
<li><a href=""/Info_update"" title=""Info update""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-320px -176px""></span><span class=""sprite-text"">info_update2</span></span></a></li>
<li><a href=""/Reserved6"" title=""Reserved6""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-320px -176px""></span><span class=""sprite-text"">reserved6</span></span></a></li>
<li><a href=""/Stonecutter/old"" title=""Stonecutter/old""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-336px -192px""></span><span class=""sprite-text"">Stonecutter (Old)</span></span></a></li></ul>
</td></tr></tbody></table>
</td></tr>
<tr>
<th>Removed
</th>
<td>
<ul><li><a href=""/Hay_Bale#History"" title=""Hay Bale""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-144px -1152px""></span><span class=""sprite-text"">Hexahedral Hay Bale</span></span></a></li></ul>
<table>
<tbody><tr>
<th><a href=""/Java_Edition"" title=""Java Edition""><i>Java Edition</i></a> only
</th>
<td>
<ul><li><a href=""/Wool#History"" title=""Wool""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-272px -1280px""></span><span class=""sprite-text"">Cloth</span></span></a></li>
<li><a href=""/Java_Edition_removed_features#Cog"" title=""Java Edition removed features""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-352px -32px""></span><span class=""sprite-text"">Cog</span></span></a></li>
<li><a href=""/Coral_Block#History"" title=""Coral Block""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-208px -1232px""></span><span class=""sprite-text"">Dead Coral Block</span></span></a></li>
<li><a href=""/Java_Edition_mentioned_features#Dirt_slab"" title=""Java Edition mentioned features""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-256px -112px""></span><span class=""sprite-text"">Dirt Slab</span></span></a></li>
<li><a href=""/Jack_o%27Lantern#History"" title=""Jack o&#39;Lantern""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-368px -1152px""></span><span class=""sprite-text"">Faceless Jack o'Lantern</span></span></a></li>
<li><a href=""/Infinite_Lava_Source"" title=""Infinite Lava Source""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-176px -288px""></span><span class=""sprite-text"">Infinite Lava Source</span></span></a></li>
<li><a href=""/Infinite_Water_Source"" title=""Infinite Water Source""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-160px -288px""></span><span class=""sprite-text"">Infinite Water Source</span></span></a></li>
<li><a href=""/Locked_chest"" title=""Locked chest""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-304px -96px""></span><span class=""sprite-text"">Locked chest</span></span></a></li>
<li><a href=""/Melon_Seeds#History"" title=""Melon Seeds""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-240px -96px""></span><span class=""sprite-text"">Overripe Melon Stem</span></span></a></li>
<li><a href=""/Pumpkin_Seeds#History"" title=""Pumpkin Seeds""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-240px -96px""></span><span class=""sprite-text"">Overripe Pumpkin Stem</span></span></a></li>
<li><a href=""/Flower_Pot#History"" title=""Flower Pot""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-368px -240px""></span><span class=""sprite-text"">Potted non-flower block</span></span></a></li>
<li><a href=""/Flower"" title=""Flower""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-192px -0px""></span><span class=""sprite-text"">Rose</span></span></a>
<ul><li><a href=""/Flower_Pot#History"" title=""Flower Pot""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-224px -704px""></span><span class=""sprite-text"">Potted</span></span></a></li></ul></li>
<li><a href=""/Emerald_Ore#History"" title=""Emerald Ore""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-288px -112px""></span><span class=""sprite-text"">Ruby Ore</span></span></a></li>
<li><a href=""/Sponge#History"" title=""Sponge""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-0px -48px""></span><span class=""sprite-text"">Sponge (Classic)</span></span></a></li>
<li><a href=""/Grass"" title=""Grass""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-112px -48px""></span><span class=""sprite-text"">Shrub</span></span></a>
<ul><li><a href=""/Grass#History"" title=""Grass""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-112px -32px""></span><span class=""sprite-text"">Green</span></span></a></li></ul></li>
<li><a href=""/Piston"" title=""Piston""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-64px -1200px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-80px -1200px""></span><span class=""sprite-text"">Six-sided Piston</span></span></span></span></a></li></ul>
</td></tr>
<tr>
<th><a href=""/Bedrock_Edition"" title=""Bedrock Edition"">Bedrock Edition</a> only
</th>
<td>
<ul><li><a href=""/Flower"" title=""Flower""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-352px -64px""></span><span class=""sprite-text"">Cyan Flower</span></span></a></li>
<li><a href=""/Bell"" title=""Bell""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-240px -880px""></span><span class=""sprite-text"">Diorite Bell</span></span></a></li>
<li><a href=""/Bell"" title=""Bell""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-256px -880px""></span><span class=""sprite-text"">Granite Bell</span></span></a>
<ul><li><a href=""/Bell"" title=""Bell""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-272px -880px""></span><span class=""sprite-text"">Polished</span></span></a></li></ul></li>
<li><a href=""/Bone_Block#History"" title=""Bone Block""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-336px -816px""></span><span class=""sprite-text"">Hexahedral Bone Block</span></span></a></li>
<li><a href=""/Purpur_Block#History"" title=""Purpur Block""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-224px -944px""></span><span class=""sprite-text"">Hexahedral Purpur Pillar</span></span></a></li>
<li><a href=""/Block_of_Quartz#History"" title=""Block of Quartz""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-112px -1008px""></span><span class=""sprite-text"">Hexahedral Quartz Pillar</span></span></a></li></ul>
</td></tr></tbody></table>
</td></tr>
<tr>
<th><a href=""/Unimplemented_features"" title=""Unimplemented features"">Unimplemented</a>
</th>
<td>
<ul><li><a href=""/Java_Edition_mentioned_features#Branches"" title=""Java Edition mentioned features"">Branches</a></li>
<li><a href=""/Java_Edition_mentioned_features#Colored_wood_planks"" title=""Java Edition mentioned features""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-336px -800px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-16px -816px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-64px -816px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-80px -816px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-112px -816px""></span><span class=""sprite-text"">Colored Wood Planks</span></span></span></span></span></span></span></span></span></span></a></li>
<li><a href=""/Coral_Block#History"" title=""Coral Block""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-192px -1296px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-224px -1296px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-240px -1296px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-208px -1296px""></span><span class=""sprite-text"">Coral Slab</span></span></span></span></span></span></span></span></a></li>
<li><a href=""/Barrel#Gallery"" title=""Barrel""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-192px -1264px""></span><span class=""sprite-text"">Empty Barrel</span></span></a></li>
<li><a href=""/Barrel#Gallery"" title=""Barrel""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-224px -1232px""></span><span class=""sprite-text"">Fish Barrel</span></span></a></li>
<li><a href=""/Java_Edition_removed_features#Chairs_and_other_furniture"" title=""Java Edition removed features""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-240px -848px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-288px -848px""></span><span class=""sprite-text"">Furniture</span></span></span></span></a></li>
<li><a href=""/Java_Edition_removed_features#Paeonia"" title=""Java Edition removed features""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-192px -816px""></span><span class=""sprite-text"">Paeonia</span></span></a></li>
<li><a href=""/Java_Edition_mentioned_features#Spike_block"" title=""Java Edition mentioned features"">Spike Block</a></li>
<li><a href=""/Wax_Block"" class=""mw-redirect"" title=""Wax Block""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-16px -1280px""></span><span class=""sprite-text"">Wax Block</span></span></a></li></ul>
</td></tr>
<tr>
<th>Joke features
</th>
<td>
<ul><li><a href=""/20w14infinite#Blocks"" class=""mw-redirect"" title=""20w14infinite""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-144px -1488px""></span><span class=""sprite-text"">An Ant</span></span></a></li>
<li><a href=""/Java_Edition_2.0#Blocks"" title=""Java Edition 2.0""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-288px -880px""></span><span class=""sprite-text"">Block of Coal</span></span></a></li>
<li><a href=""/20w14infinite#Blocks"" class=""mw-redirect"" title=""20w14infinite""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-368px -240px""></span><span class=""sprite-text"">Box of Infinite Books</span></span></a></li>
<li><a href=""/Java_Edition_2.0#Blocks"" title=""Java Edition 2.0""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-80px -528px""></span><span class=""sprite-text"">Burnt-out Torch</span></span></a></li>
<li><a href=""/20w14infinite#Blocks"" class=""mw-redirect"" title=""20w14infinite""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-176px -1488px""></span><span class=""sprite-text"">Cursor</span></span></a></li>
<li><a href=""/Java_Edition_2.0#Blocks"" title=""Java Edition 2.0""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-96px -528px""></span><span class=""sprite-text"">Etho Slab</span></span></a></li>
<li><a href=""/20w14infinite#Blocks"" class=""mw-redirect"" title=""20w14infinite""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-368px -240px""></span><span class=""sprite-text"">Funky Portal</span></span></a></li>
<li><a href=""/20w14infinite#Blocks"" class=""mw-redirect"" title=""20w14infinite""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-240px -16px""></span><span class=""sprite-text"">Leftover</span></span></a></li>
<li><a href=""/20w14infinite#Blocks"" class=""mw-redirect"" title=""20w14infinite""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-192px -1488px""></span><span class=""sprite-text"">Swaggiest stairs ever</span></span></a></li>
<li><a href=""/Java_Edition_2.0#Blocks"" title=""Java Edition 2.0""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-208px -1264px""></span><span class=""sprite-text"">Tinted Glass</span></span></a></li>
<li><a href=""/Java_Edition_2.0#Blocks"" title=""Java Edition 2.0""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-80px -1296px""></span><span class=""sprite-text"">Tinted Glass Pane</span></span></a></li>
<li><a href=""/USB_Charger_Block"" title=""USB Charger Block""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-288px -288px""></span><span class=""sprite-text"">USB Charger Block</span></span></a></li></ul>
</td></tr></tbody></table>

<!-- 
NewPP limit report
Cached time: 20200916223106
Cache expiry: 86400
Dynamic content: false
CPU time usage: 0.659 seconds
Real time usage: 1.505 seconds
Preprocessor visited node count: 6739/1000000
Preprocessor generated node count: 26026/1000000
Post‐expand include size: 321948/2097152 bytes
Template argument size: 375/2097152 bytes
Highest expansion depth: 9/40
Expensive parser function count: 2/99
Unstrip recursion depth: 0/20
Unstrip post‐expand size: 0/5000000 bytes
ExtLoops count: 0
Lua time usage: 0.830/7 seconds
Lua virtual size: 9.49 MB/100 MB
Lua estimated memory usage: 0 bytes
-->
<!--
Transclusion expansion time report (%,ms,calls,template)
130.57% 1709.249    462 Template:BlockLink
100.00% 1309.021      1 -total
  3.92%   51.376     20 Template:BlockSprite
  2.22%   29.020      1 Template:ItemLink
  2.17%   28.387     11 Template:El
  0.28%    3.611      1 Template:SimpleNavbox
-->

<!-- Saved in parser cache with key minecraft_gamepedia:pcache:idhash:2475-0!canonical and timestamp 20200916223105 and revision id 1683616
 -->
</div></div>						<div class=""printfooter"">
							Retrieved from ""<a dir=""ltr"" href=""https://minecraft.gamepedia.com/index.php?title=Template:Blocks/content&amp;oldid=1683616"">https://minecraft.gamepedia.com/index.php?title=Template:Blocks/content&amp;oldid=1683616</a>""						</div>
						<div id=""catlinks"" class=""catlinks"" data-mw=""interface""><div id=""mw-normal-catlinks"" class=""mw-normal-catlinks""><a href=""/Special:Categories"" title=""Special:Categories"">Category</a>: <ul><li><a href=""/Category:Ajax_loaded_pages"" title=""Category:Ajax loaded pages"">Ajax loaded pages</a></li></ul></div><div id=""mw-hidden-catlinks"" class=""mw-hidden-catlinks mw-hidden-cats-hidden"">Hidden categories: <ul><li><a href=""/Category:Pages_using_DynamicPageList_dplreplace_parser_function"" title=""Category:Pages using DynamicPageList dplreplace parser function"">Pages using DynamicPageList dplreplace parser function</a></li><li><a href=""/Category:Pages_with_missing_sprites"" title=""Category:Pages with missing sprites"">Pages with missing sprites</a></li></ul></div></div>					<div class=""visualClear""></div>
									</div>
								<div id=""siderail_minecraft_gamepedia"">
					<div id='atfmrec_minecraft_gamepedia'><div id='cdm-zone-02'></div></div><div id='middlemrec_minecraft_gamepedia'><a href='https://www.gamepedia.com/PRO?utm_source=GPPromo&utm_medium=Rail'><img src='https://gamepedia.cursecdn.com/commons_hydra/c/c2/Pro_Promorail_v2.jpg'></a></div><div id='btfmrec_minecraft_gamepedia'><div id='cdm-zone-06'></div></div>				</div>
				<div class=""visualClear""></div>
					<div id=btflb><div id='cdm-zone-04'></div></div>			</div>
			<div id=""mw-navigation"">
				<h2>Navigation menu</h2>
				<div id=""mw-head"">
										<div id=""left-navigation"">
											<div id=""p-namespaces"" role=""navigation"" class=""vectorTabs"" aria-labelledby=""p-namespaces-label"">
						<h3 id=""p-namespaces-label"">Namespaces</h3>
						<ul>
							<li id=""ca-nstab-template"" class=""selected""><span><a href=""/Template:Blocks/content"" title=""View the template [c]"" accesskey=""c"">Template</a></span></li><li id=""ca-talk""><span><a href=""/Template_talk:Blocks/content"" rel=""discussion"" class=""mw-redirect"" title=""Talk about the content page [t]"" accesskey=""t"">Talk</a></span></li>						</ul>
					</div>
										<div id=""p-variants"" role=""navigation"" class=""vectorMenu emptyPortlet"" aria-labelledby=""p-variants-label"">
												<input type=""checkbox"" class=""vectorMenuCheckbox"" aria-labelledby=""p-variants-label"" />
						<h3 id=""p-variants-label"">
							<span>Variants</span>
						</h3>
						<div class=""menu"">
							<ul>
															</ul>
						</div>
					</div>
										<div id=""p-sharing"" role=""navigation"" class=""vectorMenu"" aria-labelledby=""p-sharing-label"">
						<input type=""checkbox"" class=""vectorMenuCheckbox"" aria-labelledby=""p-sharing-label"" />
						<h3 id=""p-sharing-label""><span>Share</span></h3>
						<div class=""menu"">
							
				<div id=""socialIconImages"">
					<a href=""https://twitter.com/intent/tweet?text=Check+out+Template%3ABlocks%2Fcontent+on+Minecraft+Wiki+--&amp;via=CurseGamepedia&amp;url=https%3A%2F%2Fminecraft.gamepedia.com%2FTemplate%3ABlocks%2Fcontent"" target=""_blank""><img src=""/extensions/Social/images/social/twitter.svg""/></a><a href=""https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fminecraft.gamepedia.com%2FTemplate%3ABlocks%2Fcontent"" target=""_blank""><img src=""/extensions/Social/images/social/facebook.svg""/></a><a href=""https://www.reddit.com/submit?url=https%3A%2F%2Fminecraft.gamepedia.com%2FTemplate%3ABlocks%2Fcontent&amp;title=Check+out+Template%3ABlocks%2Fcontent+on+Minecraft+Wiki+via+%40CurseGamepedia%3A+https%3A%2F%2Fminecraft.gamepedia.com%2FTemplate%3ABlocks%2Fcontent"" target=""_blank""><img src=""/extensions/Social/images/social/reddit.svg""/></a><a href=""https://www.tumblr.com/share/link?url=https%3A%2F%2Fminecraft.gamepedia.com%2FTemplate%3ABlocks%2Fcontent&amp;name=Minecraft+Wiki&amp;description=Check+out+Template%3ABlocks%2Fcontent+on+Minecraft+Wiki+via+%3Ca+href%3D%22https%3A%2F%2Ftwitter.com%2FCurseGamepedia%22+target%3D%22_blank%22%3E%40CurseGamepedia%3C%2Fa%3E%3A+https%3A%2F%2Fminecraft.gamepedia.com%2FTemplate%3ABlocks%2Fcontent"" target=""_blank""><img src=""/extensions/Social/images/social/tumblr.svg""/></a><a href=""https://vk.com/share.php?url=https%3A%2F%2Fminecraft.gamepedia.com%2FTemplate%3ABlocks%2Fcontent"" target=""_blank""><img src=""/extensions/Social/images/social/vk.svg""/></a>
				</div>
						</div>
					</div>
										</div>
					<div id=""right-navigation"">
											<div id=""p-views"" role=""navigation"" class=""vectorTabs"" aria-labelledby=""p-views-label"">
						<h3 id=""p-views-label"">Views</h3>
						<ul>
							<li id=""ca-view"" class=""collapsible selected""><span><a href=""/Template:Blocks/content"">View</a></span></li><li id=""ca-viewsource"" class=""collapsible""><span><a href=""/index.php?title=Template:Blocks/content&amp;action=edit"" title=""This page is protected.&#10;You can view its source [e]"" accesskey=""e"">View source</a></span></li><li id=""ca-history"" class=""collapsible""><span><a href=""/index.php?title=Template:Blocks/content&amp;action=history"" title=""Past revisions of this page [h]"" accesskey=""h"">History</a></span></li>						</ul>
					</div>
										<div id=""p-cactions"" role=""navigation"" class=""vectorMenu emptyPortlet"" aria-labelledby=""p-cactions-label"">
						<input type=""checkbox"" class=""vectorMenuCheckbox"" aria-labelledby=""p-cactions-label"" />
						<h3 id=""p-cactions-label""><span>More</span></h3>
						<div class=""menu"">
							<ul>
															</ul>
						</div>
					</div>
										<div id=""p-search"" role=""search"">
						<h3>
							<label for=""searchInput"">Search</label>
						</h3>
						<form action=""/index.php"" id=""searchform"">
							<div id=""simpleSearch"">
								<input type=""search"" name=""search"" placeholder=""Search Minecraft Wiki"" title=""Search Minecraft Wiki [f]"" accesskey=""f"" id=""searchInput""/><input type=""hidden"" value=""Special:Search"" name=""title""/><input type=""submit"" name=""fulltext"" value=""Search"" title=""Search the pages for this text"" id=""mw-searchButton"" class=""searchButton mw-fallbackSearchButton""/><input type=""submit"" name=""go"" value=""Go"" title=""Go to a page with this exact name if it exists"" id=""searchButton"" class=""searchButton""/>							</div>
						</form>
					</div>
										</div>
				</div>
				<div id=""mw-panel"">
					<div id=""p-logo"" role=""banner""><a class=""mw-wiki-logo"" href=""/Minecraft_Wiki"" title=""Visit the main page - Minecraft Wiki""></a></div>
							<div class=""portal"" role=""navigation"" id=""p-Minecraft_Wiki"" aria-labelledby=""p-Minecraft_Wiki-label"">
			<h3 id=""p-Minecraft_Wiki-label"">Minecraft Wiki</h3>
			<div class=""body"">
								<ul>
					<li id=""n-mainpage-description""><a href=""/Minecraft_Wiki"" title=""Visit the main page [z]"" accesskey=""z"">Main page</a></li><li id=""n-portal""><a href=""/Minecraft_Wiki:Community_portal"" title=""About the Minecraft Wiki, what you can do, where to find things"">Community portal</a></li><li id=""n-Projects""><a href=""/Minecraft_Wiki:Projects"" title=""View and create projects on the wiki"">Projects</a></li><li id=""n-Wiki-rules""><a href=""/Minecraft_Wiki:Wiki_rules"" title=""Rules to follow when editing the wiki"">Wiki rules</a></li><li id=""n-Style-guide""><a href=""/Minecraft_Wiki:Style_guide"" title=""Guidelines to format articles and keep content consistent"">Style guide</a></li><li id=""n-Sandbox""><a href=""/Minecraft_Wiki:Sandbox"" title=""Experiment with making edits here"">Sandbox</a></li><li id=""n-recentchanges""><a href=""/Special:RecentChanges"" title=""A list of recent changes in the wiki [r]"" accesskey=""r"">Recent changes</a></li><li id=""n-randompage""><a href=""/Special:RandomRootpage"" title=""Load a random page [x]"" accesskey=""x"">Random page</a></li><li id=""n-Admin-noticeboard""><a href=""/Minecraft_Wiki:Admin_noticeboard"" title=""Report issues requiring admin attention here"">Admin noticeboard</a></li><li id=""n-Directors-page""><a href=""/Minecraft_Wiki:Directors"" title=""List of admins for all of the language wikis"">Directors page</a></li><li id=""n-Discord-server""><a href=""/Minecraft_Wiki:Discord"">Discord server</a></li>				</ul>
							</div>
		</div>
			<div class=""portal"" role=""navigation"" id=""p-Games"" aria-labelledby=""p-Games-label"">
			<h3 id=""p-Games-label"">Games</h3>
			<div class=""body"">
								<ul>
					<li id=""n-Minecraft""><a href=""/Minecraft"">Minecraft</a></li><li id=""n-Minecraft-Earth""><a href=""/Minecraft_Earth"">Minecraft Earth</a></li><li id=""n-Minecraft-Dungeons""><a href=""/Minecraft_Dungeons"">Minecraft Dungeons</a></li>				</ul>
							</div>
		</div>
			<div class=""portal"" role=""navigation"" id=""p-Useful_pages"" aria-labelledby=""p-Useful_pages-label"">
			<h3 id=""p-Useful_pages-label"">Useful pages</h3>
			<div class=""body"">
								<ul>
					<li id=""n-Trading""><a href=""/Trading"" title=""Information about trading with villagers"">Trading</a></li><li id=""n-Brewing""><a href=""/Brewing"" title=""Information about brewing"">Brewing</a></li><li id=""n-Enchanting""><a href=""/Enchanting"" title=""Information about enchanting"">Enchanting</a></li><li id=""n-Mobs""><a href=""/Mob"" title=""Information about the various friendly and non-friendly creatures found in Minecraft"">Mobs</a></li><li id=""n-Blocks""><a href=""/Block"" title=""Detailed information on the various blocks available in Minecraft"">Blocks</a></li><li id=""n-Items""><a href=""/Item"" title=""Detailed information on the various items available in Minecraft"">Items</a></li><li id=""n-Crafting""><a href=""/Crafting"" title=""Information about crafting"">Crafting</a></li><li id=""n-Smelting""><a href=""/Smelting"" title=""Information about smelting"">Smelting</a></li><li id=""n-Tutorials""><a href=""/Tutorials"">Tutorials</a></li><li id=""n-Resource-pack""><a href=""/Resource_pack"" title=""Resource packs alter the look and feel of the game"">Resource pack</a></li><li id=""n-Redstone-circuit""><a href=""/Mechanics/Redstone/Circuit"" title=""Information about redstone circuits"">Redstone circuit</a></li>				</ul>
							</div>
		</div>
			<div class=""portal"" role=""navigation"" id=""p-Minecraft_links"" aria-labelledby=""p-Minecraft_links-label"">
			<h3 id=""p-Minecraft_links-label"">Minecraft links</h3>
			<div class=""body"">
								<ul>
					<li id=""n-Website""><a href=""https://minecraft.net/"" rel=""nofollow"" target=""_self"" title=""The Minecraft website"">Website</a></li><li id=""n-Minecraft-Discord""><a href=""https://discord.gg/minecraft"" rel=""nofollow"" target=""_self"" title=""Minecraft&#039;s Discord server"">Minecraft Discord</a></li><li id=""n-Support""><a href=""https://help.minecraft.net/"" rel=""nofollow"" target=""_self"" title=""Mojang support center"">Support</a></li><li id=""n-Bug-tracker""><a href=""https://bugs.mojang.com/"" rel=""nofollow"" target=""_self"" title=""Mojang&#039;s issue tracker"">Bug tracker</a></li><li id=""n-Feedback""><a href=""https://feedback.minecraft.net"" rel=""nofollow"" target=""_self"" title=""Minecraft feedback website"">Feedback</a></li><li id=""n-Twitter""><a href=""https://twitter.com/Minecraft"" rel=""nofollow"" target=""_self"" title=""Minecraft&#039;s Twitter page"">Twitter</a></li><li id=""n-Facebook""><a href=""https://www.facebook.com/minecraft"" rel=""nofollow"" target=""_self"" title=""Minecraft&#039;s Facebook page"">Facebook</a></li><li id=""n-YouTube""><a href=""https://www.youtube.com/minecraft"" rel=""nofollow"" target=""_self"" title=""Minecraft&#039;s YouTube channel"">YouTube</a></li><li id=""n-Minecraft-Twitch""><a href=""https://www.twitch.tv/minecraft"" rel=""nofollow"" target=""_self"" title=""Minecraft&#039;s Twitch channel"">Minecraft Twitch</a></li>				</ul>
							</div>
		</div>
			<div class=""portal"" role=""navigation"" id=""p-Gamepedia"" aria-labelledby=""p-Gamepedia-label"">
			<h3 id=""p-Gamepedia-label"">Gamepedia</h3>
			<div class=""body"">
								<ul>
					<li id=""n-gamepedia-support""><a href=""https://gamepedia.zendesk.com"" rel=""nofollow"" target=""_self"">Gamepedia support</a></li><li id=""n-bad-ad""><a href=""https://community.fandom.com/wiki/Help%3ABad_advertisements"" target=""_self"">Report a bad ad</a></li><li id=""n-gamepedia-help""><a href=""https://help.gamepedia.com/"">Help Wiki</a></li><li id=""n-gamepedia-contact""><a href=""https://help.gamepedia.com/How_to_contact_Gamepedia"">Contact us</a></li>				</ul>
							</div>
		</div>
			<div class=""portal"" role=""navigation"" id=""p-tb"" aria-labelledby=""p-tb-label"">
			<h3 id=""p-tb-label"">Tools</h3>
			<div class=""body"">
								<ul>
					<li id=""t-whatlinkshere""><a href=""/Special:WhatLinksHere/Template:Blocks/content"" title=""A list of all wiki pages that link here [j]"" accesskey=""j"">What links here</a></li><li id=""t-recentchangeslinked""><a href=""/Special:RecentChangesLinked/Template:Blocks/content"" rel=""nofollow"" title=""Recent changes in pages linked from this page [k]"" accesskey=""k"">Related changes</a></li><li id=""t-specialpages""><a href=""/Special:SpecialPages"" title=""A list of all special pages [q]"" accesskey=""q"">Special pages</a></li><li id=""t-print""><a href=""/index.php?title=Template:Blocks/content&amp;printable=yes"" rel=""alternate"" title=""Printable version of this page [p]"" accesskey=""p"">Printable version</a></li><li id=""t-permalink""><a href=""/index.php?title=Template:Blocks/content&amp;oldid=1683616"" title=""Permanent link to this revision of the page"">Permanent link</a></li><li id=""t-info""><a href=""/index.php?title=Template:Blocks/content&amp;action=info"" title=""More information about this page"">Page information</a></li>				</ul>
							</div>
		</div>
			<div class=""portal"" role=""navigation"" id=""p-lang"" aria-labelledby=""p-lang-label"">
			<h3 id=""p-lang-label"">In other languages</h3>
			<div class=""body"">
								<ul>
					<li class=""interlanguage-link interwiki-es""><a href=""//minecraft-es.gamepedia.com/Plantilla:Bloques/contenido"" title=""Plantilla:Bloques/contenido – español"" lang=""es"" hreflang=""es"" class=""interlanguage-link-target"">Español</a></li><li class=""interlanguage-link interwiki-fr""><a href=""//minecraft-fr.gamepedia.com/Mod%C3%A8le:Blocs/contenu"" title=""Modèle:Blocs/contenu – français"" lang=""fr"" hreflang=""fr"" class=""interlanguage-link-target"">Français</a></li><li class=""interlanguage-link interwiki-it""><a href=""//minecraft-it.gamepedia.com/Template:Blocchi/content"" title=""Template:Blocchi/content – italiano"" lang=""it"" hreflang=""it"" class=""interlanguage-link-target"">Italiano</a></li><li class=""interlanguage-link interwiki-ja""><a href=""//minecraft-ja.gamepedia.com/%E3%83%86%E3%83%B3%E3%83%97%E3%83%AC%E3%83%BC%E3%83%88:Blocks/content"" title=""テンプレート:Blocks/content – 日本語"" lang=""ja"" hreflang=""ja"" class=""interlanguage-link-target"">日本語</a></li><li class=""interlanguage-link interwiki-ko""><a href=""//minecraft-ko.gamepedia.com/%ED%8B%80:blocks/content"" title=""틀:blocks/content – 한국어"" lang=""ko"" hreflang=""ko"" class=""interlanguage-link-target"">한국어</a></li><li class=""interlanguage-link interwiki-nl""><a href=""//minecraft-nl.gamepedia.com/Template:Blokken/inhoud"" title=""Template:Blokken/inhoud – Nederlands"" lang=""nl"" hreflang=""nl"" class=""interlanguage-link-target"">Nederlands</a></li><li class=""interlanguage-link interwiki-tr""><a href=""//minecraft-tr.gamepedia.com/%C5%9Eablon:Blocks/content"" title=""Şablon:Blocks/content – Türkçe"" lang=""tr"" hreflang=""tr"" class=""interlanguage-link-target"">Türkçe</a></li><li class=""interlanguage-link interwiki-pl""><a href=""//minecraft-pl.gamepedia.com/Template:Bloki/zawarto%C5%9B%C4%87"" title=""Template:Bloki/zawartość – polski"" lang=""pl"" hreflang=""pl"" class=""interlanguage-link-target"">Polski</a></li><li class=""interlanguage-link interwiki-ru""><a href=""//minecraft-ru.gamepedia.com/%D0%A8%D0%B0%D0%B1%D0%BB%D0%BE%D0%BD:%D0%91%D0%BB%D0%BE%D0%BA%D0%B8/%D0%A1%D0%BE%D0%B4%D0%B5%D1%80%D0%B6%D0%B8%D0%BC%D0%BE%D0%B5"" title=""Шаблон:Блоки/Содержимое – русский"" lang=""ru"" hreflang=""ru"" class=""interlanguage-link-target"">Русский</a></li><li class=""interlanguage-link interwiki-zh""><a href=""//minecraft-zh.gamepedia.com/Template:Blocks/content"" title=""Template:Blocks/content – 中文"" lang=""zh"" hreflang=""zh"" class=""interlanguage-link-target"">中文</a></li>				</ul>
							</div>
		</div>
			<div class=""portal"" role=""navigation"" id=""p-socialProfiles"" aria-labelledby=""p-socialProfiles-label"">
			<h3 id=""p-socialProfiles-label""></h3>
			<div class=""body"">
				<div class='socialSidebar'>
						<script type='text/javascript'>
							document.getElementById('p-socialProfiles').className = 'portal persistent';
						</script>
						<div class='socialLink twitter'><a href=""https://twitter.com/CurseGamepedia"" target=""_blank""><img src=""/extensions/Social/images/social/twitter.svg"" width=""32""/></a></div><div class='socialLink facebook'><a href=""https://www.facebook.com/CurseGamepedia"" target=""_blank""><img src=""/extensions/Social/images/social/facebook.svg"" width=""32""/></a></div><div class='socialLink twitch'><a href=""https://www.twitch.tv/gamepedia/videos"" target=""_blank""><img src=""/extensions/Social/images/social/twitch.svg"" width=""32""/></a></div>
					</div>			</div>
		</div>
					</div>
			</div>
						<div id=""footer"" role=""contentinfo"">
								<ul id=""footer-info"">
										<li id=""footer-info-lastmod""> This page was last edited on 8 September 2020, at 10:52.</li>
											<li id=""footer-info-copyright"">Content is available under <a class=""external"" rel=""nofollow"" href=""//creativecommons.org/licenses/by-nc-sa/3.0/"">CC BY-NC-SA 3.0</a> unless otherwise noted.<br/>Game content and materials are trademarks and copyrights of their respective publisher and its licensors.  All rights reserved.<br />
This site is a part of Fandom, Inc. and is not affiliated with the game publisher.</li>
										</ul>
									<ul id=""footer-places"">
										<li id=""footer-places-about""><a href=""/Minecraft_Wiki:About"" title=""Minecraft Wiki:About"">About Minecraft Wiki</a></li>
											<li id=""footer-places-disclaimer""><a href=""/Minecraft_Wiki:General_disclaimer"" title=""Minecraft Wiki:General disclaimer"">Disclaimers</a></li>
											<li id=""footer-places-mobileview""><a href=""https://minecraft.gamepedia.com/index.php?title=Template:Blocks/content&amp;mobileaction=toggle_view_mobile"" class=""noprint stopMobileRedirectToggle"">Mobile view</a></li>
										</ul>
														<ul id=""footer-icons"" class=""noprint"">
												<li id=""footer-copyrightico"">
							<a href=""//creativecommons.org/licenses/by-nc-sa/3.0/"" target=""_self""><img src=""//i.creativecommons.org/l/by-nc-sa/3.0/88x31.png"" alt=""CC BY-NC-SA 3.0"" width=""88"" height=""31""/></a>						</li>
													<li id=""footer-poweredbyico"">
							<a href=""//www.mediawiki.org/"" target=""_self""><img src=""/resources/assets/poweredby_mediawiki_88x31.png"" alt=""Powered by MediaWiki"" srcset=""/resources/assets/poweredby_mediawiki_132x47.png 1.5x, /resources/assets/poweredby_mediawiki_176x62.png 2x"" width=""88"" height=""31""/></a><a href=""https://help.gamepedia.com/What_is_Hydra"" target=""_self""><img src=""/skins/Hydra/images/icons/poweredbyhydra.png"" alt=""Powered by Hydra"" width=""88"" height=""31""/></a>						</li>
												</ul>
									<div style=""clear: both;""></div>
			</div>
		</div>
		<div id=""footer-push""></div>
	</div>
		
<script>(window.RLQ=window.RLQ||[]).push(function(){mw.config.set({""wgPageParseReport"":{""limitreport"":{""cputime"":""0.659"",""walltime"":""1.505"",""ppvisitednodes"":{""value"":6739,""limit"":1000000},""ppgeneratednodes"":{""value"":26026,""limit"":1000000},""postexpandincludesize"":{""value"":321948,""limit"":2097152},""templateargumentsize"":{""value"":375,""limit"":2097152},""expansiondepth"":{""value"":9,""limit"":40},""expensivefunctioncount"":{""value"":2,""limit"":99},""unstrip-depth"":{""value"":0,""limit"":20},""unstrip-size"":{""value"":0,""limit"":5000000},""timingprofile"":[""130.57% 1709.249    462 Template:BlockLink"",""100.00% 1309.021      1 -total"",""  3.92%   51.376     20 Template:BlockSprite"",""  2.22%   29.020      1 Template:ItemLink"",""  2.17%   28.387     11 Template:El"",""  0.28%    3.611      1 Template:SimpleNavbox""]},""loops"":{""limitreport-count-unlimited"":[0]},""scribunto"":{""limitreport-timeusage"":{""value"":""0.830"",""limit"":""7""},""limitreport-virtmemusage"":{""value"":9953280,""limit"":104857600},""limitreport-estmemusage"":0},""cachereport"":{""timestamp"":""20200916223106"",""ttl"":86400,""transientcontent"":false}}});});</script>
<link href=""https://fonts.googleapis.com/css?family=Rubik:400,700&amp;display=swap&amp;subset=cyrillic,latin-ext"" rel=""stylesheet"">
<div id=""footer-and-prefooter"">
	<div id=""gamepedia-footer"">
		<div class=""footer-wrapper-gp"">
			<div class=""footer-box footer-logo"">
				<a href=""https://www.gamepedia.com"">
					<img src=""/skins/Hydra/images/footer/premium-logo-light.svg"" class=""footer-gp-logo""/>
				</a>
			</div>
			<div class=""footer-box footer-social"">
				<ul class=""social"">
					<li>
						<a href=""https://www.facebook.com/CurseGamepedia"" title=""Facebook""><svg width=""10"" height=""21"" xmlns=""http://www.w3.org/2000/svg""><path d=""M9.364531 3.5096969H7.651507c-1.370419 0-1.598822.7261441-1.598822 1.6943364v2.2994565h3.311846l-.342605 3.6307209H6.281088v9.3188503H2.85504v-9.3188503H0V7.5034898h2.85504V4.8409612C2.85504 1.8153604 4.568064 0 7.080499 0c1.142016 0 2.16983.121024 2.512435.121024v3.3886729h-.228403z"" fill-rule=""evenodd""/></svg></a>
					</li>
					<li>
						<a href=""https://twitter.com/CurseGamepedia"" title=""Twitter""><svg width=""23"" height=""18"" xmlns=""http://www.w3.org/2000/svg""><path d=""M19.822727 4.2972696v.5729693c0 5.8729351-4.518416 12.6053241-12.826471 12.6053241-2.623596 0-4.955681-.7162116-6.996256-2.0053925.437266 0 .728777.1432423 1.166043.1432423 2.040575 0 4.081149-.7162116 5.684458-1.8621501-1.894819 0-3.643883-1.2891809-4.226905-3.151331.291511 0 .583022.1432423.874532.1432423.437266 0 .437266 0 1.020288-.1432423-2.186331-.429727-4.08115-2.2918772-4.08115-4.440512 0 .429727 1.603309.429727 2.332086.5729693C1.603309 5.8729351.874532 4.5837542.874532 3.0080887c0-.8594539.291511-1.5756655.728777-2.2918771 2.18633 2.7216041 5.684458 4.4405119 9.328342 4.7269965-.145756-.4297269-.145756-.7162116-.145756-1.0026962C10.785895 2.0053925 12.82647 0 15.304311 0c1.311798 0 2.477841.429727 3.352373 1.4324232 1.020287-.2864846 1.894819-.5729693 2.769351-1.1459386-.437266 1.1459386-1.166042 1.8621502-1.894819 2.4351195.874532-.1432423 1.894819-.429727 2.623596-.7162116-.728777.8594539-1.457553 1.7189078-2.332085 2.2918771z"" fill-rule=""evenodd""/></svg></a>
					</li>
					<li>
						<a href=""https://youtube.com/CurseEntertainment"" title=""Youtube""><svg width=""24"" height=""17"" xmlns=""http://www.w3.org/2000/svg""><path d=""M23.8 3.6s-.2-1.7-1-2.4c-.9-1-1.9-1-2.4-1C17 0 12 0 12 0S7 0 3.6.2c-.5.1-1.5.1-2.4 1-.7.7-1 2.4-1 2.4S0 5.5 0 7.5v1.8c0 1.9.2 3.9.2 3.9s.2 1.7 1 2.4c.9 1 2.1.9 2.6 1 1.9.2 8.2.2 8.2.2s5 0 8.4-.3c.5-.1 1.5-.1 2.4-1 .7-.7 1-2.4 1-2.4s.2-1.9.2-3.9V7.4c0-1.9-.2-3.8-.2-3.8zM9.5 11.5V4.8L16 8.2l-6.5 3.3z"" fill-rule=""evenodd""/></svg></a>
					</li>
				</ul>
			</div>
			<div class=""footer-box footer-links mobile-split"">
				<ul>
					<li>
						<a href=""https://support.gamepedia.com"">Support</a>
					</li>
					<li>
						<a href=""https://help.gamepedia.com/How_to_contact_Gamepedia"">Contact</a>
					</li>
					<li>
						<a href=""https://www.gamepedia.com/pro"">PRO</a>
					</li>
				</ul>
			</div>
		</div>
	</div>

	<footer id=""curse-footer"" role=""complimentary"" class=""show-ads"">
		<div class=""footer-wrapper"">
						<div class=""footer-box footer-ad"">
				<div class=""ad-placement ad-main-med-rect-footer"">
					<div id='cdm-zone-03'></div>				</div>
			</div>
						<div class=""footer-box footer-logo"">
				<a href=""https://www.fandom.com"" target=""_blank""><img src=""/skins/Hydra/images/footer/fandom-logo.svg"" /></a>
			</div>
			<div class=""footer-box footer-properties"">
				<h2>Explore Properties</h2>
				<ul class=""properties mobile-split"">
					<li><a href=""https://www.fandom.com"">Fandom</a></li>
					<li><a href=""https://www.gamepedia.com"">Gamepedia</a></li>
					<li><a href=""https://www.dndbeyond.com"">D&amp;D Beyond</a></li>
					<li><a href=""https://www.muthead.com"">Muthead</a></li>
					<li><a href=""https://www.futhead.com"">Futhead</a></li>
								</ul>
			</div>
			<div class=""footer-box footer-social"">
				<h2>Follow Us</h2>
				<ul class=""social"">
					<li><a href=""https://www.facebook.com/getfandom"" title=""Facebook""><svg class=""icon"" xmlns=""http://www.w3.org/2000/svg"" width=""11"" height=""24"" viewBox=""0 0 11 24""><path d=""M10.7 4L8.8 4C7.2 4 6.9 4.9 6.9 6L6.9 8.6 10.7 8.6 10.3 12.8 7.2 12.8 7.2 23.5 3.3 23.5 3.3 12.8 0 12.8 0 8.6 3.3 8.6 3.3 5.6C3.3 2.1 5.2 0 8.1 0 9.4 0 10.6 0.1 11 0.1L11 4 10.7 4Z""/></svg></a></li>
					<li><a href=""https://twitter.com/getfandom"" title=""Twitter""><svg width=""24"" height=""19"" xmlns=""http://www.w3.org/2000/svg""><path d=""M20.957 4.543v.606c0 6.209-4.777 13.327-13.56 13.327-2.774 0-5.24-.758-7.397-2.12.462 0 .77.15 1.233.15 2.157 0 4.314-.756 6.01-1.968-2.004 0-3.853-1.363-4.47-3.332.309 0 .617.152.925.152.463 0 .463 0 1.079-.152C2.466 10.752.462 8.783.462 6.512c0 .454 1.695.454 2.466.606C1.695 6.209.925 4.846.925 3.18c0-.908.308-1.666.77-2.423 2.311 2.878 6.01 4.695 9.862 4.998-.154-.455-.154-.758-.154-1.06C11.403 2.12 13.56 0 16.18 0c1.387 0 2.62.454 3.544 1.514 1.079-.302 2.004-.605 2.928-1.211-.462 1.211-1.233 1.969-2.003 2.574a14.785 14.785 0 0 0 2.774-.757c-.77.909-1.541 1.817-2.466 2.423z"" fill-rule=""evenodd""/></svg></a></li>
					<li><a href=""https://www.youtube.com/fandomentertainment"" title=""Youtube""><svg width=""24"" height=""17"" xmlns=""http://www.w3.org/2000/svg""><path d=""M23.8 3.6s-.2-1.7-1-2.4c-.9-1-1.9-1-2.4-1C17 0 12 0 12 0S7 0 3.6.2c-.5.1-1.5.1-2.4 1-.7.7-1 2.4-1 2.4S0 5.5 0 7.5v1.8c0 1.9.2 3.9.2 3.9s.2 1.7 1 2.4c.9 1 2.1.9 2.6 1 1.9.2 8.2.2 8.2.2s5 0 8.4-.3c.5-.1 1.5-.1 2.4-1 .7-.7 1-2.4 1-2.4s.2-1.9.2-3.9V7.4c0-1.9-.2-3.8-.2-3.8zM9.5 11.5V4.8L16 8.2l-6.5 3.3z"" fill-rule=""evenodd"" /></svg></a></li>
					<li><a href=""https://www.instagram.com/getfandom/"" title=""Instagram""><svg width=""20"" height=""20"" xmlns=""http://www.w3.org/2000/svg""><path d=""M17.510373 0H2.406639C1.078838 0 0 1.0788382 0 2.3236515v15.1867219c0 1.3278009 1.078838 2.406639 2.406639 2.406639h15.186722C18.921162 19.9170124 20 18.8381743 20 17.593361V2.3236515C19.917012 1.0788382 18.838174 0 17.510373 0zm-2.572614 2.4896266h1.659751c.497926 0 .829876.3319502.829876.8298755v1.659751c0 .4979253-.33195.8298755-.829876.8298755h-1.659751c-.497925 0-.829875-.3319502-.829875-.8298755v-1.659751c0-.4979253.33195-.8298755.829875-.8298755zM9.958506 6.1410788c2.074689 0 3.817428 1.7427386 3.817428 3.8174274s-1.742739 3.8174274-3.817428 3.8174274c-2.074689 0-3.817427-1.7427386-3.817427-3.8174274s1.742738-3.8174274 3.817427-3.8174274zm6.639004 11.2863071H3.319502c-.497925 0-.829875-.3319502-.829875-.8298755V8.2987552h1.659751c-.248963.9128631-.331951 1.9917012-.082988 2.9875519.497925 2.3236514 2.406639 4.1493775 4.73029 4.5643153 3.900415.746888 7.302905-2.1576763 7.302905-5.8921162 0-.5809128-.165975-1.1618257-.248963-1.659751h1.659751v8.2987552c-.082987.4979253-.414937.8298755-.912863.8298755z"" fill-rule=""evenodd"" /></svg></a></li>
					<li><a href=""https://www.linkedin.com/company/fandomwikia/"" title=""LinkedIn""><svg width=""19"" height=""19"" xmlns=""http://www.w3.org/2000/svg""><path d=""M3.859375 19h-3.5625V5.9375h3.5625V19zM2.078125 4.43175C.931 4.43175 0 3.493625 0 2.337S.931.24225 2.078125.24225 4.15625 1.180375 4.15625 2.337s-.929812 2.09475-2.078125 2.09475zM18.109375 19h-3.5625v-6.65475c0-3.9995-4.75-3.6966875-4.75 0V19h-3.5625V5.9375h3.5625v2.0959375c1.65775-3.070875 8.3125-3.2976875 8.3125 2.94025V19z"" fill-rule=""nonzero"" /></svg></a></li>
				</ul>
			</div>
			<div class=""footer-box footer-overview"">
				<h2>Overview</h2>
				<ul class=""mobile-split"">
					<li><a href=""https://www.fandom.com/about"">About</a></li>
					<li><a href=""https://www.fandom.com/careers"">Careers</a></li>
					<li><a href=""https://www.fandom.com/press"">Press</a></li>
					<li><a href=""https://www.fandom.com/about#contact"">Contact Us</a></li>
					<li><a href=""https://www.fandom.com/terms-of-use"">Terms of Use</a></li>
					<li><a href=""https://www.fandom.com/privacy-policy"">Privacy Policy</a></li>
				</ul>
			</div>
			<div class=""footer-box footer-community"">
				<h2>Community</h2>
				<ul class=""mobile-split"">
					<li><a href=""https://community.fandom.com/wiki/Community_Central"">Community Central</a></li>
					<li><a href=""https://fandom.zendesk.com/hc/en-us"">Support</a></li>
					<li><a href=""https://community.fandom.com/wiki/Help:Contents"">Help</a></li>
					<li><a href=""https://www.gamepedia.com/do-not-sell-my-info"">Do Not Sell My Info</a></li>
				</ul>
			</div>
			<div class=""footer-box footer-advertise"">
				<h2>Advertise</h2>
				<ul class=""mobile-split"">
					<li><a href=""https://www.fandom.com/mediakit"">Media Kit</a></li>
					<li><a href=""https://www.fandom.com/mediakit#contact"">Contact Us</a></li>
				</ul>
			</div>
		</div>
		<div class=""footer-post"">
			Minecraft Wiki is a Fandom Gaming Community.			<hr />
			<span class=""footer-post-mobile""><a href=""https://minecraft.gamepedia.com/index.php?title=Template:Blocks/content&amp;mobileaction=toggle_view_mobile"">View Mobile Site</a></span>
		</div>
	</footer>
</div>

<!-- Begin comScore -->
<script>
var _comscore = _comscore || [];
_comscore.push({ c1: ""2"", c2: ""6035118"" });
(function() {
    var s = document.createElement(""script""), el = document.getElementsByTagName(""script"")[0]; s.async = true;
    s.src = (document.location.protocol == ""https:"" ? ""https://sb"" : ""http://b"") + "".scorecardresearch.com/beacon.js"";
    el.parentNode.insertBefore(s, el);
})();
</script>
<noscript>
<img src=""https://sb.scorecardresearch.com/p?c1=2&c2=6035118&cv=2.0&cj=1"" alt=""Tracking Pixel""/>
</noscript>
<!-- End comScore -->

<!-- Begin Nielsen -->
<script type=""text/javascript"">
(function () {
	var d = new Image(1, 1);
	d.onerror = d.onload = function () {
		d.onerror = d.onload = null;
	};
	d.src = [""//secure-us.imrworldwide.com/cgi-bin/m?ci=us-603339h&cg=0&cc=1&si="", escape(window.location.href), ""&rp="", escape(document.referrer), ""&ts=compact&rnd="", (new Date()).getTime()].join('');
})();
</script>
<noscript>
<div><img src=""//secure-us.imrworldwide.com/cgi-bin/m?ci=us-603339h&cg=0&cc=1&ts=noscript"" width=""1"" height=""1"" alt="""" /></div>
</noscript>
<!-- End Nielsen --><div id='cdm-zone-end'></div><!-- Start PubMP -->
<script type=""text/javascript"" src=""//ads.pubmatic.com/AdServer/js/showad.js#PIX&ptask=DSP&SPug=1""></script> 
<!-- End PubMP -->
			<script type=""text/javascript"">
				window.genreCategory = 'Sandbox';
				window.wikiTags = [""building"",""crafting"",""game:minecraft"",""gathering"",""linux"",""mac"",""mining"",""mojang"",""online"",""pc"",""ps3"",""ps4"",""survival"",""xbox 360"",""xbox one""];
			</script>
<script>(window.RLQ=window.RLQ||[]).push(function(){mw.config.set({""wgBackendResponseTime"":1602});});</script>
	</body>
</html>
		
";

            var parseHTML = HtmlExtensions.Parse(longHTML);
        }

        [TestMethod]
        public void OrganizeHTML()
        {
            var longHTML = @"<!DOCTYPE html>
<html class=""client-nojs"" lang=""en"" dir=""ltr"">
<head>
<meta charset=""UTF-8""/>
<title>Template:Blocks/content – Official Minecraft Wiki</title>
<script>document.documentElement.className = document.documentElement.className.replace( /(^|\s)client-nojs(\s|$)/, ""$1client-js$2"" );</script>
<script>(window.RLQ=window.RLQ||[]).push(function(){mw.config.set({""wgCanonicalNamespace"":""Template"",""wgCanonicalSpecialPageName"":false,""wgNamespaceNumber"":10,""wgPageName"":""Template:Blocks/content"",""wgTitle"":""Blocks/content"",""wgCurRevisionId"":1683616,""wgRevisionId"":1683616,""wgArticleId"":2475,""wgIsArticle"":true,""wgIsRedirect"":false,""wgAction"":""view"",""wgUserName"":null,""wgUserGroups"":[""*""],""wgCategories"":[""Pages using DynamicPageList dplreplace parser function"",""Pages with missing sprites"",""Ajax loaded pages""],""wgBreakFrames"":true,""wgPageContentLanguage"":""en"",""wgPageContentModel"":""wikitext"",""wgSeparatorTransformTable"":["""",""""],""wgDigitTransformTable"":["""",""""],""wgDefaultDateFormat"":""dmy"",""wgMonthNames"":["""",""January"",""February"",""March"",""April"",""May"",""June"",""July"",""August"",""September"",""October"",""November"",""December""],""wgMonthNamesShort"":["""",""Jan"",""Feb"",""Mar"",""Apr"",""May"",""Jun"",""Jul"",""Aug"",""Sep"",""Oct"",""Nov"",""Dec""],""wgRelevantPageName"":""Template:Blocks/content"",""wgRelevantArticleId"":2475,""wgRequestId"":""6d6808885b4893e385258973"",""wgCSPNonce"":false,""wgIsProbablyEditable"":false,""wgRelevantPageIsProbablyEditable"":false,""wgRestrictionEdit"":[""autoconfirmed""],""wgRestrictionMove"":[""autoconfirmed""],""dsSiteKey"":""aa746ead3a810cdf7dd11b4779c53009"",""wgMFDisplayWikibaseDescriptions"":{""search"":false,""nearby"":false,""watchlist"":false,""tagline"":false},""wgCollapsibleVectorEnabledModules"":{""collapsiblenav"":true,""experiments"":true},""wgVisualEditor"":{""pageLanguageCode"":""en"",""pageLanguageDir"":""ltr"",""pageVariantFallbacks"":""en"",""usePageImages"":true,""usePageDescriptions"":false},""wgVisualEditorToolbarScrollOffset"":0,""wgVisualEditorUnsupportedEditParams"":[""undo"",""undoafter"",""veswitched""],""wgEditSubmitButtonLabelPublish"":false});mw.loader.state({""ext.gadget.dungeonsWiki"":""ready"",""ext.gadget.earthWiki"":""ready"",""ext.gadget.site-styles"":""ready"",""ext.gadget.sound-styles"":""ready"",""site.styles"":""ready"",""noscript"":""ready"",""user.styles"":""ready"",""user"":""ready"",""user.options"":""ready"",""user.tokens"":""loading"",""mediawiki.legacy.shared"":""ready"",""mediawiki.legacy.commonPrint"":""ready"",""ext.siteGlobals.styles"":""ready"",""ext.visualEditor.desktopArticleTarget.noscript"":""ready"",""skins.vector.styles.responsive"":""ready"",""ext.social.styles"":""ready"",""mediawiki.skinning.interface"":""ready"",""skins.vector.styles"":""ready"",""skins.z.hydra.light.styles"":""ready"",""skins.hydra.googlefont.styles"":""ready"",""skins.hydra.netbar"":""ready"",""skins.hydra.footer"":""ready"",""skins.hydra.advertisements.styles"":""ready""});mw.loader.implement(""user.tokens@0tffind"",function($,jQuery,require,module){/*@nomin*/mw.user.tokens.set({""editToken"":""+\\"",""patrolToken"":""+\\"",""watchToken"":""+\\"",""csrfToken"":""+\\""});
});RLPAGEMODULES=[""ext.helios.logout.scripts"",""site"",""mediawiki.page.startup"",""mediawiki.page.ready"",""mediawiki.searchSuggest"",""ext.track.scripts"",""ext.crusher.tables"",""ext.gadget.refTooltip"",""ext.gadget.site"",""ext.gadget.sound"",""ext.gadget.spriteEditLoader"",""ext.gadget.purge"",""ext.gadget.protectionLocks"",""ext.gadget.ImageForeignUseCheck"",""ext.siteGlobals.scripts"",""ext.collapsiblevector.collapsibleNav"",""ext.visualEditor.desktopArticleTarget.init"",""ext.visualEditor.targetLoader"",""skins.vector.js"",""skins.hydra.advertisements.js"",""skins.hydra.footer.js"",""ext.social.scripts""];mw.loader.load(RLPAGEMODULES);});</script>
<link rel=""stylesheet"" href=""/load.php?lang=en&amp;modules=ext.siteGlobals.styles%7Cext.social.styles%7Cext.visualEditor.desktopArticleTarget.noscript%7Cmediawiki.legacy.commonPrint%2Cshared%7Cmediawiki.skinning.interface%7Cskins.hydra.advertisements.styles%7Cskins.hydra.footer%2Cnetbar%7Cskins.hydra.googlefont.styles%7Cskins.vector.styles%7Cskins.vector.styles.responsive%7Cskins.z.hydra.light.styles&amp;only=styles&amp;skin=hydra""/>
<script async="""" src=""/load.php?lang=en&amp;modules=startup&amp;only=scripts&amp;skin=hydra""></script>
<meta name=""ResourceLoaderDynamicStyles"" content=""""/>
<link rel=""stylesheet"" href=""/load.php?lang=en&amp;modules=ext.gadget.dungeonsWiki%2CearthWiki%2Csite-styles%2Csound-styles&amp;only=styles&amp;skin=hydra""/>
<link rel=""stylesheet"" href=""/load.php?lang=en&amp;modules=site.styles&amp;only=styles&amp;skin=hydra""/>
<noscript><link rel=""stylesheet"" href=""/load.php?lang=en&amp;modules=noscript&amp;only=styles&amp;skin=hydra""/></noscript>
<meta name=""generator"" content=""MediaWiki 1.33.3""/>
<meta name=""description""/>
<meta name=""keywords"" content=""""/>
<meta property=""og:title"" content=""Template:Blocks/content""/>
<meta property=""og:type"" content=""website""/>
<meta property=""og:image"" content=""https://minecraft.gamepedia.com/media/minecraft.gamepedia.com/b/bc/Wiki.png?version=8dcd2d0ecba9fbe6ecb884e9d0c11e3f""/>
<meta property=""og:url"" content=""https://minecraft.gamepedia.com/Template:Blocks/content""/>
<meta property=""og:site_name"" content=""Minecraft Wiki""/>
<meta name=""viewport"" content=""width=device-width, initial-scale=1""/>
<link rel=""apple-touch-icon"" href=""""/>
<link rel=""shortcut icon"" href=""https://minecraft.gamepedia.com/media/minecraft.gamepedia.com/6/64/Favicon.ico?version=ed57d32751506dd14279b7164eb8e848&amp;version=ed57d32751506dd14279b7164eb8e848""/>
<link rel=""search"" type=""application/opensearchdescription+xml"" href=""/opensearch_desc.php"" title=""Minecraft Wiki (en)""/>
<link rel=""EditURI"" type=""application/rsd+xml"" href=""https://minecraft.gamepedia.com/api.php?action=rsd""/>
<link rel=""license"" href=""//creativecommons.org/licenses/by-nc-sa/3.0/""/>
<link rel=""alternate"" type=""application/atom+xml"" title=""Minecraft Wiki Atom feed"" href=""/index.php?title=Special:RecentChanges&amp;feed=atom""/>
<link rel=""canonical"" href=""https://minecraft.gamepedia.com/Template:Blocks/content""/>
<meta itemprop=""author"" content=""Gamepedia"" />
<!--[if lt IE 9]><script src=""/load.php?lang=en&amp;modules=html5shiv&amp;only=scripts&amp;skin=hydra&amp;sync=1""></script><![endif]-->
</head>
<body class=""mediawiki ltr sitedir-ltr mw-hide-empty-elt ns-10 ns-subject page-Template_Blocks_content rootpage-Template_Blocks skin-hydra action-view site-minecraft-gamepedia show-ads"">
<script type=""application/ld+json"">
{
    ""@context"": ""http://schema.org/"",
    ""@type"": ""Article"",
    ""name"": ""Template:Blocks/content"",
    ""headline"": ""Template:Blocks/content"",
    ""image"": {
        ""@type"": ""ImageObject"",
        ""url"": ""https://static.wikia.nocookie.net/minecraft_gamepedia/images/b/bc/Wiki.png/revision/latest?cb=20200503182357?version=8dcd2d0ecba9fbe6ecb884e9d0c11e3f"",
        ""width"": ""160"",
        ""height"": ""132""
    },
    ""author"": {
        ""@type"": ""Organization"",
        ""name"": ""Minecraft Wiki""
    },
    ""publisher"": {
        ""@type"": ""Organization"",
        ""name"": ""Gamepedia"",
        ""logo"": {
            ""@type"": ""ImageObject"",
            ""url"": ""https://minecraft.gamepedia.com/skins/Hydra/images/icons/gp-pub-logo.png""
        },
        ""sameAs"": [
            ""https://twitter.com/CurseGamepedia"", ""https://www.facebook.com/CurseGamepedia"", ""https://www.twitch.tv/gamepedia/videos""
        ]
    },
    ""potentialAction"": {
        ""@type"": ""SearchAction"",
        ""target"": ""https://minecraft.gamepedia.com/index.php?title=Special:Search&search={search_term}"",
        ""query-input"": ""required name=search_term""
    },
    ""datePublished"": ""2020-09-08T10:52:46Z"",
    ""dateModified"": ""2020-09-08T10:52:46Z"",
    ""mainEntityOfPage"": ""https://minecraft.gamepedia.com/Template:Blocks/content""
}
</script>
<script>
var version = 'v76.3.3';
var host = 'https://static.wikia.nocookie.net/fandom-ae-assets/platforms/' + version + '/gamepedia';
var jsScript = document.createElement('script');
var cssLink = document.createElement('link');
 
jsScript.id = 'ae3.bundle';
jsScript.src = host + '/main.bundle.js';
jsScript.async = true;
jsScript.type = 'text/javascript';
 
cssLink.id = 'ae3.styles';
cssLink.href = host + '/styles.css';
cssLink.type = 'text/css';
cssLink.rel = 'stylesheet';
 
document.head.appendChild(jsScript);
document.head.appendChild(cssLink);
 
if (Math.max(document.documentElement.clientHeight, window.innerHeight || 0) < 800) {
    document.addEventListener(""DOMContentLoaded"", function(event) {
        var element = document.getElementById(""cdm-zone-06"");
        if (element) {
            element.parentNode.removeChild(element);
        }
    });
}
 
// LEGACY PART
 
window.isMobileResponsive = false;
window.factorem = window.factorem || {};
window.factorem.slotSizes =  [[[728,90],[980,150],[980,250],[970,150],[970,250]],[[300,250],[300,600]],[[300,250]],[[728,90]],[],[[300,250]],[],[]];
factorem.moatFrequency=""1.0"";
factorem.sandboxPolicy=""mobile"";
</script>

<!-- Google Tag Manager K -->
<script type=""text/javascript"">(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-NR933TZ');</script>
<!-- End Google Tag Manager K -->

<script type=""text/javascript"">!function(){var t=window.SambaTV=window.SambaTV||[];if(!t.track){if(t.invoked)return void(window.console&&window.console.error&&window.console.error(""Samba Metrics snippet included twice.""));t.invoked=!0,t.methods=[""track"",""Impression"",""Purchase"",""Register"",""Click"",""Login"",""Register""],t.factory=function(e){return function(){var r=Array.prototype.slice.call(arguments);return r.unshift(e),t.push(r),t}};for(var e=0;e<t.methods.length;e++){var r=t.methods[e];t[r]=t.factory(r)}t.load=function(t){var e=document.createElement(""script"");e.type=""text/javascript"",e.async=!0,e.src=(""https:""===document.location.protocol?""https://"":""http://"")+""tag.mtrcs.samba.tv/v3/tag/""+t+""/sambaTag.js"";var r=document.getElementsByTagName(""script"")[0];r.parentNode.insertBefore(e,r)},t.attrs||(t.attrs={}),t.SNIPPET_VERSION=""1.0.1"",t.load(""wikia/fandom-gamepedia"")}}();</script>
<script>
window.wgAdDriverPagesWithoutAds = [
  ""Tutorials/Mob_farm""
];
</script>	<script type=""text/javascript"">
		(function(i, s, o, g, r, a, m) {
			i['GoogleAnalyticsObject'] = r;
			i[r] = i[r] || function() {
				(i[r].q = i[r].q || []).push(arguments)
			}

			, i[r].l = 1 * new Date();
			a = s.createElement(o),
				m = s.getElementsByTagName(o)[0];
			a.async = 1;
			a.src = g;
			m.parentNode.insertBefore(a, m)
		})(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');
		ga('create', 'UA-35871056-4', 'auto', 'tracker0', []);
		ga('create', 'UA-1045810-36', 'auto', 'tracker1', []);
		ga('tracker0.send', 'pageview');

		if (window.cdnprovider) {
			ga(
				'tracker1.send',
				'pageview',
				{
					'dimension1':  window.cdnprovider
				}
			);
		} else {
			ga('tracker1.send', 'pageview');
		}

	</script>

<div id=""netbar"">
	<div class=""netbar-flex"">
		<div class=""netbar-box left logo""><a href=""https://www.gamepedia.com"">Gamepedia</a></div>
				<div class=""netbar-box left""><a href=""https://support.gamepedia.com/"">Help</a></div>
												<div class=""netbar-box left officialwiki""><a href=""/index.php?title=Special:AllSites&amp;filter=official""><img src=""/skins/Hydra/images/netbar/official-wiki.svg"" width=""90""></a></div>
						<div class=""netbar-spacer"">&nbsp;</div>
						<div class=""netbar-box right""><a href=""https://fandomauth.gamepedia.com/signin?redirect=https%3A%2F%2Fminecraft.gamepedia.com%2FTemplate%3ABlocks%2Fcontent"" id=""login-link"" class=""aqua-link"">Sign In</a></div>
					<div class=""netbar-box right""><a href=""https://fandomauth.gamepedia.com/register?redirect=https%3A%2F%2Fminecraft.gamepedia.com%2FTemplate%3ABlocks%2Fcontent"" id=""register-link"" class=""aqua-link"">Register</a></div>
						</div>
</div>
	<div id=""global-wrapper"" class=""with-siderail"">
		<div id=""mw-page-base"" class=""noprint""></div>
		<div id=""mw-head-base"" class=""noprint""></div>
		<div id=""pageWrapper"">
			<div id=""content"" class=""mw-body"" role=""main"" itemprop=""articleBody"">
				<a id=""top""></a>
								<!-- ATF Leaderboard -->
								<div id=""atflb"">
					<div id='cdm-zone-01'></div>				</div>
								<!-- /ATF Leaderboard -->
				<div class=""mw-indicators mw-body-content"">
</div>
<h1 id=""firstHeading"" class=""firstHeading"" lang=""en"" itemprop=""name"">Template:Blocks/content</h1>				<div id=""bodyContent"" class=""mw-body-content"">
					<div id=""siteSub"" class=""noprint"">From Minecraft Wiki</div>					<div id=""contentSub""><span class=""subpages"">&lt; <a href=""/Template:Blocks"" title=""Template:Blocks"">Template:Blocks</a></span></div>
										<div id=""jump-to-nav"" class=""mw-jump"">
						Jump to:						<a href=""#mw-head"">navigation</a>, 						<a href=""#p-search"">search</a>
					</div>
					<div id=""mw-content-text"" lang=""en"" dir=""ltr"" class=""mw-content-ltr""><div class=""mw-parser-output""><table class=""navbox hlist collapsible"">
<tbody><tr>
<th class=""navbox-top"" colspan=""2""><span class=""navbox-title""><a href=""/Block"" title=""Block"">Blocks</a></span>
</th></tr>
<tr>
<th>Natural
</th>
<td>
<ul><li><a href=""/Bedrock"" title=""Bedrock""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-320px -816px""></span><span class=""sprite-text"">Bedrock</span></span></a></li>
<li><a href=""/Gravel"" title=""Gravel""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-208px -832px""></span><span class=""sprite-text"">Gravel</span></span></a></li>
<li><a href=""/Magma_Block"" title=""Magma Block""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-240px -832px""></span><span class=""sprite-text"">Magma Block</span></span></a></li>
<li><a href=""/Obsidian"" title=""Obsidian""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-320px -832px""></span><span class=""sprite-text"">Obsidian</span></span></a></li></ul>
<table>
<tbody><tr>
<th>Overworld
</th>
<td>
<ul><li><a href=""/Clay_(block)"" class=""mw-redirect"" title=""Clay (block)""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-336px -864px""></span><span class=""sprite-text"">Clay</span></span></a>
<ul><li><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-144px -880px""></span> <span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-320px -224px""></span> <span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-304px -224px""></span> <span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-304px -240px""></span> <span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-368px -208px""></span> <a href=""/Terracotta"" title=""Terracotta""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-336px -224px""></span><span class=""sprite-text"">Terracotta</span></span></a></li></ul></li>
<li><a href=""/Coarse_Dirt"" title=""Coarse Dirt""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-368px -304px""></span><span class=""sprite-text"">Coarse Dirt</span></span></a></li>
<li><a href=""/Dirt"" title=""Dirt""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-32px -0px""></span><span class=""sprite-text"">Dirt</span></span></a></li>
<li><a href=""/Grass_Block"" title=""Grass Block""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-192px -832px""></span><span class=""sprite-text"">Grass Block</span></span></a></li>
<li><a href=""/Ice"" title=""Ice""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-224px -832px""></span><span class=""sprite-text"">Ice</span></span></a>
<ul><li><a href=""/Packed_Ice"" title=""Packed Ice""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-336px -832px""></span><span class=""sprite-text"">Packed</span></span></a></li>
<li><a href=""/Blue_Ice"" title=""Blue Ice""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-0px -528px""></span><span class=""sprite-text"">Blue</span></span></a></li></ul></li>
<li><a href=""/Mycelium"" title=""Mycelium""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-272px -832px""></span><span class=""sprite-text"">Mycelium</span></span></a></li>
<li><a href=""/Podzol"" title=""Podzol""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-256px -256px""></span><span class=""sprite-text"">Podzol</span></span></a></li>
<li><a href=""/Sand"" title=""Sand""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-80px -848px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-16px -848px""></span><span class=""sprite-text"">Sand</span></span></span></span></a></li>
<li><a href=""/Sandstone"" title=""Sandstone""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-112px -848px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-48px -848px""></span><span class=""sprite-text"">Sandstone</span></span></span></span></a></li>
<li><a href=""/Snow"" title=""Snow""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-176px -848px""></span><span class=""sprite-text"">Snow</span></span></a>
<ul><li><a href=""/Snow_Block"" title=""Snow Block""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-160px -848px""></span><span class=""sprite-text"">Block</span></span></a></li></ul></li>
<li><a href=""/Stone"" title=""Stone""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-288px -816px""></span><span class=""sprite-text"">Stone</span></span></a>
<ul><li><a href=""/Granite"" title=""Granite""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-176px -832px""></span><span class=""sprite-text"">Granite</span></span></a></li>
<li><a href=""/Diorite"" title=""Diorite""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-96px -832px""></span><span class=""sprite-text"">Diorite</span></span></a></li>
<li><a href=""/Andesite"" title=""Andesite""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-304px -816px""></span><span class=""sprite-text"">Andesite</span></span></a></li></ul></li></ul>
<table>
<tbody><tr>
<th>Ore
</th>
<td>
<ul><li><a href=""/Coal_Ore"" title=""Coal Ore""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-320px -912px""></span><span class=""sprite-text"">Coal Ore</span></span></a></li>
<li><a href=""/Diamond_Ore"" title=""Diamond Ore""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-336px -912px""></span><span class=""sprite-text"">Diamond Ore</span></span></a></li>
<li><a href=""/Emerald_Ore"" title=""Emerald Ore""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-352px -912px""></span><span class=""sprite-text"">Emerald Ore</span></span></a></li>
<li><a href=""/Gold_Ore"" title=""Gold Ore""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-368px -912px""></span><span class=""sprite-text"">Gold Ore</span></span></a></li>
<li><a href=""/Iron_Ore"" title=""Iron Ore""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-0px -928px""></span><span class=""sprite-text"">Iron Ore</span></span></a></li>
<li><a href=""/Lapis_Lazuli_Ore"" title=""Lapis Lazuli Ore""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-16px -928px""></span><span class=""sprite-text"">Lapis Lazuli Ore</span></span></a></li>
<li><a href=""/Redstone_Ore"" title=""Redstone Ore""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-48px -928px""></span><span class=""sprite-text"">Redstone Ore</span></span></a></li></ul>
</td></tr></tbody></table>
</td></tr>
<tr>
<th><a href=""/Liquid"" title=""Liquid"">Liquids</a>
</th>
<td>
<ul><li><a href=""/Bubble_Column"" title=""Bubble Column""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-128px -528px""></span><span class=""sprite-text"">Bubble Column</span></span></a></li>
<li><a href=""/Lava"" title=""Lava""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-336px -48px""></span><span class=""sprite-text"">Lava</span></span></a></li>
<li><a href=""/Water"" title=""Water""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-352px -48px""></span><span class=""sprite-text"">Water</span></span></a></li></ul>
</td></tr>
<tr>
<th>Gases
</th>
<td>
<ul><li><a href=""/Air"" title=""Air""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-240px -16px""></span><span class=""sprite-text"">Air</span></span></a></li></ul>
<table>
<tbody><tr>
<th><a href=""/Java_Edition"" title=""Java Edition""><i>Java Edition</i></a> only
</th>
<td>
<ul><li><a href=""/Cave_Air"" class=""mw-redirect"" title=""Cave Air""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-240px -16px""></span><span class=""sprite-text"">Cave Air</span></span></a></li>
<li><a href=""/Void_Air"" class=""mw-redirect"" title=""Void Air""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-240px -16px""></span><span class=""sprite-text"">Void Air</span></span></a></li></ul>
</td></tr></tbody></table>
</td></tr>
<tr>
<th><a href=""/The_Nether"" title=""The Nether"">The Nether</a>
</th>
<td>
<ul><li><a href=""/Basalt"" title=""Basalt""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-320px -1392px""></span><span class=""sprite-text"">Basalt</span></span></a></li>
<li><a href=""/Blackstone"" title=""Blackstone""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-208px -1456px""></span><span class=""sprite-text"">Blackstone</span></span></a></li>
<li><a href=""/Glowstone"" title=""Glowstone""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-160px -832px""></span><span class=""sprite-text"">Glowstone</span></span></a></li>
<li><a href=""/Netherrack"" title=""Netherrack""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-304px -832px""></span><span class=""sprite-text"">Netherrack</span></span></a></li>
<li><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-272px -1392px""></span> <a href=""/Nylium"" title=""Nylium""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-0px -1408px""></span><span class=""sprite-text"">Nylium</span></span></a></li>
<li><a href=""/Soul_Fire"" class=""mw-redirect"" title=""Soul Fire""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-352px -1424px""></span><span class=""sprite-text"">Soul Fire</span></span></a></li>
<li><a href=""/Soul_Sand"" title=""Soul Sand""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-208px -848px""></span><span class=""sprite-text"">Soul Sand</span></span></a></li>
<li><a href=""/Soul_Soil"" title=""Soul Soil""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-368px -1392px""></span><span class=""sprite-text"">Soul Soil</span></span></a></li></ul>
<table>
<tbody><tr>
<th>Ore
</th>
<td>
<ul><li><a href=""/Ancient_Debris"" title=""Ancient Debris""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-128px -1392px""></span><span class=""sprite-text"">Ancient Debris</span></span></a></li>
<li><a href=""/Nether_Gold_Ore"" title=""Nether Gold Ore""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-144px -1456px""></span><span class=""sprite-text"">Nether Gold Ore</span></span></a></li>
<li><a href=""/Nether_Quartz_Ore"" title=""Nether Quartz Ore""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-32px -928px""></span><span class=""sprite-text"">Nether Quartz Ore</span></span></a></li></ul>
</td></tr></tbody></table>
</td></tr>
<tr>
<th><a href=""/The_End"" title=""The End"">The End</a>
</th>
<td>
<ul><li><a href=""/End_Stone"" title=""End Stone""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-128px -832px""></span><span class=""sprite-text"">End Stone</span></span></a></li></ul>
</td></tr></tbody></table>
</td></tr>
<tr>
<th><a href=""/Structures"" class=""mw-redirect"" title=""Structures"">Structures</a>
</th>
<td>
<ul><li><a href=""/Bone_Block"" title=""Bone Block""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-336px -816px""></span><span class=""sprite-text"">Bone Block</span></span></a></li>
<li><a href=""/Crying_Obsidian"" title=""Crying Obsidian""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-64px -1440px""></span><span class=""sprite-text"">Crying Obsidian</span></span></a></li>
<li><a href=""/Iron_Bars"" title=""Iron Bars""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-224px -896px""></span><span class=""sprite-text"">Iron Bars</span></span></a></li></ul>
<table>
<tbody><tr>
<th><a href=""/Overworld"" title=""Overworld"">Overworld</a>
</th>
<td>
<ul><li><a href=""/Banner"" title=""Banner""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-96px -1120px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-96px -320px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-320px -704px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-352px -704px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-0px -720px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-48px -720px""></span><span class=""sprite-text"">Banner</span></span></span></span></span></span></span></span></span></span></span></span></a></li>
<li><a href=""/Bee_Nest"" class=""mw-redirect"" title=""Bee Nest""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-320px -1264px""></span><span class=""sprite-text"">Bee Nest</span></span></a></li>
<li><a href=""/Bookshelf"" title=""Bookshelf""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-272px -928px""></span><span class=""sprite-text"">Bookshelf</span></span></a></li>
<li><a href=""/Bricks"" title=""Bricks""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-288px -928px""></span><span class=""sprite-text"">Bricks</span></span></a></li>
<li><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-160px -352px""></span> <span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-0px -736px""></span> <span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-16px -768px""></span> <span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-48px -736px""></span> <a href=""/Carpet"" title=""Carpet""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-80px -736px""></span><span class=""sprite-text"">Carpet</span></span></a></li>
<li><a href=""/Carved_Pumpkin"" class=""mw-redirect"" title=""Carved Pumpkin""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-0px -1152px""></span><span class=""sprite-text"">Carved Pumpkin</span></span></a></li>
<li><a href=""/Cobblestone"" title=""Cobblestone""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-32px -832px""></span><span class=""sprite-text"">Cobblestone</span></span></a></li>
<li><a href=""/Mossy_Cobblestone"" title=""Mossy Cobblestone""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-256px -832px""></span><span class=""sprite-text"">Mossy Cobblestone</span></span></a></li>
<li><a href=""/Wall"" title=""Wall""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-208px -784px""></span><span class=""sprite-text"">Walls</span></span></a></li>
<li><a href=""/Cobweb"" title=""Cobweb""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-48px -832px""></span><span class=""sprite-text"">Cobweb</span></span></a></li>
<li><a href=""/End_Portal_Frame"" title=""End Portal Frame""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-320px -1216px""></span><span class=""sprite-text"">End Portal Frame</span></span></a></li>
<li><a href=""/Oak_Fence"" class=""mw-redirect"" title=""Oak Fence""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-48px -960px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-336px -944px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-64px -960px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-0px -960px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-304px -944px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-368px -944px""></span><span class=""sprite-text"">Fences</span></span></span></span></span></span></span></span></span></span></span></span></a></li>
<li><a href=""/Fire"" title=""Fire""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-304px -48px""></span><span class=""sprite-text"">Fire</span></span></a></li>
<li><a href=""/Flower_Pot"" title=""Flower Pot""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-192px -896px""></span><span class=""sprite-text"">Flower Pot</span></span></a></li>
<li><a href=""/Glass"" title=""Glass""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-32px -1008px""></span><span class=""sprite-text"">Glass</span></span></a>
<ul><li><a href=""/Glass_Pane"" title=""Glass Pane""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-64px -144px""></span><span class=""sprite-text"">Pane</span></span></a></li></ul></li>
<li><a href=""/Grass_Path"" title=""Grass Path""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-272px -320px""></span><span class=""sprite-text"">Grass Path</span></span></a></li>
<li><a href=""/Glazed_Terracotta"" title=""Glazed Terracotta""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-112px -448px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-64px -448px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-32px -448px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-144px -448px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-0px -448px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-48px -448px""></span><span class=""sprite-text"">Glazed Terracotta</span></span></span></span></span></span></span></span></span></span></span></span></a></li>
<li><a href=""/Hay_Bale"" title=""Hay Bale""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-144px -1152px""></span><span class=""sprite-text"">Hay Bale</span></span></a></li>
<li><a href=""/Infested_Block"" title=""Infested Block""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-336px -1440px""></span><span class=""sprite-text"">Infested Block</span></span></a></li>
<li><a href=""/Jack_o%27Lantern"" title=""Jack o&#39;Lantern""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-176px -1152px""></span><span class=""sprite-text"">Jack o'Lantern</span></span></a></li>
<li><a href=""/Ladder"" title=""Ladder""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-240px -896px""></span><span class=""sprite-text"">Ladder</span></span></a></li>
<li><a href=""/Planks"" title=""Planks""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-224px -928px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-224px -848px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-240px -928px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-208px -928px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-176px -928px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-192px -928px""></span><span class=""sprite-text"">Planks</span></span></span></span></span></span></span></span></span></span></span></span></a></li>
<li><a href=""/Andesite"" title=""Andesite""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-304px -896px""></span><span class=""sprite-text"">Polished Andesite</span></span></a></li>
<li><a href=""/Diorite"" title=""Diorite""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-320px -896px""></span><span class=""sprite-text"">Polished Diorite</span></span></a></li>
<li><a href=""/Granite"" title=""Granite""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-336px -896px""></span><span class=""sprite-text"">Polished Granite</span></span></a></li>
<li><a href=""/Prismarine"" title=""Prismarine""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-0px -848px""></span><span class=""sprite-text"">Prismarine</span></span></a>
<ul><li><a href=""/Prismarine"" title=""Prismarine""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-208px -960px""></span><span class=""sprite-text"">Bricks</span></span></a></li>
<li><a href=""/Prismarine"" title=""Prismarine""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-128px -960px""></span><span class=""sprite-text"">Dark</span></span></a></li></ul></li>
<li><a href=""/Sea_Lantern"" title=""Sea Lantern""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-0px -320px""></span><span class=""sprite-text"">Sea Lantern</span></span></a></li>
<li><a href=""/Sandstone"" title=""Sandstone""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-80px -832px""></span><span class=""sprite-text"">Cut Sandstone</span></span></a>
<ul><li><a href=""/Sandstone"" title=""Sandstone""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-0px -832px""></span><span class=""sprite-text"">Chiseled</span></span></a></li>
<li><a href=""/Sandstone"" title=""Sandstone""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-128px -848px""></span><span class=""sprite-text"">Smooth</span></span></a></li></ul></li>
<li><a href=""/Smooth_Stone"" title=""Smooth Stone""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-288px -944px""></span><span class=""sprite-text"">Smooth Stone</span></span></a></li>
<li><a href=""/Spawner"" title=""Spawner""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-240px -1216px""></span><span class=""sprite-text"">Spawner</span></span></a>
<ul><li><a href=""/Spawner#Natural_generation"" title=""Spawner""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-112px -1232px""></span><span class=""sprite-text"">Cave Spider</span></span></a></li>
<li><a href=""/Spawner#Natural_generation"" title=""Spawner""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-144px -1232px""></span><span class=""sprite-text"">Silverfish</span></span></a></li>
<li><a href=""/Spawner#Natural_generation"" title=""Spawner""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-160px -1232px""></span><span class=""sprite-text"">Skeleton</span></span></a></li>
<li><a href=""/Spawner#Natural_generation"" title=""Spawner""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-176px -1232px""></span><span class=""sprite-text"">Spider</span></span></a></li>
<li><a href=""/Spawner#Natural_generation"" title=""Spawner""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-192px -1232px""></span><span class=""sprite-text"">Zombie</span></span></a></li></ul></li>
<li><a href=""/Sponge"" title=""Sponge""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-0px -1040px""></span><span class=""sprite-text"">Sponge</span></span></a>
<ul><li><a href=""/Sponge"" title=""Sponge""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-112px -1040px""></span><span class=""sprite-text"">Wet</span></span></a></li></ul></li>
<li><a href=""/Stained_Glass_Pane"" class=""mw-redirect"" title=""Stained Glass Pane""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-256px -368px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-80px -368px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-288px -368px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-64px -368px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-128px -368px""></span><span class=""sprite-text"">Stained Glass Pane</span></span></span></span></span></span></span></span></span></span></a></li>
<li><a href=""/Stone_Bricks"" title=""Stone Bricks""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-176px -960px""></span><span class=""sprite-text"">Stone Bricks</span></span></a>
<ul><li><a href=""/Stone_Bricks"" title=""Stone Bricks""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-112px -960px""></span><span class=""sprite-text"">Cracked</span></span></a></li>
<li><a href=""/Stone_Bricks"" title=""Stone Bricks""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-160px -960px""></span><span class=""sprite-text"">Mossy</span></span></a></li>
<li><a href=""/Stone_Bricks"" title=""Stone Bricks""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-96px -960px""></span><span class=""sprite-text"">Chiseled</span></span></a></li></ul></li>
<li><a href=""/Log"" title=""Log""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-80px -560px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-320px -544px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-128px -560px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-32px -560px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-272px -544px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-368px -544px""></span><span class=""sprite-text"">Stripped Log</span></span></span></span></span></span></span></span></span></span></span></span></a></li>
<li><a href=""/Wood"" title=""Wood""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-112px -864px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-352px -848px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-160px -864px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-64px -864px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-304px -848px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-16px -864px""></span><span class=""sprite-text"">Wood</span></span></span></span></span></span></span></span></span></span></span></span></a>
<ul><li><a href=""/Wood"" title=""Wood""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-80px -560px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-320px -544px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-128px -560px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-32px -560px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-272px -544px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-368px -544px""></span><span class=""sprite-text"">Stripped</span></span></span></span></span></span></span></span></span></span></span></span></a></li></ul></li>
<li><a href=""/Wool"" title=""Wool""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-0px -64px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-32px -176px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-32px -144px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-16px -128px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-32px -160px""></span><span class=""sprite-text"">Wool</span></span></span></span></span></span></span></span></span></span></a></li></ul>
</td></tr>
<tr>
<th><a href=""/The_Nether"" title=""The Nether"">The Nether</a>
</th>
<td>
<ul><li><a href=""/Chain"" title=""Chain""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-80px -1472px""></span><span class=""sprite-text"">Chain</span></span></a></li>
<li><a href=""/Gilded_Blackstone"" title=""Gilded Blackstone""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-368px -1456px""></span><span class=""sprite-text"">Gilded Blackstone</span></span></a></li>
<li><a href=""/Nether_Bricks"" title=""Nether Bricks""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-192px -960px""></span><span class=""sprite-text"">Nether Bricks</span></span></a>
<ul><li><a href=""/Fence"" title=""Fence""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-32px -960px""></span><span class=""sprite-text"">Fence</span></span></a></li></ul></li>
<li><a href=""/Nether_Portal_(block)"" title=""Nether Portal (block)""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-320px -16px""></span><span class=""sprite-text"">Nether Portal</span></span></a></li>
<li><a href=""/Polished_Basalt"" class=""mw-redirect"" title=""Polished Basalt""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-192px -1440px""></span><span class=""sprite-text"">Polished Basalt</span></span></a></li>
<li><a href=""/Polished_Blackstone"" title=""Polished Blackstone""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-304px -1456px""></span><span class=""sprite-text"">Polished Blackstone</span></span></a>
<ul><li><a href=""/Polished_Blackstone_Bricks"" title=""Polished Blackstone Bricks""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-48px -1472px""></span><span class=""sprite-text"">Bricks</span></span></a></li></ul></li>
<li><a href=""/Spawner"" title=""Spawner""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-240px -1216px""></span><span class=""sprite-text"">Spawner</span></span></a>
<ul><li><a href=""/Spawner#Natural_generation"" title=""Spawner""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-96px -1232px""></span><span class=""sprite-text"">Blaze</span></span></a></li>
<li><a href=""/Spawner#Natural_generation"" title=""Spawner""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-96px -1472px""></span><span class=""sprite-text"">Magma Cube</span></span></a></li></ul></li></ul>
</td></tr>
<tr>
<th><a href=""/The_End"" title=""The End"">The End</a>
</th>
<td>
<ul><li><a href=""/Banner"" title=""Banner""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-80px -720px""></span><span class=""sprite-text"">Banner</span></span></a></li>
<li><a href=""/Dragon_Egg"" title=""Dragon Egg""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-112px -160px""></span><span class=""sprite-text"">Dragon Egg</span></span></a></li>
<li><a href=""/End_Gateway_(block)"" title=""End Gateway (block)""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-368px -80px""></span><span class=""sprite-text"">End Gateway</span></span></a></li>
<li><a href=""/End_Portal_(block)"" title=""End Portal (block)""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-368px -80px""></span><span class=""sprite-text"">End Portal</span></span></a></li>
<li><a href=""/End_Rod"" title=""End Rod""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-208px -1216px""></span><span class=""sprite-text"">End Rod</span></span></a></li>
<li><a href=""/End_Stone_Bricks"" title=""End Stone Bricks""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-144px -960px""></span><span class=""sprite-text"">End Stone Bricks</span></span></a></li>
<li><a href=""/Purpur_Block"" title=""Purpur Block""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-208px -944px""></span><span class=""sprite-text"">Purpur Block</span></span></a>
<ul><li><a href=""/Purpur_Block"" title=""Purpur Block""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-224px -944px""></span><span class=""sprite-text"">Pillar</span></span></a></li></ul></li>
<li><a href=""/Stained_Glass"" class=""mw-redirect"" title=""Stained Glass""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-208px -912px""></span><span class=""sprite-text"">Stained Glass</span></span></a></li></ul>
</td></tr></tbody></table>
</td></tr>
<tr>
<th><a href=""/Plants"" title=""Plants"">Flora and fauna</a>
</th>
<td>
<table>
<tbody><tr>
<th>Plants
</th>
<td>
<table>
<tbody><tr>
<th><a href=""/Overworld"" title=""Overworld"">Overworld</a>
</th>
<td>
<ul><li><a href=""/Bamboo"" title=""Bamboo""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-32px -672px""></span><span class=""sprite-text"">Bamboo</span></span></a>
<ul><li><a href=""/Bamboo_Sapling"" class=""mw-redirect"" title=""Bamboo Sapling""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-288px -656px""></span><span class=""sprite-text"">Sapling</span></span></a></li></ul></li>
<li><a href=""/Beetroots"" class=""mw-redirect"" title=""Beetroots""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-224px -1136px""></span><span class=""sprite-text"">Beetroots</span></span></a></li>
<li><a href=""/Cactus"" title=""Cactus""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-48px -944px""></span><span class=""sprite-text"">Cactus</span></span></a></li>
<li><a href=""/Carrots"" class=""mw-redirect"" title=""Carrots""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-320px -1136px""></span><span class=""sprite-text"">Carrots</span></span></a></li>
<li><a href=""/Cocoa"" class=""mw-redirect"" title=""Cocoa""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-64px -1152px""></span><span class=""sprite-text"">Cocoa</span></span></a></li>
<li><a href=""/Dead_Bush"" title=""Dead Bush""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-16px -1120px""></span><span class=""sprite-text"">Dead Bush</span></span></a></li>
<li><a href=""/Fern"" class=""mw-redirect"" title=""Fern""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-32px -1120px""></span><span class=""sprite-text"">Fern</span></span></a>
<ul><li><a href=""/Grass"" title=""Grass""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-64px -1120px""></span><span class=""sprite-text"">Large</span></span></a></li></ul></li>
<li><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-192px -1120px""></span> <span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-304px -1120px""></span> <span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-336px -1120px""></span> <a href=""/Flowers"" class=""mw-redirect"" title=""Flowers""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-336px -656px""></span><span class=""sprite-text"">Flowers</span></span></a></li>
<li><a href=""/Grass"" title=""Grass""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-48px -1120px""></span><span class=""sprite-text"">Grass</span></span></a>
<ul><li><a href=""/Grass"" title=""Grass""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-112px -1120px""></span><span class=""sprite-text"">Tall</span></span></a></li></ul></li>
<li><a href=""/Kelp"" title=""Kelp""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-320px -368px""></span><span class=""sprite-text"">Kelp</span></span></a>
<ul><li><a href=""/Dried_Kelp_Block"" title=""Dried Kelp Block""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-16px -304px""></span><span class=""sprite-text"">Dried, Block</span></span></a></li></ul></li>
<li><a href=""/Leaves"" title=""Leaves""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-0px -1248px""></span><span class=""sprite-text"">Leaves</span></span></a></li>
<li><a href=""/Lily_Pad"" title=""Lily Pad""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-80px -1120px""></span><span class=""sprite-text"">Lily Pad</span></span></a></li>
<li><a href=""/Log"" title=""Log""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-112px -864px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-352px -848px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-160px -864px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-64px -864px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-304px -848px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-16px -864px""></span><span class=""sprite-text"">Log</span></span></span></span></span></span></span></span></span></span></span></span></a></li>
<li><a href=""/Melon"" title=""Melon""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-192px -1152px""></span><span class=""sprite-text"">Melon</span></span></a>
<ul><li><a href=""/Melon_Stem"" class=""mw-redirect"" title=""Melon Stem""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-240px -96px""></span><span class=""sprite-text"">Stem</span></span></a></li></ul></li>
<li><a href=""/Potatoes"" class=""mw-redirect"" title=""Potatoes""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-304px -1152px""></span><span class=""sprite-text"">Potatoes</span></span></a></li>
<li><a href=""/Pumpkin"" title=""Pumpkin""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-368px -1152px""></span><span class=""sprite-text"">Pumpkin</span></span></a>
<ul><li><a href=""/Pumpkin_Stem"" class=""mw-redirect"" title=""Pumpkin Stem""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-240px -96px""></span><span class=""sprite-text"">Stem</span></span></a></li></ul></li>
<li><a href=""/Sapling"" title=""Sapling""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-160px -944px""></span><span class=""sprite-text"">Sapling</span></span></a></li>
<li><a href=""/Seagrass"" title=""Seagrass""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-48px -304px""></span><span class=""sprite-text"">Seagrass</span></span></a>
<ul><li><a href=""/Seagrass"" title=""Seagrass""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-336px -368px""></span><span class=""sprite-text"">Tall</span></span></a></li></ul></li>
<li><a href=""/Sugar_Cane"" title=""Sugar Cane""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-48px -1168px""></span><span class=""sprite-text"">Sugar Cane</span></span></a></li>
<li><a href=""/Sweet_Berry_Bush"" class=""mw-redirect"" title=""Sweet Berry Bush""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-320px -784px""></span><span class=""sprite-text"">Sweet Berry Bush</span></span></a></li>
<li><a href=""/Vines"" title=""Vines""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-128px -1120px""></span><span class=""sprite-text"">Vines</span></span></a></li>
<li><a href=""/Wheat_Seeds"" title=""Wheat Seeds""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-64px -1168px""></span><span class=""sprite-text"">Wheat</span></span></a></li></ul>
</td></tr>
<tr>
<th><a href=""/The_End"" title=""The End"">The End</a>
</th>
<td>
<ul><li><a href=""/Chorus_Plant"" title=""Chorus Plant""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-48px -1152px""></span><span class=""sprite-text"">Chorus Plant</span></span></a>
<ul><li><a href=""/Chorus_Flower"" title=""Chorus Flower""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-16px -1152px""></span><span class=""sprite-text"">Flower</span></span></a></li></ul></li></ul>
</td></tr></tbody></table>
</td></tr>
<tr>
<th>Animals
</th>
<td>
<table>
<tbody><tr>
<td>
<ul><li><a href=""/Turtle_Egg"" title=""Turtle Egg""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-128px -544px""></span><span class=""sprite-text"">Turtle Egg</span></span></a></li>
<li><a href=""/Coral"" title=""Coral""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-192px -304px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-32px -368px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-352px -368px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-0px -384px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-16px -384px""></span><span class=""sprite-text"">Coral</span></span></span></span></span></span></span></span></span></span></a>
<ul><li><a href=""/Coral"" title=""Coral""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-208px -656px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-224px -656px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-240px -656px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-256px -656px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-272px -656px""></span><span class=""sprite-text"">Dead</span></span></span></span></span></span></span></span></span></span></a></li></ul></li>
<li><a href=""/Coral_Block"" title=""Coral Block""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-64px -304px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-224px -304px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-240px -304px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-256px -304px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-0px -336px""></span><span class=""sprite-text"">Coral Block</span></span></span></span></span></span></span></span></span></span></a>
<ul><li><a href=""/Coral_Block"" title=""Coral Block""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-0px -352px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-16px -352px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-32px -352px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-0px -368px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-16px -368px""></span><span class=""sprite-text"">Dead</span></span></span></span></span></span></span></span></span></span></a></li></ul></li>
<li><a href=""/Coral_Fan"" title=""Coral Fan""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-288px -48px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-192px -128px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-304px -160px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-144px -176px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-192px -224px""></span><span class=""sprite-text"">Coral Fan</span></span></span></span></span></span></span></span></span></span></a>
<ul><li><a href=""/Coral_Fan"" title=""Coral Fan""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-240px -560px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-192px -560px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-176px -560px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-208px -560px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-224px -560px""></span><span class=""sprite-text"">Dead</span></span></span></span></span></span></span></span></span></span></a></li></ul></li>
<li><a href=""/Sea_Pickle"" title=""Sea Pickle""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-112px -528px""></span><span class=""sprite-text"">Sea Pickle</span></span></a></li></ul>
</td></tr></tbody></table>
</td></tr>
<tr>
<th>Fungi
</th>
<td>
<table>
<tbody><tr>
<th><a href=""/Overworld"" title=""Overworld"">Overworld</a>
</th>
<td>
<ul><li><a href=""/Mushrooms"" class=""mw-redirect"" title=""Mushrooms""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-240px -1152px""></span><span class=""sprite-text"">Mushrooms</span></span></a>
<ul><li><a href=""/Mushroom_Block"" title=""Mushroom Block""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-208px -1136px""></span><span class=""sprite-text"">Blocks</span></span></a></li></ul></li></ul>
</td></tr>
<tr>
<th><a href=""/The_Nether"" title=""The Nether"">The Nether</a>
</th>
<td>
<ul><li><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-176px -1392px""></span> <a href=""/Fungus"" title=""Fungus""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-192px -1392px""></span><span class=""sprite-text"">Fungus</span></span></a></li>
<li><a href=""/Nether_Sprouts"" title=""Nether Sprouts""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-192px -1424px""></span><span class=""sprite-text"">Nether Sprouts</span></span></a></li>
<li><a href=""/Nether_Wart"" title=""Nether Wart""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-256px -1152px""></span><span class=""sprite-text"">Nether Wart</span></span></a></li>
<li><a href=""/Nether_Wart_Block"" title=""Nether Wart Block""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-304px -1392px""></span><span class=""sprite-text"">Nether Wart Block</span></span></a>
<ul><li><a href=""/Warped_Wart_Block"" class=""mw-redirect"" title=""Warped Wart Block""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-240px -1392px""></span><span class=""sprite-text"">Warped</span></span></a></li></ul></li>
<li><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-176px -1424px""></span> <a href=""/Roots"" title=""Roots""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-208px -1424px""></span><span class=""sprite-text"">Roots</span></span></a></li>
<li><a href=""/Shroomlight"" title=""Shroomlight""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-352px -1392px""></span><span class=""sprite-text"">Shroomlight</span></span></a></li>
<li><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-32px -1408px""></span> <a href=""/Stem"" title=""Stem""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-128px -1408px""></span><span class=""sprite-text"">Stem</span></span></a></li>
<li><a href=""/Twisting_Vines"" title=""Twisting Vines""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-160px -1440px""></span><span class=""sprite-text"">Twisting Vines</span></span></a></li>
<li><a href=""/Weeping_Vines"" title=""Weeping Vines""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-208px -1392px""></span><span class=""sprite-text"">Weeping Vines</span></span></a></li></ul>
</td></tr></tbody></table>
</td></tr></tbody></table>
</td></tr>
<tr>
<th>Manufactured
</th>
<td>
<ul><li><a href=""/Concrete"" title=""Concrete""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-368px -416px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-32px -432px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-16px -432px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-272px -416px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-304px -416px""></span><span class=""sprite-text"">Concrete</span></span></span></span></span></span></span></span></span></span></a>
<ul><li><a href=""/Concrete_Powder"" title=""Concrete Powder""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-240px -432px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-288px -432px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-272px -432px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-144px -432px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-176px -432px""></span><span class=""sprite-text"">Powder</span></span></span></span></span></span></span></span></span></span></a></li></ul></li>
<li><a href=""/Conduit"" title=""Conduit""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-16px -528px""></span><span class=""sprite-text"">Conduit</span></span></a></li>
<li><a href=""/Cracked_Nether_Bricks"" class=""mw-redirect"" title=""Cracked Nether Bricks""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-32px -1472px""></span><span class=""sprite-text"">Cracked Nether Bricks</span></span></a>
<ul><li><a href=""/Nether_Bricks"" title=""Nether Bricks""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-16px -1472px""></span><span class=""sprite-text"">Chiseled</span></span></a></li>
<li><a href=""/Nether_Bricks"" title=""Nether Bricks""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-224px -960px""></span><span class=""sprite-text"">Red</span></span></a></li></ul></li>
<li><a href=""/Cut_Red_Sandstone"" class=""mw-redirect"" title=""Cut Red Sandstone""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-64px -832px""></span><span class=""sprite-text"">Cut Red Sandstone</span></span></a>
<ul><li><a href=""/Sandstone"" title=""Sandstone""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-368px -816px""></span><span class=""sprite-text"">Chiseled</span></span></a></li>
<li><a href=""/Sandstone"" title=""Sandstone""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-64px -848px""></span><span class=""sprite-text"">Smooth</span></span></a></li></ul></li>
<li><a href=""/Honey_block"" class=""mw-redirect"" title=""Honey block""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-288px -1296px""></span><span class=""sprite-text"">Honey block</span></span></a></li>
<li><a href=""/Honeycomb_block"" class=""mw-redirect"" title=""Honeycomb block""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-320px -1296px""></span><span class=""sprite-text"">Honeycomb block</span></span></a></li>
<li><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-32px -1408px""></span> <a href=""/Hyphae"" class=""mw-redirect"" title=""Hyphae""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-128px -1408px""></span><span class=""sprite-text"">Hyphae</span></span></a>
<ul><li><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-64px -1408px""></span> <a href=""/Hyphae"" class=""mw-redirect"" title=""Hyphae""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-96px -1408px""></span><span class=""sprite-text"">Stripped</span></span></a></li></ul></li>
<li><a href=""/Planks"" title=""Planks""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-256px -1408px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-272px -1408px""></span><span class=""sprite-text"">Planks</span></span></span></span></a></li>
<li><a href=""/Slab"" title=""Slab""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-272px -960px""></span><span class=""sprite-text"">Slab</span></span></a></li>
<li><a href=""/Slime_Block"" title=""Slime Block""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-272px -944px""></span><span class=""sprite-text"">Slime Block</span></span></a></li>
<li><a href=""/Stairs"" title=""Stairs""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-240px -976px""></span><span class=""sprite-text"">Stairs</span></span></a></li>
<li><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-64px -1408px""></span> <a href=""/Stripped_Stem"" class=""mw-redirect"" title=""Stripped Stem""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-96px -1408px""></span><span class=""sprite-text"">Stripped Stem</span></span></a></li></ul>
<table>
<tbody><tr>
<th><a href=""/BE"" class=""mw-redirect"" title=""BE"">BE</a> &amp; <i><a href=""/EE"" class=""mw-redirect"" title=""EE"">EE</a></i> only
</th>
<td>
<ul><li><a href=""/Elements"" class=""mw-redirect"" title=""Elements""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-192px -576px""></span><span class=""sprite-text"">Elements</span></span></a></li>
<li><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-112px -240px""></span><a href=""/Hardened_Glass"" title=""Hardened Glass""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-224px -528px""></span><span class=""sprite-text"">Hardened Glass</span></span></a></li>
<li><a href=""/Heat_Block"" title=""Heat Block""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-176px -656px""></span><span class=""sprite-text"">Heat Block</span></span></a></li></ul>
</td></tr></tbody></table>
</td></tr>
<tr>
<th>Mineral blocks
</th>
<td>
<ul><li><a href=""/Block_of_Coal"" title=""Block of Coal""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-64px -928px""></span><span class=""sprite-text"">Block of Coal</span></span></a></li>
<li><a href=""/Block_of_Diamond"" title=""Block of Diamond""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-80px -928px""></span><span class=""sprite-text"">Block of Diamond</span></span></a></li>
<li><a href=""/Block_of_Emerald"" title=""Block of Emerald""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-96px -928px""></span><span class=""sprite-text"">Block of Emerald</span></span></a></li>
<li><a href=""/Block_of_Gold"" title=""Block of Gold""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-112px -928px""></span><span class=""sprite-text"">Block of Gold</span></span></a></li>
<li><a href=""/Block_of_Iron"" title=""Block of Iron""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-128px -928px""></span><span class=""sprite-text"">Block of Iron</span></span></a></li>
<li><a href=""/Block_of_Netherite"" title=""Block of Netherite""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-160px -1392px""></span><span class=""sprite-text"">Block of Netherite</span></span></a></li>
<li><a href=""/Block_of_Quartz"" title=""Block of Quartz""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-48px -1008px""></span><span class=""sprite-text"">Block of Quartz</span></span></a>
<ul><li><a href=""/Block_of_Quartz"" title=""Block of Quartz""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-80px -1008px""></span><span class=""sprite-text"">Chiseled</span></span></a></li>
<li><a href=""/Block_of_Quartz"" title=""Block of Quartz""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-112px -1008px""></span><span class=""sprite-text"">Pillar</span></span></a></li>
<li><a href=""/Block_of_Quartz"" title=""Block of Quartz""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-64px -1008px""></span><span class=""sprite-text"">Smooth</span></span></a></li>
<li><a href=""/Block_of_Quartz"" title=""Block of Quartz""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-192px -1456px""></span><span class=""sprite-text"">Bricks</span></span></a></li></ul></li>
<li><a href=""/Block_of_Redstone"" title=""Block of Redstone""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-160px -928px""></span><span class=""sprite-text"">Block of Redstone</span></span></a></li>
<li><a href=""/Lapis_Lazuli_Block"" title=""Lapis Lazuli Block""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-144px -928px""></span><span class=""sprite-text"">Lapis Lazuli Block</span></span></a></li></ul>
</td></tr>
<tr>
<th>Utility
</th>
<td>
<ul><li><a href=""/Anvil"" title=""Anvil""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-192px -1008px""></span><span class=""sprite-text"">Anvil</span></span></a></li>
<li><a href=""/Barrel"" title=""Barrel""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-48px -672px""></span><span class=""sprite-text"">Barrel</span></span></a></li>
<li><a href=""/Beacon"" title=""Beacon""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-80px -1232px""></span><span class=""sprite-text"">Beacon</span></span></a></li>
<li><a href=""/Beehive"" title=""Beehive""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-240px -1264px""></span><span class=""sprite-text"">Beehive</span></span></a></li>
<li><a href=""/Bed"" title=""Bed""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-64px -1104px""></span><span class=""sprite-text"">Bed</span></span></a></li>
<li><a href=""/Bell"" title=""Bell""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-32px -800px""></span><span class=""sprite-text"">Bell</span></span></a></li>
<li><a href=""/Brewing_Stand"" title=""Brewing Stand""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-256px -1008px""></span><span class=""sprite-text"">Brewing Stand</span></span></a></li>
<li><a href=""/Blast_Furnace"" title=""Blast Furnace""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-96px -672px""></span><span class=""sprite-text"">Blast Furnace</span></span></a></li>
<li><a href=""/Cake"" title=""Cake""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-192px -1216px""></span><span class=""sprite-text"">Cake</span></span></a></li>
<li><a href=""/Campfire"" title=""Campfire""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-0px -800px""></span><span class=""sprite-text"">Campfire</span></span></a></li>
<li><a href=""/Cartography_Table"" title=""Cartography Table""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-144px -672px""></span><span class=""sprite-text"">Cartography Table</span></span></a></li>
<li><a href=""/Cauldron"" title=""Cauldron""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-304px -1008px""></span><span class=""sprite-text"">Cauldron</span></span></a></li>
<li><a href=""/Chest"" title=""Chest""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-336px -80px""></span><span class=""sprite-text"">Chest</span></span></a>
<ul><li><a href=""/Ender_Chest"" title=""Ender Chest""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-160px -1024px""></span><span class=""sprite-text"">Ender</span></span></a></li></ul></li>
<li><a href=""/Crafting_Table"" title=""Crafting Table""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-0px -1024px""></span><span class=""sprite-text"">Crafting Table</span></span></a></li>
<li><a href=""/Enchanting_Table"" title=""Enchanting Table""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-96px -1024px""></span><span class=""sprite-text"">Enchanting Table</span></span></a></li>
<li><a href=""/Fletching_Table"" title=""Fletching Table""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-192px -672px""></span><span class=""sprite-text"">Fletching Table</span></span></a></li>
<li><a href=""/Farmland"" title=""Farmland""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-112px -1152px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-128px -1152px""></span><span class=""sprite-text"">Farmland</span></span></span></span></a></li>
<li><a href=""/Frosted_Ice"" title=""Frosted Ice""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-144px -832px""></span><span class=""sprite-text"">Frosted Ice</span></span></a></li>
<li><a href=""/Furnace"" title=""Furnace""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-16px -944px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-80px -944px""></span><span class=""sprite-text"">Furnace</span></span></span></span></a></li>
<li><a href=""/Grindstone"" title=""Grindstone""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-112px -688px""></span><span class=""sprite-text"">Grindstone</span></span></a></li>
<li><a href=""/Lodestone"" title=""Lodestone""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-160px -1456px""></span><span class=""sprite-text"">Lodestone</span></span></a></li>
<li><a href=""/Mob_head"" class=""mw-redirect"" title=""Mob head""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-224px -1216px""></span><span class=""sprite-text"">Mob Heads</span></span></a>
<ul><li><a href=""/Mob_head"" class=""mw-redirect"" title=""Mob head""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-336px -144px""></span><span class=""sprite-text"">Creeper</span></span></a></li>
<li><a href=""/Mob_head"" class=""mw-redirect"" title=""Mob head""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-224px -240px""></span><span class=""sprite-text"">Dragon</span></span></a></li>
<li><a href=""/Mob_head"" class=""mw-redirect"" title=""Mob head""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-256px -1216px""></span><span class=""sprite-text"">Skeleton</span></span></a></li>
<li><a href=""/Mob_head"" class=""mw-redirect"" title=""Mob head""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-272px -1216px""></span><span class=""sprite-text"">Wither Skeleton</span></span></a></li>
<li><a href=""/Mob_head"" class=""mw-redirect"" title=""Mob head""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-288px -1216px""></span><span class=""sprite-text"">Zombie</span></span></a></li></ul></li>
<li><a href=""/Jukebox"" title=""Jukebox""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-208px -1024px""></span><span class=""sprite-text"">Jukebox</span></span></a></li>
<li><a href=""/Lantern"" title=""Lantern""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-32px -768px""></span><span class=""sprite-text"">Lantern</span></span></a>
<ul><li><a href=""/Soul_Lantern"" class=""mw-redirect"" title=""Soul Lantern""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-112px -1424px""></span><span class=""sprite-text"">Soul</span></span></a></li></ul></li>
<li><a href=""/Lectern"" title=""Lectern""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-176px -688px""></span><span class=""sprite-text"">Lectern</span></span></a></li>
<li><a href=""/Loom"" title=""Loom""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-32px -688px""></span><span class=""sprite-text"">Loom</span></span></a></li>
<li><a href=""/Respawn_Anchor"" title=""Respawn Anchor""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-288px -1440px""></span><span class=""sprite-text"">Respawn Anchor</span></span></a></li>
<li><a href=""/Scaffolding"" title=""Scaffolding""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-240px -688px""></span><span class=""sprite-text"">Scaffolding</span></span></a></li>
<li><a href=""/Smithing_Table"" title=""Smithing Table""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-240px -672px""></span><span class=""sprite-text"">Smithing Table</span></span></a></li>
<li><a href=""/Smoker"" title=""Smoker""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-288px -672px""></span><span class=""sprite-text"">Smoker</span></span></a></li>
<li><a href=""/Stonecutter"" title=""Stonecutter""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-368px -672px""></span><span class=""sprite-text"">Stonecutter</span></span></a></li>
<li><a href=""/Shulker_Box"" title=""Shulker Box""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-224px -384px""></span><span class=""sprite-text"">Shulker Box</span></span></a></li>
<li><a href=""/Sign"" title=""Sign""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-320px -32px""></span><span class=""sprite-text"">Sign</span></span></a></li>
<li><a href=""/TNT"" title=""TNT""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-48px -1040px""></span><span class=""sprite-text"">TNT</span></span></a></li>
<li><a href=""/Torch"" title=""Torch""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-96px -1040px""></span><span class=""sprite-text"">Torch</span></span></a>
<ul><li><a href=""/Soul_Torch"" class=""mw-redirect"" title=""Soul Torch""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-256px -1392px""></span><span class=""sprite-text"">Soul</span></span></a></li></ul></li></ul>
<table>
<tbody><tr>
<th><a href=""/Bedrock_Edition"" title=""Bedrock Edition"">Bedrock Edition</a> only
</th>
<td>
<ul><li><a href=""/Item_Frame"" title=""Item Frame""><span class=""nowrap""><span class=""sprite item-sprite"" style=""background-image:url(/media/f/f5/ItemCSS.png?version=1597474691265);background-position:-0px -528px""></span><span class=""sprite-text"">Item Frame</span></span></a>
<ul><li>as a block</li></ul></li></ul>
</td></tr>
<tr>
<th><a href=""/Education_Edition"" title=""Education Edition""><i>Education Edition</i></a> only
</th>
<td>
<ul><li><a href=""/Chalkboard"" title=""Chalkboard""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-192px -416px""></span><span class=""sprite-text"">Chalkboard</span></span></a></li></ul>
</td></tr>
<tr>
<th><a href=""/BE"" class=""mw-redirect"" title=""BE"">BE</a> &amp; <i><a href=""/EE"" class=""mw-redirect"" title=""EE"">EE</a></i> only
</th>
<td>
<ul><li><a href=""/Compound_Creator"" title=""Compound Creator""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-272px -528px""></span><span class=""sprite-text"">Compound Creator</span></span></a></li>
<li><a href=""/Element_Constructor"" title=""Element Constructor""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-272px -560px""></span><span class=""sprite-text"">Element Constructor</span></span></a></li>
<li><a href=""/Lab_Table"" title=""Lab Table""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-0px -576px""></span><span class=""sprite-text"">Lab Table</span></span></a></li>
<li><a href=""/Material_Reducer"" title=""Material Reducer""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-80px -576px""></span><span class=""sprite-text"">Material Reducer</span></span></a></li>
<li><a href=""/Colored_Torch"" title=""Colored Torch""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-192px -656px""></span><span class=""sprite-text"">Colored Torches</span></span></a></li>
<li><a href=""/Underwater_Torch"" title=""Underwater Torch""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-176px -576px""></span><span class=""sprite-text"">Underwater Torch</span></span></a></li>
<li><a href=""/Underwater_TNT"" title=""Underwater TNT""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-80px -1392px""></span><span class=""sprite-text"">Underwater TNT</span></span></a></li></ul>
</td></tr></tbody></table>
</td></tr>
<tr>
<th><a href=""/Tutorials/Mechanisms"" title=""Tutorials/Mechanisms"">Mechanisms</a>
</th>
<td>
<ul><li><a href=""/Button"" title=""Button""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-304px -192px""></span><span class=""sprite-text"">Button</span></span></a></li>
<li><a href=""/Dispenser"" title=""Dispenser""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-256px -928px""></span><span class=""sprite-text"">Dispenser</span></span></a></li>
<li><a href=""/Daylight_Detector"" title=""Daylight Detector""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-144px -48px""></span><span class=""sprite-text"">Daylight Detector</span></span></a>
<ul><li><a href=""/Daylight_Sensor#As_a_night_time_detector"" class=""mw-redirect"" title=""Daylight Sensor""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-160px -32px""></span><span class=""sprite-text"">Inverted</span></span></a></li></ul></li>
<li><a href=""/Door"" title=""Door""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-192px -880px""></span><span class=""sprite-text"">Doors</span></span></a></li>
<li><a href=""/Dropper"" title=""Dropper""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-192px -944px""></span><span class=""sprite-text"">Dropper</span></span></a></li>
<li><a href=""/Fence_Gate"" title=""Fence Gate""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-288px -1168px""></span><span class=""sprite-text"">Fence Gates</span></span></a></li>
<li><a href=""/Hopper"" title=""Hopper""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-176px -1184px""></span><span class=""sprite-text"">Hopper</span></span></a></li>
<li><a href=""/Lever"" title=""Lever""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-288px -1184px""></span><span class=""sprite-text"">Lever</span></span></a></li>
<li><a href=""/Note_Block"" title=""Note Block""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-224px -1024px""></span><span class=""sprite-text"">Note Block</span></span></a></li>
<li><a href=""/Observer"" title=""Observer""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-304px -1024px""></span><span class=""sprite-text"">Observer</span></span></a></li>
<li><a href=""/Piston"" title=""Piston""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-32px -1200px""></span><span class=""sprite-text"">Piston</span></span></a>
<ul><li><a href=""/Piston"" title=""Piston""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-80px -1200px""></span><span class=""sprite-text"">Sticky</span></span></a></li>
<li><a href=""/Piston"" title=""Piston""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-32px -1232px""></span><span class=""sprite-text"">Head</span></span></a></li>
<li><a href=""/Piston"" title=""Piston""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-272px -128px""></span><span class=""sprite-text"">Moving</span></span></a></li></ul></li>
<li><a href=""/Pressure_Plate"" title=""Pressure Plate""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-304px -1168px""></span><span class=""sprite-text"">Pressure Plates</span></span></a></li>
<li><a href=""/Rail"" title=""Rail""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-224px -1200px""></span><span class=""sprite-text"">Rail</span></span></a>
<ul><li><a href=""/Activator_Rail"" title=""Activator Rail""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-240px -1168px""></span><span class=""sprite-text"">Activator</span></span></a></li>
<li><a href=""/Detector_Rail"" title=""Detector Rail""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-128px -1184px""></span><span class=""sprite-text"">Detector</span></span></a></li>
<li><a href=""/Powered_Rail"" title=""Powered Rail""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-176px -1200px""></span><span class=""sprite-text"">Powered</span></span></a></li></ul></li>
<li><a href=""/Redstone_Dust"" title=""Redstone Dust""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-64px -176px""></span><span class=""sprite-text"">Redstone Dust</span></span></a>
<ul><li><a href=""/Redstone_Comparator"" title=""Redstone Comparator""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-336px -1200px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-160px -1200px""></span><span class=""sprite-text"">Comparator</span></span></span></span></a></li>
<li><a href=""/Redstone_Lamp"" title=""Redstone Lamp""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-352px -1200px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-336px -1184px""></span><span class=""sprite-text"">Lamp</span></span></span></span></a></li>
<li><a href=""/Redstone_Repeater"" title=""Redstone Repeater""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-0px -1216px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-208px -1200px""></span><span class=""sprite-text"">Repeater</span></span></span></span></a></li>
<li><a href=""/Redstone_Torch"" title=""Redstone Torch""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-32px -1216px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-16px -1216px""></span><span class=""sprite-text"">Torch</span></span></span></span></a></li></ul></li>
<li><a href=""/Target"" title=""Target""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-32px -1440px""></span><span class=""sprite-text"">Target</span></span></a></li>
<li><a href=""/Trapdoor"" title=""Trapdoor""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-320px -1168px""></span><span class=""sprite-text"">Trapdoors</span></span></a></li>
<li><a href=""/Trapped_Chest"" title=""Trapped Chest""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-256px -176px""></span><span class=""sprite-text"">Trapped Chest</span></span></a></li>
<li><a href=""/Tripwire_Hook"" title=""Tripwire Hook""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-160px -1216px""></span><span class=""sprite-text"">Tripwire Hook</span></span></a></li>
<li><a href=""/String#Tripwire"" title=""String""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-288px -128px""></span><span class=""sprite-text"">Tripwire</span></span></a></li></ul>
</td></tr>
<tr>
<th><a href=""/Creative"" title=""Creative"">Creative</a> only
</th>
<td>
<ul><li><a href=""/Player_Head"" class=""mw-redirect"" title=""Player Head""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-320px -144px""></span><span class=""sprite-text"">Player Head</span></span></a></li></ul>
<table>
<tbody><tr>
<th><a href=""/Java_Edition"" title=""Java Edition""><i>Java Edition</i></a> only
</th>
<td>
<ul><li><a href=""/Petrified_Oak_Slab"" class=""mw-redirect"" title=""Petrified Oak Slab""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-48px -976px""></span><span class=""sprite-text"">Petrified Oak Slab</span></span></a></li></ul>
</td></tr>
<tr>
<th><a href=""/Bedrock_Edition"" title=""Bedrock Edition"">Bedrock Edition</a> only
</th>
<td>
<ul><li><a href=""/Monster_Spawner"" class=""mw-redirect"" title=""Monster Spawner""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-240px -1216px""></span><span class=""sprite-text"">Empty Monster Spawner</span></span></a></li></ul>
</td></tr>
<tr>
<th><a href=""/BE"" class=""mw-redirect"" title=""BE"">BE</a> &amp; <i><a href=""/EE"" class=""mw-redirect"" title=""EE"">EE</a></i> only
</th>
<td>
<ul><li><a href=""/Allow_and_Deny_Blocks"" class=""mw-redirect"" title=""Allow and Deny Blocks""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-128px -416px""></span><span class=""sprite-text"">Allow</span></span></a></li>
<li><a href=""/Border"" title=""Border""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-256px -288px""></span><span class=""sprite-text"">Border</span></span></a></li>
<li><a href=""/Allow_and_Deny_Blocks"" class=""mw-redirect"" title=""Allow and Deny Blocks""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-144px -416px""></span><span class=""sprite-text"">Deny</span></span></a></li></ul>
</td></tr></tbody></table>
</td></tr>
<tr>
<th><a href=""/Commands"" title=""Commands"">Commands</a> only
</th>
<td>
<ul><li><a href=""/Barrier"" title=""Barrier""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-304px -1216px""></span><span class=""sprite-text"">Barrier</span></span></a></li>
<li><a href=""/Command_Block"" title=""Command Block""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-64px -1184px""></span><span class=""sprite-text"">Command Block</span></span></a>
<ul><li><a href=""/Command_Block"" title=""Command Block""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-48px -1184px""></span><span class=""sprite-text"">Chain</span></span></a></li>
<li><a href=""/Command_Block"" title=""Command Block""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-48px -1216px""></span><span class=""sprite-text"">Repeating</span></span></a></li></ul></li>
<li><a href=""/Jigsaw_Block"" title=""Jigsaw Block""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-48px -768px""></span><span class=""sprite-text"">Jigsaw Block</span></span></a></li>
<li><a href=""/Structure_Block"" title=""Structure Block""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-192px -800px""></span><span class=""sprite-text"">Structure Block</span></span></a></li>
<li><a href=""/Structure_Void"" class=""mw-redirect"" title=""Structure Void""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-48px -1232px""></span><span class=""sprite-text"">Structure Void</span></span></a></li>
<li><a href=""/TNT"" title=""TNT""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-48px -1040px""></span><span class=""sprite-text"">Unstable TNT</span></span></a></li></ul>
<table>
<tbody><tr>
<th><a href=""/Java_Edition"" title=""Java Edition""><i>Java Edition</i></a> only
</th>
<td>
<ul><li><a href=""/Mob_head"" class=""mw-redirect"" title=""Mob head""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-320px -144px""></span><span class=""sprite-text"">Custom Player Head</span></span></a></li></ul>
</td></tr>
<tr>
<th><a href=""/Bedrock_Edition"" title=""Bedrock Edition"">Bedrock Edition</a> only
</th>
<td>
<ul><li><a href=""/Bedrock_Edition_unused_features#Purpur_Blocks"" title=""Bedrock Edition unused features""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-208px -944px""></span><span class=""sprite-text"">Chiseled Purpur</span></span></a></li>
<li><a href=""/Bedrock_Edition_unused_features#Fake_Wood_Slab"" title=""Bedrock Edition unused features""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-48px -976px""></span><span class=""sprite-text"">Fake Wood Slab</span></span></a></li>
<li><a href=""/Light_Block"" title=""Light Block""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-208px -1376px""></span><span class=""sprite-text"">Light Block</span></span></a></li>
<li><a href=""/Stone_Bricks#ID"" title=""Stone Bricks""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-176px -960px""></span><span class=""sprite-text"">Smooth Stone Bricks</span></span></a></li>
<li><a href=""/Bedrock_Edition_unused_features#Purpur_Blocks"" title=""Bedrock Edition unused features""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-208px -944px""></span><span class=""sprite-text"">tile.purpur_block.smooth.name</span></span></a></li></ul>
</td></tr>
<tr>
<th><a href=""/Education_Edition"" title=""Education Edition""><i>Education Edition</i></a> only
</th>
<td>
<ul><li><a href=""/Camera"" title=""Camera""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-272px -80px""></span><span class=""sprite-text"">Camera</span></span></a></li></ul>
</td></tr></tbody></table>
</td></tr>
<tr>
<th>Unused
</th>
<td>
<table>
<tbody><tr>
<th><a href=""/Bedrock_Edition"" title=""Bedrock Edition"">Bedrock Edition</a> only
</th>
<td>
<ul><li><a href=""/Bedrock_Edition_unused_features#Bell-less_Block"" title=""Bedrock Edition unused features""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-256px -1296px""></span><span class=""sprite-text"">Bell Stand</span></span></a></li>
<li><a href=""/Glowing_Obsidian"" title=""Glowing Obsidian""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-368px -160px""></span><span class=""sprite-text"">Glowing Obsidian</span></span></a></li>
<li><a href=""/Invisible_Bedrock"" title=""Invisible Bedrock""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-240px -16px""></span><span class=""sprite-text"">Invisible Bedrock</span></span></a></li>
<li><a href=""/Nether_Reactor_Core"" title=""Nether Reactor Core""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-352px -160px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-96px -896px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-80px -896px""></span><span class=""sprite-text"">Nether Reactor Core</span></span></span></span></span></span></a></li>
<li><a href=""/Bedrock_Edition_unused_features#Smokeless_Campfire"" title=""Bedrock Edition unused features""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-0px -800px""></span><span class=""sprite-text"">Smokeless Campfire</span></span></a></li>
<li><a href=""/Info_update"" title=""Info update""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-320px -176px""></span><span class=""sprite-text"">info_update</span></span></a></li>
<li><a href=""/Info_update"" title=""Info update""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-320px -176px""></span><span class=""sprite-text"">info_update2</span></span></a></li>
<li><a href=""/Reserved6"" title=""Reserved6""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-320px -176px""></span><span class=""sprite-text"">reserved6</span></span></a></li>
<li><a href=""/Stonecutter/old"" title=""Stonecutter/old""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-336px -192px""></span><span class=""sprite-text"">Stonecutter (Old)</span></span></a></li></ul>
</td></tr></tbody></table>
</td></tr>
<tr>
<th>Removed
</th>
<td>
<ul><li><a href=""/Hay_Bale#History"" title=""Hay Bale""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-144px -1152px""></span><span class=""sprite-text"">Hexahedral Hay Bale</span></span></a></li></ul>
<table>
<tbody><tr>
<th><a href=""/Java_Edition"" title=""Java Edition""><i>Java Edition</i></a> only
</th>
<td>
<ul><li><a href=""/Wool#History"" title=""Wool""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-272px -1280px""></span><span class=""sprite-text"">Cloth</span></span></a></li>
<li><a href=""/Java_Edition_removed_features#Cog"" title=""Java Edition removed features""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-352px -32px""></span><span class=""sprite-text"">Cog</span></span></a></li>
<li><a href=""/Coral_Block#History"" title=""Coral Block""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-208px -1232px""></span><span class=""sprite-text"">Dead Coral Block</span></span></a></li>
<li><a href=""/Java_Edition_mentioned_features#Dirt_slab"" title=""Java Edition mentioned features""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-256px -112px""></span><span class=""sprite-text"">Dirt Slab</span></span></a></li>
<li><a href=""/Jack_o%27Lantern#History"" title=""Jack o&#39;Lantern""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-368px -1152px""></span><span class=""sprite-text"">Faceless Jack o'Lantern</span></span></a></li>
<li><a href=""/Infinite_Lava_Source"" title=""Infinite Lava Source""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-176px -288px""></span><span class=""sprite-text"">Infinite Lava Source</span></span></a></li>
<li><a href=""/Infinite_Water_Source"" title=""Infinite Water Source""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-160px -288px""></span><span class=""sprite-text"">Infinite Water Source</span></span></a></li>
<li><a href=""/Locked_chest"" title=""Locked chest""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-304px -96px""></span><span class=""sprite-text"">Locked chest</span></span></a></li>
<li><a href=""/Melon_Seeds#History"" title=""Melon Seeds""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-240px -96px""></span><span class=""sprite-text"">Overripe Melon Stem</span></span></a></li>
<li><a href=""/Pumpkin_Seeds#History"" title=""Pumpkin Seeds""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-240px -96px""></span><span class=""sprite-text"">Overripe Pumpkin Stem</span></span></a></li>
<li><a href=""/Flower_Pot#History"" title=""Flower Pot""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-368px -240px""></span><span class=""sprite-text"">Potted non-flower block</span></span></a></li>
<li><a href=""/Flower"" title=""Flower""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-192px -0px""></span><span class=""sprite-text"">Rose</span></span></a>
<ul><li><a href=""/Flower_Pot#History"" title=""Flower Pot""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-224px -704px""></span><span class=""sprite-text"">Potted</span></span></a></li></ul></li>
<li><a href=""/Emerald_Ore#History"" title=""Emerald Ore""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-288px -112px""></span><span class=""sprite-text"">Ruby Ore</span></span></a></li>
<li><a href=""/Sponge#History"" title=""Sponge""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-0px -48px""></span><span class=""sprite-text"">Sponge (Classic)</span></span></a></li>
<li><a href=""/Grass"" title=""Grass""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-112px -48px""></span><span class=""sprite-text"">Shrub</span></span></a>
<ul><li><a href=""/Grass#History"" title=""Grass""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-112px -32px""></span><span class=""sprite-text"">Green</span></span></a></li></ul></li>
<li><a href=""/Piston"" title=""Piston""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-64px -1200px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-80px -1200px""></span><span class=""sprite-text"">Six-sided Piston</span></span></span></span></a></li></ul>
</td></tr>
<tr>
<th><a href=""/Bedrock_Edition"" title=""Bedrock Edition"">Bedrock Edition</a> only
</th>
<td>
<ul><li><a href=""/Flower"" title=""Flower""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-352px -64px""></span><span class=""sprite-text"">Cyan Flower</span></span></a></li>
<li><a href=""/Bell"" title=""Bell""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-240px -880px""></span><span class=""sprite-text"">Diorite Bell</span></span></a></li>
<li><a href=""/Bell"" title=""Bell""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-256px -880px""></span><span class=""sprite-text"">Granite Bell</span></span></a>
<ul><li><a href=""/Bell"" title=""Bell""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-272px -880px""></span><span class=""sprite-text"">Polished</span></span></a></li></ul></li>
<li><a href=""/Bone_Block#History"" title=""Bone Block""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-336px -816px""></span><span class=""sprite-text"">Hexahedral Bone Block</span></span></a></li>
<li><a href=""/Purpur_Block#History"" title=""Purpur Block""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-224px -944px""></span><span class=""sprite-text"">Hexahedral Purpur Pillar</span></span></a></li>
<li><a href=""/Block_of_Quartz#History"" title=""Block of Quartz""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-112px -1008px""></span><span class=""sprite-text"">Hexahedral Quartz Pillar</span></span></a></li></ul>
</td></tr></tbody></table>
</td></tr>
<tr>
<th><a href=""/Unimplemented_features"" title=""Unimplemented features"">Unimplemented</a>
</th>
<td>
<ul><li><a href=""/Java_Edition_mentioned_features#Branches"" title=""Java Edition mentioned features"">Branches</a></li>
<li><a href=""/Java_Edition_mentioned_features#Colored_wood_planks"" title=""Java Edition mentioned features""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-336px -800px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-16px -816px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-64px -816px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-80px -816px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-112px -816px""></span><span class=""sprite-text"">Colored Wood Planks</span></span></span></span></span></span></span></span></span></span></a></li>
<li><a href=""/Coral_Block#History"" title=""Coral Block""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-192px -1296px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-224px -1296px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-240px -1296px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-208px -1296px""></span><span class=""sprite-text"">Coral Slab</span></span></span></span></span></span></span></span></a></li>
<li><a href=""/Barrel#Gallery"" title=""Barrel""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-192px -1264px""></span><span class=""sprite-text"">Empty Barrel</span></span></a></li>
<li><a href=""/Barrel#Gallery"" title=""Barrel""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-224px -1232px""></span><span class=""sprite-text"">Fish Barrel</span></span></a></li>
<li><a href=""/Java_Edition_removed_features#Chairs_and_other_furniture"" title=""Java Edition removed features""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-240px -848px""></span><span class=""sprite-text""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-288px -848px""></span><span class=""sprite-text"">Furniture</span></span></span></span></a></li>
<li><a href=""/Java_Edition_removed_features#Paeonia"" title=""Java Edition removed features""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-192px -816px""></span><span class=""sprite-text"">Paeonia</span></span></a></li>
<li><a href=""/Java_Edition_mentioned_features#Spike_block"" title=""Java Edition mentioned features"">Spike Block</a></li>
<li><a href=""/Wax_Block"" class=""mw-redirect"" title=""Wax Block""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-16px -1280px""></span><span class=""sprite-text"">Wax Block</span></span></a></li></ul>
</td></tr>
<tr>
<th>Joke features
</th>
<td>
<ul><li><a href=""/20w14infinite#Blocks"" class=""mw-redirect"" title=""20w14infinite""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-144px -1488px""></span><span class=""sprite-text"">An Ant</span></span></a></li>
<li><a href=""/Java_Edition_2.0#Blocks"" title=""Java Edition 2.0""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-288px -880px""></span><span class=""sprite-text"">Block of Coal</span></span></a></li>
<li><a href=""/20w14infinite#Blocks"" class=""mw-redirect"" title=""20w14infinite""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-368px -240px""></span><span class=""sprite-text"">Box of Infinite Books</span></span></a></li>
<li><a href=""/Java_Edition_2.0#Blocks"" title=""Java Edition 2.0""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-80px -528px""></span><span class=""sprite-text"">Burnt-out Torch</span></span></a></li>
<li><a href=""/20w14infinite#Blocks"" class=""mw-redirect"" title=""20w14infinite""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-176px -1488px""></span><span class=""sprite-text"">Cursor</span></span></a></li>
<li><a href=""/Java_Edition_2.0#Blocks"" title=""Java Edition 2.0""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-96px -528px""></span><span class=""sprite-text"">Etho Slab</span></span></a></li>
<li><a href=""/20w14infinite#Blocks"" class=""mw-redirect"" title=""20w14infinite""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-368px -240px""></span><span class=""sprite-text"">Funky Portal</span></span></a></li>
<li><a href=""/20w14infinite#Blocks"" class=""mw-redirect"" title=""20w14infinite""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-240px -16px""></span><span class=""sprite-text"">Leftover</span></span></a></li>
<li><a href=""/20w14infinite#Blocks"" class=""mw-redirect"" title=""20w14infinite""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-192px -1488px""></span><span class=""sprite-text"">Swaggiest stairs ever</span></span></a></li>
<li><a href=""/Java_Edition_2.0#Blocks"" title=""Java Edition 2.0""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-208px -1264px""></span><span class=""sprite-text"">Tinted Glass</span></span></a></li>
<li><a href=""/Java_Edition_2.0#Blocks"" title=""Java Edition 2.0""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-80px -1296px""></span><span class=""sprite-text"">Tinted Glass Pane</span></span></a></li>
<li><a href=""/USB_Charger_Block"" title=""USB Charger Block""><span class=""nowrap""><span class=""sprite block-sprite"" style=""background-image:url(/media/d/df/BlockCSS.png?version=20200824052639);background-position:-288px -288px""></span><span class=""sprite-text"">USB Charger Block</span></span></a></li></ul>
</td></tr></tbody></table>

<!-- 
NewPP limit report
Cached time: 20200916223106
Cache expiry: 86400
Dynamic content: false
CPU time usage: 0.659 seconds
Real time usage: 1.505 seconds
Preprocessor visited node count: 6739/1000000
Preprocessor generated node count: 26026/1000000
Post‐expand include size: 321948/2097152 bytes
Template argument size: 375/2097152 bytes
Highest expansion depth: 9/40
Expensive parser function count: 2/99
Unstrip recursion depth: 0/20
Unstrip post‐expand size: 0/5000000 bytes
ExtLoops count: 0
Lua time usage: 0.830/7 seconds
Lua virtual size: 9.49 MB/100 MB
Lua estimated memory usage: 0 bytes
-->
<!--
Transclusion expansion time report (%,ms,calls,template)
130.57% 1709.249    462 Template:BlockLink
100.00% 1309.021      1 -total
  3.92%   51.376     20 Template:BlockSprite
  2.22%   29.020      1 Template:ItemLink
  2.17%   28.387     11 Template:El
  0.28%    3.611      1 Template:SimpleNavbox
-->

<!-- Saved in parser cache with key minecraft_gamepedia:pcache:idhash:2475-0!canonical and timestamp 20200916223105 and revision id 1683616
 -->
</div></div>						<div class=""printfooter"">
							Retrieved from ""<a dir=""ltr"" href=""https://minecraft.gamepedia.com/index.php?title=Template:Blocks/content&amp;oldid=1683616"">https://minecraft.gamepedia.com/index.php?title=Template:Blocks/content&amp;oldid=1683616</a>""						</div>
						<div id=""catlinks"" class=""catlinks"" data-mw=""interface""><div id=""mw-normal-catlinks"" class=""mw-normal-catlinks""><a href=""/Special:Categories"" title=""Special:Categories"">Category</a>: <ul><li><a href=""/Category:Ajax_loaded_pages"" title=""Category:Ajax loaded pages"">Ajax loaded pages</a></li></ul></div><div id=""mw-hidden-catlinks"" class=""mw-hidden-catlinks mw-hidden-cats-hidden"">Hidden categories: <ul><li><a href=""/Category:Pages_using_DynamicPageList_dplreplace_parser_function"" title=""Category:Pages using DynamicPageList dplreplace parser function"">Pages using DynamicPageList dplreplace parser function</a></li><li><a href=""/Category:Pages_with_missing_sprites"" title=""Category:Pages with missing sprites"">Pages with missing sprites</a></li></ul></div></div>					<div class=""visualClear""></div>
									</div>
								<div id=""siderail_minecraft_gamepedia"">
					<div id='atfmrec_minecraft_gamepedia'><div id='cdm-zone-02'></div></div><div id='middlemrec_minecraft_gamepedia'><a href='https://www.gamepedia.com/PRO?utm_source=GPPromo&utm_medium=Rail'><img src='https://gamepedia.cursecdn.com/commons_hydra/c/c2/Pro_Promorail_v2.jpg'></a></div><div id='btfmrec_minecraft_gamepedia'><div id='cdm-zone-06'></div></div>				</div>
				<div class=""visualClear""></div>
					<div id=btflb><div id='cdm-zone-04'></div></div>			</div>
			<div id=""mw-navigation"">
				<h2>Navigation menu</h2>
				<div id=""mw-head"">
										<div id=""left-navigation"">
											<div id=""p-namespaces"" role=""navigation"" class=""vectorTabs"" aria-labelledby=""p-namespaces-label"">
						<h3 id=""p-namespaces-label"">Namespaces</h3>
						<ul>
							<li id=""ca-nstab-template"" class=""selected""><span><a href=""/Template:Blocks/content"" title=""View the template [c]"" accesskey=""c"">Template</a></span></li><li id=""ca-talk""><span><a href=""/Template_talk:Blocks/content"" rel=""discussion"" class=""mw-redirect"" title=""Talk about the content page [t]"" accesskey=""t"">Talk</a></span></li>						</ul>
					</div>
										<div id=""p-variants"" role=""navigation"" class=""vectorMenu emptyPortlet"" aria-labelledby=""p-variants-label"">
												<input type=""checkbox"" class=""vectorMenuCheckbox"" aria-labelledby=""p-variants-label"" />
						<h3 id=""p-variants-label"">
							<span>Variants</span>
						</h3>
						<div class=""menu"">
							<ul>
															</ul>
						</div>
					</div>
										<div id=""p-sharing"" role=""navigation"" class=""vectorMenu"" aria-labelledby=""p-sharing-label"">
						<input type=""checkbox"" class=""vectorMenuCheckbox"" aria-labelledby=""p-sharing-label"" />
						<h3 id=""p-sharing-label""><span>Share</span></h3>
						<div class=""menu"">
							
				<div id=""socialIconImages"">
					<a href=""https://twitter.com/intent/tweet?text=Check+out+Template%3ABlocks%2Fcontent+on+Minecraft+Wiki+--&amp;via=CurseGamepedia&amp;url=https%3A%2F%2Fminecraft.gamepedia.com%2FTemplate%3ABlocks%2Fcontent"" target=""_blank""><img src=""/extensions/Social/images/social/twitter.svg""/></a><a href=""https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fminecraft.gamepedia.com%2FTemplate%3ABlocks%2Fcontent"" target=""_blank""><img src=""/extensions/Social/images/social/facebook.svg""/></a><a href=""https://www.reddit.com/submit?url=https%3A%2F%2Fminecraft.gamepedia.com%2FTemplate%3ABlocks%2Fcontent&amp;title=Check+out+Template%3ABlocks%2Fcontent+on+Minecraft+Wiki+via+%40CurseGamepedia%3A+https%3A%2F%2Fminecraft.gamepedia.com%2FTemplate%3ABlocks%2Fcontent"" target=""_blank""><img src=""/extensions/Social/images/social/reddit.svg""/></a><a href=""https://www.tumblr.com/share/link?url=https%3A%2F%2Fminecraft.gamepedia.com%2FTemplate%3ABlocks%2Fcontent&amp;name=Minecraft+Wiki&amp;description=Check+out+Template%3ABlocks%2Fcontent+on+Minecraft+Wiki+via+%3Ca+href%3D%22https%3A%2F%2Ftwitter.com%2FCurseGamepedia%22+target%3D%22_blank%22%3E%40CurseGamepedia%3C%2Fa%3E%3A+https%3A%2F%2Fminecraft.gamepedia.com%2FTemplate%3ABlocks%2Fcontent"" target=""_blank""><img src=""/extensions/Social/images/social/tumblr.svg""/></a><a href=""https://vk.com/share.php?url=https%3A%2F%2Fminecraft.gamepedia.com%2FTemplate%3ABlocks%2Fcontent"" target=""_blank""><img src=""/extensions/Social/images/social/vk.svg""/></a>
				</div>
						</div>
					</div>
										</div>
					<div id=""right-navigation"">
											<div id=""p-views"" role=""navigation"" class=""vectorTabs"" aria-labelledby=""p-views-label"">
						<h3 id=""p-views-label"">Views</h3>
						<ul>
							<li id=""ca-view"" class=""collapsible selected""><span><a href=""/Template:Blocks/content"">View</a></span></li><li id=""ca-viewsource"" class=""collapsible""><span><a href=""/index.php?title=Template:Blocks/content&amp;action=edit"" title=""This page is protected.&#10;You can view its source [e]"" accesskey=""e"">View source</a></span></li><li id=""ca-history"" class=""collapsible""><span><a href=""/index.php?title=Template:Blocks/content&amp;action=history"" title=""Past revisions of this page [h]"" accesskey=""h"">History</a></span></li>						</ul>
					</div>
										<div id=""p-cactions"" role=""navigation"" class=""vectorMenu emptyPortlet"" aria-labelledby=""p-cactions-label"">
						<input type=""checkbox"" class=""vectorMenuCheckbox"" aria-labelledby=""p-cactions-label"" />
						<h3 id=""p-cactions-label""><span>More</span></h3>
						<div class=""menu"">
							<ul>
															</ul>
						</div>
					</div>
										<div id=""p-search"" role=""search"">
						<h3>
							<label for=""searchInput"">Search</label>
						</h3>
						<form action=""/index.php"" id=""searchform"">
							<div id=""simpleSearch"">
								<input type=""search"" name=""search"" placeholder=""Search Minecraft Wiki"" title=""Search Minecraft Wiki [f]"" accesskey=""f"" id=""searchInput""/><input type=""hidden"" value=""Special:Search"" name=""title""/><input type=""submit"" name=""fulltext"" value=""Search"" title=""Search the pages for this text"" id=""mw-searchButton"" class=""searchButton mw-fallbackSearchButton""/><input type=""submit"" name=""go"" value=""Go"" title=""Go to a page with this exact name if it exists"" id=""searchButton"" class=""searchButton""/>							</div>
						</form>
					</div>
										</div>
				</div>
				<div id=""mw-panel"">
					<div id=""p-logo"" role=""banner""><a class=""mw-wiki-logo"" href=""/Minecraft_Wiki"" title=""Visit the main page - Minecraft Wiki""></a></div>
							<div class=""portal"" role=""navigation"" id=""p-Minecraft_Wiki"" aria-labelledby=""p-Minecraft_Wiki-label"">
			<h3 id=""p-Minecraft_Wiki-label"">Minecraft Wiki</h3>
			<div class=""body"">
								<ul>
					<li id=""n-mainpage-description""><a href=""/Minecraft_Wiki"" title=""Visit the main page [z]"" accesskey=""z"">Main page</a></li><li id=""n-portal""><a href=""/Minecraft_Wiki:Community_portal"" title=""About the Minecraft Wiki, what you can do, where to find things"">Community portal</a></li><li id=""n-Projects""><a href=""/Minecraft_Wiki:Projects"" title=""View and create projects on the wiki"">Projects</a></li><li id=""n-Wiki-rules""><a href=""/Minecraft_Wiki:Wiki_rules"" title=""Rules to follow when editing the wiki"">Wiki rules</a></li><li id=""n-Style-guide""><a href=""/Minecraft_Wiki:Style_guide"" title=""Guidelines to format articles and keep content consistent"">Style guide</a></li><li id=""n-Sandbox""><a href=""/Minecraft_Wiki:Sandbox"" title=""Experiment with making edits here"">Sandbox</a></li><li id=""n-recentchanges""><a href=""/Special:RecentChanges"" title=""A list of recent changes in the wiki [r]"" accesskey=""r"">Recent changes</a></li><li id=""n-randompage""><a href=""/Special:RandomRootpage"" title=""Load a random page [x]"" accesskey=""x"">Random page</a></li><li id=""n-Admin-noticeboard""><a href=""/Minecraft_Wiki:Admin_noticeboard"" title=""Report issues requiring admin attention here"">Admin noticeboard</a></li><li id=""n-Directors-page""><a href=""/Minecraft_Wiki:Directors"" title=""List of admins for all of the language wikis"">Directors page</a></li><li id=""n-Discord-server""><a href=""/Minecraft_Wiki:Discord"">Discord server</a></li>				</ul>
							</div>
		</div>
			<div class=""portal"" role=""navigation"" id=""p-Games"" aria-labelledby=""p-Games-label"">
			<h3 id=""p-Games-label"">Games</h3>
			<div class=""body"">
								<ul>
					<li id=""n-Minecraft""><a href=""/Minecraft"">Minecraft</a></li><li id=""n-Minecraft-Earth""><a href=""/Minecraft_Earth"">Minecraft Earth</a></li><li id=""n-Minecraft-Dungeons""><a href=""/Minecraft_Dungeons"">Minecraft Dungeons</a></li>				</ul>
							</div>
		</div>
			<div class=""portal"" role=""navigation"" id=""p-Useful_pages"" aria-labelledby=""p-Useful_pages-label"">
			<h3 id=""p-Useful_pages-label"">Useful pages</h3>
			<div class=""body"">
								<ul>
					<li id=""n-Trading""><a href=""/Trading"" title=""Information about trading with villagers"">Trading</a></li><li id=""n-Brewing""><a href=""/Brewing"" title=""Information about brewing"">Brewing</a></li><li id=""n-Enchanting""><a href=""/Enchanting"" title=""Information about enchanting"">Enchanting</a></li><li id=""n-Mobs""><a href=""/Mob"" title=""Information about the various friendly and non-friendly creatures found in Minecraft"">Mobs</a></li><li id=""n-Blocks""><a href=""/Block"" title=""Detailed information on the various blocks available in Minecraft"">Blocks</a></li><li id=""n-Items""><a href=""/Item"" title=""Detailed information on the various items available in Minecraft"">Items</a></li><li id=""n-Crafting""><a href=""/Crafting"" title=""Information about crafting"">Crafting</a></li><li id=""n-Smelting""><a href=""/Smelting"" title=""Information about smelting"">Smelting</a></li><li id=""n-Tutorials""><a href=""/Tutorials"">Tutorials</a></li><li id=""n-Resource-pack""><a href=""/Resource_pack"" title=""Resource packs alter the look and feel of the game"">Resource pack</a></li><li id=""n-Redstone-circuit""><a href=""/Mechanics/Redstone/Circuit"" title=""Information about redstone circuits"">Redstone circuit</a></li>				</ul>
							</div>
		</div>
			<div class=""portal"" role=""navigation"" id=""p-Minecraft_links"" aria-labelledby=""p-Minecraft_links-label"">
			<h3 id=""p-Minecraft_links-label"">Minecraft links</h3>
			<div class=""body"">
								<ul>
					<li id=""n-Website""><a href=""https://minecraft.net/"" rel=""nofollow"" target=""_self"" title=""The Minecraft website"">Website</a></li><li id=""n-Minecraft-Discord""><a href=""https://discord.gg/minecraft"" rel=""nofollow"" target=""_self"" title=""Minecraft&#039;s Discord server"">Minecraft Discord</a></li><li id=""n-Support""><a href=""https://help.minecraft.net/"" rel=""nofollow"" target=""_self"" title=""Mojang support center"">Support</a></li><li id=""n-Bug-tracker""><a href=""https://bugs.mojang.com/"" rel=""nofollow"" target=""_self"" title=""Mojang&#039;s issue tracker"">Bug tracker</a></li><li id=""n-Feedback""><a href=""https://feedback.minecraft.net"" rel=""nofollow"" target=""_self"" title=""Minecraft feedback website"">Feedback</a></li><li id=""n-Twitter""><a href=""https://twitter.com/Minecraft"" rel=""nofollow"" target=""_self"" title=""Minecraft&#039;s Twitter page"">Twitter</a></li><li id=""n-Facebook""><a href=""https://www.facebook.com/minecraft"" rel=""nofollow"" target=""_self"" title=""Minecraft&#039;s Facebook page"">Facebook</a></li><li id=""n-YouTube""><a href=""https://www.youtube.com/minecraft"" rel=""nofollow"" target=""_self"" title=""Minecraft&#039;s YouTube channel"">YouTube</a></li><li id=""n-Minecraft-Twitch""><a href=""https://www.twitch.tv/minecraft"" rel=""nofollow"" target=""_self"" title=""Minecraft&#039;s Twitch channel"">Minecraft Twitch</a></li>				</ul>
							</div>
		</div>
			<div class=""portal"" role=""navigation"" id=""p-Gamepedia"" aria-labelledby=""p-Gamepedia-label"">
			<h3 id=""p-Gamepedia-label"">Gamepedia</h3>
			<div class=""body"">
								<ul>
					<li id=""n-gamepedia-support""><a href=""https://gamepedia.zendesk.com"" rel=""nofollow"" target=""_self"">Gamepedia support</a></li><li id=""n-bad-ad""><a href=""https://community.fandom.com/wiki/Help%3ABad_advertisements"" target=""_self"">Report a bad ad</a></li><li id=""n-gamepedia-help""><a href=""https://help.gamepedia.com/"">Help Wiki</a></li><li id=""n-gamepedia-contact""><a href=""https://help.gamepedia.com/How_to_contact_Gamepedia"">Contact us</a></li>				</ul>
							</div>
		</div>
			<div class=""portal"" role=""navigation"" id=""p-tb"" aria-labelledby=""p-tb-label"">
			<h3 id=""p-tb-label"">Tools</h3>
			<div class=""body"">
								<ul>
					<li id=""t-whatlinkshere""><a href=""/Special:WhatLinksHere/Template:Blocks/content"" title=""A list of all wiki pages that link here [j]"" accesskey=""j"">What links here</a></li><li id=""t-recentchangeslinked""><a href=""/Special:RecentChangesLinked/Template:Blocks/content"" rel=""nofollow"" title=""Recent changes in pages linked from this page [k]"" accesskey=""k"">Related changes</a></li><li id=""t-specialpages""><a href=""/Special:SpecialPages"" title=""A list of all special pages [q]"" accesskey=""q"">Special pages</a></li><li id=""t-print""><a href=""/index.php?title=Template:Blocks/content&amp;printable=yes"" rel=""alternate"" title=""Printable version of this page [p]"" accesskey=""p"">Printable version</a></li><li id=""t-permalink""><a href=""/index.php?title=Template:Blocks/content&amp;oldid=1683616"" title=""Permanent link to this revision of the page"">Permanent link</a></li><li id=""t-info""><a href=""/index.php?title=Template:Blocks/content&amp;action=info"" title=""More information about this page"">Page information</a></li>				</ul>
							</div>
		</div>
			<div class=""portal"" role=""navigation"" id=""p-lang"" aria-labelledby=""p-lang-label"">
			<h3 id=""p-lang-label"">In other languages</h3>
			<div class=""body"">
								<ul>
					<li class=""interlanguage-link interwiki-es""><a href=""//minecraft-es.gamepedia.com/Plantilla:Bloques/contenido"" title=""Plantilla:Bloques/contenido – español"" lang=""es"" hreflang=""es"" class=""interlanguage-link-target"">Español</a></li><li class=""interlanguage-link interwiki-fr""><a href=""//minecraft-fr.gamepedia.com/Mod%C3%A8le:Blocs/contenu"" title=""Modèle:Blocs/contenu – français"" lang=""fr"" hreflang=""fr"" class=""interlanguage-link-target"">Français</a></li><li class=""interlanguage-link interwiki-it""><a href=""//minecraft-it.gamepedia.com/Template:Blocchi/content"" title=""Template:Blocchi/content – italiano"" lang=""it"" hreflang=""it"" class=""interlanguage-link-target"">Italiano</a></li><li class=""interlanguage-link interwiki-ja""><a href=""//minecraft-ja.gamepedia.com/%E3%83%86%E3%83%B3%E3%83%97%E3%83%AC%E3%83%BC%E3%83%88:Blocks/content"" title=""テンプレート:Blocks/content – 日本語"" lang=""ja"" hreflang=""ja"" class=""interlanguage-link-target"">日本語</a></li><li class=""interlanguage-link interwiki-ko""><a href=""//minecraft-ko.gamepedia.com/%ED%8B%80:blocks/content"" title=""틀:blocks/content – 한국어"" lang=""ko"" hreflang=""ko"" class=""interlanguage-link-target"">한국어</a></li><li class=""interlanguage-link interwiki-nl""><a href=""//minecraft-nl.gamepedia.com/Template:Blokken/inhoud"" title=""Template:Blokken/inhoud – Nederlands"" lang=""nl"" hreflang=""nl"" class=""interlanguage-link-target"">Nederlands</a></li><li class=""interlanguage-link interwiki-tr""><a href=""//minecraft-tr.gamepedia.com/%C5%9Eablon:Blocks/content"" title=""Şablon:Blocks/content – Türkçe"" lang=""tr"" hreflang=""tr"" class=""interlanguage-link-target"">Türkçe</a></li><li class=""interlanguage-link interwiki-pl""><a href=""//minecraft-pl.gamepedia.com/Template:Bloki/zawarto%C5%9B%C4%87"" title=""Template:Bloki/zawartość – polski"" lang=""pl"" hreflang=""pl"" class=""interlanguage-link-target"">Polski</a></li><li class=""interlanguage-link interwiki-ru""><a href=""//minecraft-ru.gamepedia.com/%D0%A8%D0%B0%D0%B1%D0%BB%D0%BE%D0%BD:%D0%91%D0%BB%D0%BE%D0%BA%D0%B8/%D0%A1%D0%BE%D0%B4%D0%B5%D1%80%D0%B6%D0%B8%D0%BC%D0%BE%D0%B5"" title=""Шаблон:Блоки/Содержимое – русский"" lang=""ru"" hreflang=""ru"" class=""interlanguage-link-target"">Русский</a></li><li class=""interlanguage-link interwiki-zh""><a href=""//minecraft-zh.gamepedia.com/Template:Blocks/content"" title=""Template:Blocks/content – 中文"" lang=""zh"" hreflang=""zh"" class=""interlanguage-link-target"">中文</a></li>				</ul>
							</div>
		</div>
			<div class=""portal"" role=""navigation"" id=""p-socialProfiles"" aria-labelledby=""p-socialProfiles-label"">
			<h3 id=""p-socialProfiles-label""></h3>
			<div class=""body"">
				<div class='socialSidebar'>
						<script type='text/javascript'>
							document.getElementById('p-socialProfiles').className = 'portal persistent';
						</script>
						<div class='socialLink twitter'><a href=""https://twitter.com/CurseGamepedia"" target=""_blank""><img src=""/extensions/Social/images/social/twitter.svg"" width=""32""/></a></div><div class='socialLink facebook'><a href=""https://www.facebook.com/CurseGamepedia"" target=""_blank""><img src=""/extensions/Social/images/social/facebook.svg"" width=""32""/></a></div><div class='socialLink twitch'><a href=""https://www.twitch.tv/gamepedia/videos"" target=""_blank""><img src=""/extensions/Social/images/social/twitch.svg"" width=""32""/></a></div>
					</div>			</div>
		</div>
					</div>
			</div>
						<div id=""footer"" role=""contentinfo"">
								<ul id=""footer-info"">
										<li id=""footer-info-lastmod""> This page was last edited on 8 September 2020, at 10:52.</li>
											<li id=""footer-info-copyright"">Content is available under <a class=""external"" rel=""nofollow"" href=""//creativecommons.org/licenses/by-nc-sa/3.0/"">CC BY-NC-SA 3.0</a> unless otherwise noted.<br/>Game content and materials are trademarks and copyrights of their respective publisher and its licensors.  All rights reserved.<br />
This site is a part of Fandom, Inc. and is not affiliated with the game publisher.</li>
										</ul>
									<ul id=""footer-places"">
										<li id=""footer-places-about""><a href=""/Minecraft_Wiki:About"" title=""Minecraft Wiki:About"">About Minecraft Wiki</a></li>
											<li id=""footer-places-disclaimer""><a href=""/Minecraft_Wiki:General_disclaimer"" title=""Minecraft Wiki:General disclaimer"">Disclaimers</a></li>
											<li id=""footer-places-mobileview""><a href=""https://minecraft.gamepedia.com/index.php?title=Template:Blocks/content&amp;mobileaction=toggle_view_mobile"" class=""noprint stopMobileRedirectToggle"">Mobile view</a></li>
										</ul>
														<ul id=""footer-icons"" class=""noprint"">
												<li id=""footer-copyrightico"">
							<a href=""//creativecommons.org/licenses/by-nc-sa/3.0/"" target=""_self""><img src=""//i.creativecommons.org/l/by-nc-sa/3.0/88x31.png"" alt=""CC BY-NC-SA 3.0"" width=""88"" height=""31""/></a>						</li>
													<li id=""footer-poweredbyico"">
							<a href=""//www.mediawiki.org/"" target=""_self""><img src=""/resources/assets/poweredby_mediawiki_88x31.png"" alt=""Powered by MediaWiki"" srcset=""/resources/assets/poweredby_mediawiki_132x47.png 1.5x, /resources/assets/poweredby_mediawiki_176x62.png 2x"" width=""88"" height=""31""/></a><a href=""https://help.gamepedia.com/What_is_Hydra"" target=""_self""><img src=""/skins/Hydra/images/icons/poweredbyhydra.png"" alt=""Powered by Hydra"" width=""88"" height=""31""/></a>						</li>
												</ul>
									<div style=""clear: both;""></div>
			</div>
		</div>
		<div id=""footer-push""></div>
	</div>
		
<script>(window.RLQ=window.RLQ||[]).push(function(){mw.config.set({""wgPageParseReport"":{""limitreport"":{""cputime"":""0.659"",""walltime"":""1.505"",""ppvisitednodes"":{""value"":6739,""limit"":1000000},""ppgeneratednodes"":{""value"":26026,""limit"":1000000},""postexpandincludesize"":{""value"":321948,""limit"":2097152},""templateargumentsize"":{""value"":375,""limit"":2097152},""expansiondepth"":{""value"":9,""limit"":40},""expensivefunctioncount"":{""value"":2,""limit"":99},""unstrip-depth"":{""value"":0,""limit"":20},""unstrip-size"":{""value"":0,""limit"":5000000},""timingprofile"":[""130.57% 1709.249    462 Template:BlockLink"",""100.00% 1309.021      1 -total"",""  3.92%   51.376     20 Template:BlockSprite"",""  2.22%   29.020      1 Template:ItemLink"",""  2.17%   28.387     11 Template:El"",""  0.28%    3.611      1 Template:SimpleNavbox""]},""loops"":{""limitreport-count-unlimited"":[0]},""scribunto"":{""limitreport-timeusage"":{""value"":""0.830"",""limit"":""7""},""limitreport-virtmemusage"":{""value"":9953280,""limit"":104857600},""limitreport-estmemusage"":0},""cachereport"":{""timestamp"":""20200916223106"",""ttl"":86400,""transientcontent"":false}}});});</script>
<link href=""https://fonts.googleapis.com/css?family=Rubik:400,700&amp;display=swap&amp;subset=cyrillic,latin-ext"" rel=""stylesheet"">
<div id=""footer-and-prefooter"">
	<div id=""gamepedia-footer"">
		<div class=""footer-wrapper-gp"">
			<div class=""footer-box footer-logo"">
				<a href=""https://www.gamepedia.com"">
					<img src=""/skins/Hydra/images/footer/premium-logo-light.svg"" class=""footer-gp-logo""/>
				</a>
			</div>
			<div class=""footer-box footer-social"">
				<ul class=""social"">
					<li>
						<a href=""https://www.facebook.com/CurseGamepedia"" title=""Facebook""><svg width=""10"" height=""21"" xmlns=""http://www.w3.org/2000/svg""><path d=""M9.364531 3.5096969H7.651507c-1.370419 0-1.598822.7261441-1.598822 1.6943364v2.2994565h3.311846l-.342605 3.6307209H6.281088v9.3188503H2.85504v-9.3188503H0V7.5034898h2.85504V4.8409612C2.85504 1.8153604 4.568064 0 7.080499 0c1.142016 0 2.16983.121024 2.512435.121024v3.3886729h-.228403z"" fill-rule=""evenodd""/></svg></a>
					</li>
					<li>
						<a href=""https://twitter.com/CurseGamepedia"" title=""Twitter""><svg width=""23"" height=""18"" xmlns=""http://www.w3.org/2000/svg""><path d=""M19.822727 4.2972696v.5729693c0 5.8729351-4.518416 12.6053241-12.826471 12.6053241-2.623596 0-4.955681-.7162116-6.996256-2.0053925.437266 0 .728777.1432423 1.166043.1432423 2.040575 0 4.081149-.7162116 5.684458-1.8621501-1.894819 0-3.643883-1.2891809-4.226905-3.151331.291511 0 .583022.1432423.874532.1432423.437266 0 .437266 0 1.020288-.1432423-2.186331-.429727-4.08115-2.2918772-4.08115-4.440512 0 .429727 1.603309.429727 2.332086.5729693C1.603309 5.8729351.874532 4.5837542.874532 3.0080887c0-.8594539.291511-1.5756655.728777-2.2918771 2.18633 2.7216041 5.684458 4.4405119 9.328342 4.7269965-.145756-.4297269-.145756-.7162116-.145756-1.0026962C10.785895 2.0053925 12.82647 0 15.304311 0c1.311798 0 2.477841.429727 3.352373 1.4324232 1.020287-.2864846 1.894819-.5729693 2.769351-1.1459386-.437266 1.1459386-1.166042 1.8621502-1.894819 2.4351195.874532-.1432423 1.894819-.429727 2.623596-.7162116-.728777.8594539-1.457553 1.7189078-2.332085 2.2918771z"" fill-rule=""evenodd""/></svg></a>
					</li>
					<li>
						<a href=""https://youtube.com/CurseEntertainment"" title=""Youtube""><svg width=""24"" height=""17"" xmlns=""http://www.w3.org/2000/svg""><path d=""M23.8 3.6s-.2-1.7-1-2.4c-.9-1-1.9-1-2.4-1C17 0 12 0 12 0S7 0 3.6.2c-.5.1-1.5.1-2.4 1-.7.7-1 2.4-1 2.4S0 5.5 0 7.5v1.8c0 1.9.2 3.9.2 3.9s.2 1.7 1 2.4c.9 1 2.1.9 2.6 1 1.9.2 8.2.2 8.2.2s5 0 8.4-.3c.5-.1 1.5-.1 2.4-1 .7-.7 1-2.4 1-2.4s.2-1.9.2-3.9V7.4c0-1.9-.2-3.8-.2-3.8zM9.5 11.5V4.8L16 8.2l-6.5 3.3z"" fill-rule=""evenodd""/></svg></a>
					</li>
				</ul>
			</div>
			<div class=""footer-box footer-links mobile-split"">
				<ul>
					<li>
						<a href=""https://support.gamepedia.com"">Support</a>
					</li>
					<li>
						<a href=""https://help.gamepedia.com/How_to_contact_Gamepedia"">Contact</a>
					</li>
					<li>
						<a href=""https://www.gamepedia.com/pro"">PRO</a>
					</li>
				</ul>
			</div>
		</div>
	</div>

	<footer id=""curse-footer"" role=""complimentary"" class=""show-ads"">
		<div class=""footer-wrapper"">
						<div class=""footer-box footer-ad"">
				<div class=""ad-placement ad-main-med-rect-footer"">
					<div id='cdm-zone-03'></div>				</div>
			</div>
						<div class=""footer-box footer-logo"">
				<a href=""https://www.fandom.com"" target=""_blank""><img src=""/skins/Hydra/images/footer/fandom-logo.svg"" /></a>
			</div>
			<div class=""footer-box footer-properties"">
				<h2>Explore Properties</h2>
				<ul class=""properties mobile-split"">
					<li><a href=""https://www.fandom.com"">Fandom</a></li>
					<li><a href=""https://www.gamepedia.com"">Gamepedia</a></li>
					<li><a href=""https://www.dndbeyond.com"">D&amp;D Beyond</a></li>
					<li><a href=""https://www.muthead.com"">Muthead</a></li>
					<li><a href=""https://www.futhead.com"">Futhead</a></li>
								</ul>
			</div>
			<div class=""footer-box footer-social"">
				<h2>Follow Us</h2>
				<ul class=""social"">
					<li><a href=""https://www.facebook.com/getfandom"" title=""Facebook""><svg class=""icon"" xmlns=""http://www.w3.org/2000/svg"" width=""11"" height=""24"" viewBox=""0 0 11 24""><path d=""M10.7 4L8.8 4C7.2 4 6.9 4.9 6.9 6L6.9 8.6 10.7 8.6 10.3 12.8 7.2 12.8 7.2 23.5 3.3 23.5 3.3 12.8 0 12.8 0 8.6 3.3 8.6 3.3 5.6C3.3 2.1 5.2 0 8.1 0 9.4 0 10.6 0.1 11 0.1L11 4 10.7 4Z""/></svg></a></li>
					<li><a href=""https://twitter.com/getfandom"" title=""Twitter""><svg width=""24"" height=""19"" xmlns=""http://www.w3.org/2000/svg""><path d=""M20.957 4.543v.606c0 6.209-4.777 13.327-13.56 13.327-2.774 0-5.24-.758-7.397-2.12.462 0 .77.15 1.233.15 2.157 0 4.314-.756 6.01-1.968-2.004 0-3.853-1.363-4.47-3.332.309 0 .617.152.925.152.463 0 .463 0 1.079-.152C2.466 10.752.462 8.783.462 6.512c0 .454 1.695.454 2.466.606C1.695 6.209.925 4.846.925 3.18c0-.908.308-1.666.77-2.423 2.311 2.878 6.01 4.695 9.862 4.998-.154-.455-.154-.758-.154-1.06C11.403 2.12 13.56 0 16.18 0c1.387 0 2.62.454 3.544 1.514 1.079-.302 2.004-.605 2.928-1.211-.462 1.211-1.233 1.969-2.003 2.574a14.785 14.785 0 0 0 2.774-.757c-.77.909-1.541 1.817-2.466 2.423z"" fill-rule=""evenodd""/></svg></a></li>
					<li><a href=""https://www.youtube.com/fandomentertainment"" title=""Youtube""><svg width=""24"" height=""17"" xmlns=""http://www.w3.org/2000/svg""><path d=""M23.8 3.6s-.2-1.7-1-2.4c-.9-1-1.9-1-2.4-1C17 0 12 0 12 0S7 0 3.6.2c-.5.1-1.5.1-2.4 1-.7.7-1 2.4-1 2.4S0 5.5 0 7.5v1.8c0 1.9.2 3.9.2 3.9s.2 1.7 1 2.4c.9 1 2.1.9 2.6 1 1.9.2 8.2.2 8.2.2s5 0 8.4-.3c.5-.1 1.5-.1 2.4-1 .7-.7 1-2.4 1-2.4s.2-1.9.2-3.9V7.4c0-1.9-.2-3.8-.2-3.8zM9.5 11.5V4.8L16 8.2l-6.5 3.3z"" fill-rule=""evenodd"" /></svg></a></li>
					<li><a href=""https://www.instagram.com/getfandom/"" title=""Instagram""><svg width=""20"" height=""20"" xmlns=""http://www.w3.org/2000/svg""><path d=""M17.510373 0H2.406639C1.078838 0 0 1.0788382 0 2.3236515v15.1867219c0 1.3278009 1.078838 2.406639 2.406639 2.406639h15.186722C18.921162 19.9170124 20 18.8381743 20 17.593361V2.3236515C19.917012 1.0788382 18.838174 0 17.510373 0zm-2.572614 2.4896266h1.659751c.497926 0 .829876.3319502.829876.8298755v1.659751c0 .4979253-.33195.8298755-.829876.8298755h-1.659751c-.497925 0-.829875-.3319502-.829875-.8298755v-1.659751c0-.4979253.33195-.8298755.829875-.8298755zM9.958506 6.1410788c2.074689 0 3.817428 1.7427386 3.817428 3.8174274s-1.742739 3.8174274-3.817428 3.8174274c-2.074689 0-3.817427-1.7427386-3.817427-3.8174274s1.742738-3.8174274 3.817427-3.8174274zm6.639004 11.2863071H3.319502c-.497925 0-.829875-.3319502-.829875-.8298755V8.2987552h1.659751c-.248963.9128631-.331951 1.9917012-.082988 2.9875519.497925 2.3236514 2.406639 4.1493775 4.73029 4.5643153 3.900415.746888 7.302905-2.1576763 7.302905-5.8921162 0-.5809128-.165975-1.1618257-.248963-1.659751h1.659751v8.2987552c-.082987.4979253-.414937.8298755-.912863.8298755z"" fill-rule=""evenodd"" /></svg></a></li>
					<li><a href=""https://www.linkedin.com/company/fandomwikia/"" title=""LinkedIn""><svg width=""19"" height=""19"" xmlns=""http://www.w3.org/2000/svg""><path d=""M3.859375 19h-3.5625V5.9375h3.5625V19zM2.078125 4.43175C.931 4.43175 0 3.493625 0 2.337S.931.24225 2.078125.24225 4.15625 1.180375 4.15625 2.337s-.929812 2.09475-2.078125 2.09475zM18.109375 19h-3.5625v-6.65475c0-3.9995-4.75-3.6966875-4.75 0V19h-3.5625V5.9375h3.5625v2.0959375c1.65775-3.070875 8.3125-3.2976875 8.3125 2.94025V19z"" fill-rule=""nonzero"" /></svg></a></li>
				</ul>
			</div>
			<div class=""footer-box footer-overview"">
				<h2>Overview</h2>
				<ul class=""mobile-split"">
					<li><a href=""https://www.fandom.com/about"">About</a></li>
					<li><a href=""https://www.fandom.com/careers"">Careers</a></li>
					<li><a href=""https://www.fandom.com/press"">Press</a></li>
					<li><a href=""https://www.fandom.com/about#contact"">Contact Us</a></li>
					<li><a href=""https://www.fandom.com/terms-of-use"">Terms of Use</a></li>
					<li><a href=""https://www.fandom.com/privacy-policy"">Privacy Policy</a></li>
				</ul>
			</div>
			<div class=""footer-box footer-community"">
				<h2>Community</h2>
				<ul class=""mobile-split"">
					<li><a href=""https://community.fandom.com/wiki/Community_Central"">Community Central</a></li>
					<li><a href=""https://fandom.zendesk.com/hc/en-us"">Support</a></li>
					<li><a href=""https://community.fandom.com/wiki/Help:Contents"">Help</a></li>
					<li><a href=""https://www.gamepedia.com/do-not-sell-my-info"">Do Not Sell My Info</a></li>
				</ul>
			</div>
			<div class=""footer-box footer-advertise"">
				<h2>Advertise</h2>
				<ul class=""mobile-split"">
					<li><a href=""https://www.fandom.com/mediakit"">Media Kit</a></li>
					<li><a href=""https://www.fandom.com/mediakit#contact"">Contact Us</a></li>
				</ul>
			</div>
		</div>
		<div class=""footer-post"">
			Minecraft Wiki is a Fandom Gaming Community.			<hr />
			<span class=""footer-post-mobile""><a href=""https://minecraft.gamepedia.com/index.php?title=Template:Blocks/content&amp;mobileaction=toggle_view_mobile"">View Mobile Site</a></span>
		</div>
	</footer>
</div>

<!-- Begin comScore -->
<script>
var _comscore = _comscore || [];
_comscore.push({ c1: ""2"", c2: ""6035118"" });
(function() {
    var s = document.createElement(""script""), el = document.getElementsByTagName(""script"")[0]; s.async = true;
    s.src = (document.location.protocol == ""https:"" ? ""https://sb"" : ""http://b"") + "".scorecardresearch.com/beacon.js"";
    el.parentNode.insertBefore(s, el);
})();
</script>
<noscript>
<img src=""https://sb.scorecardresearch.com/p?c1=2&c2=6035118&cv=2.0&cj=1"" alt=""Tracking Pixel""/>
</noscript>
<!-- End comScore -->

<!-- Begin Nielsen -->
<script type=""text/javascript"">
(function () {
	var d = new Image(1, 1);
	d.onerror = d.onload = function () {
		d.onerror = d.onload = null;
	};
	d.src = [""//secure-us.imrworldwide.com/cgi-bin/m?ci=us-603339h&cg=0&cc=1&si="", escape(window.location.href), ""&rp="", escape(document.referrer), ""&ts=compact&rnd="", (new Date()).getTime()].join('');
})();
</script>
<noscript>
<div><img src=""//secure-us.imrworldwide.com/cgi-bin/m?ci=us-603339h&cg=0&cc=1&ts=noscript"" width=""1"" height=""1"" alt="""" /></div>
</noscript>
<!-- End Nielsen --><div id='cdm-zone-end'></div><!-- Start PubMP -->
<script type=""text/javascript"" src=""//ads.pubmatic.com/AdServer/js/showad.js#PIX&ptask=DSP&SPug=1""></script> 
<!-- End PubMP -->
			<script type=""text/javascript"">
				window.genreCategory = 'Sandbox';
				window.wikiTags = [""building"",""crafting"",""game:minecraft"",""gathering"",""linux"",""mac"",""mining"",""mojang"",""online"",""pc"",""ps3"",""ps4"",""survival"",""xbox 360"",""xbox one""];
			</script>
<script>(window.RLQ=window.RLQ||[]).push(function(){mw.config.set({""wgBackendResponseTime"":1602});});</script>
	</body>
</html>
		
";

            var parseHTML = HtmlExtensions.Parse(longHTML);

			var enhanceWithSrc = "https://minecraft.gamepedia.com";

            parseHTML.InnerObjects.ForEach(x =>
            {
				x.RawTagText = x.RawTagText.Replace(@"src=""/", @"src=""" + enhanceWithSrc + "/");
				x.RawTagText = x.RawTagText.Replace(@"href=""/", @"href=""" + enhanceWithSrc + "/");
				x.RawTagText = x.RawTagText.Replace(@"url(/", @"url(" + enhanceWithSrc + "/");
				//url(/
				//x.RawTagText.Replace(@"Src=""/", @"Src=""" + enhanceWithSrc + "/");
				//            x.RawTagText.Replace(@"SRC=""/", @"SRC=""" + enhanceWithSrc + "/");
				//            x.RawTagText.Replace(@"Href=""/", @"Href=""" + enhanceWithSrc + "/");
				//            x.RawTagText.Replace(@"HREF=""/", @"HREF=""" + enhanceWithSrc + "/");
			});

            HtmlObject organizedHTML = HtmlExtensions.Organize(parseHTML);

            string reWriteHTML = HtmlExtensions.WriteHTML(organizedHTML);

            var htmlWithText = organizedHTML.WithText("bedrock").ToList();
            var htmlWhereDeep = organizedHTML.Children(x => x.Attributes.Where(y => y.Name.ToLower() == "href").Any(y => y.Value == "/Bedrock")).ToList();
        }

        [TestMethod]
        public void TestScript()
        {
            var scriptHTML = @"<html>

<script language=""Ohai"">
    var i = 100;
    var i2 = 200;
    var i3 = 300;
    var i4 = 400;
</script>

<script language=""Text/OrSomething"">
var something = ""</script> <script> </script>""
var something = '</script> <script> </script>'
var something = ""</script> <script> </script>"";
var something = '</script> <script> </script>""
var something = ""</script> <script> </script>';;
</script>

<div> Some random <span> nested stuff </span> </div>

Free Text!!!

<script language=""Second Script Area"">
var something = function() { /* Do Something Here */ };
// This is a comment
<!-- This is an HTML comment... in a script?  Lol -->
</script>

</body>
";
            var longResult = HtmlExtensions.Parse(scriptHTML);
        }

        [TestMethod]
        public void TestStyle()
        {

            var scriptHTML = @"

<style attr=""Do Header Styles have Attributes?  Lol"">
.something { kthx: ""#Value""; kthx2: ""#Value""; }
#something { 
	kthx: ""#/style""; 
	kthx2: ""#Value""; 
}
</style>

";
            var longResult = HtmlExtensions.Parse(scriptHTML);
        }
    }
}