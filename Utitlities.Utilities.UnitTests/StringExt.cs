﻿using JinnDev.Utilities.Utilities;
using JinnDev.Utilities.Utilities.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Utilities.Utilities.UnitTests
{
    [TestClass]
    public class StringExt
    {
        [TestMethod]
        public void IsChars_NormalStuff()
        {
            string nullString = null;
            Assert.AreEqual(true, nullString.IsChars(CharTypes.None));
            Assert.AreEqual(true, "".IsChars(CharTypes.None));
            Assert.AreEqual(true, string.Empty.IsChars(CharTypes.None));
            Assert.AreEqual(false, " ".IsChars(CharTypes.None));
            Assert.AreEqual(true, "asdf".IsChars(CharTypes.LowercaseAlpha));
            Assert.AreEqual(false, "aBsdf".IsChars(CharTypes.LowercaseAlpha));
            Assert.AreEqual(true, "ASDF".IsChars(CharTypes.UpercaseAlpha));
            Assert.AreEqual(false, "AbSDF".IsChars(CharTypes.UpercaseAlpha));
            Assert.AreEqual(true, "aSÔ".IsChars(CharTypes.Alpha));
            Assert.AreEqual(true, "\r\n \t".IsChars(CharTypes.Spaces));
            Assert.AreEqual(false, "\r\n \tXXX".IsChars(CharTypes.Spaces));
            Assert.AreEqual(true, "1234".IsChars(CharTypes.Numeric));
            Assert.AreEqual(false, "two".IsChars(CharTypes.Numeric));
            Assert.AreEqual(false, "12.34".IsChars(CharTypes.Numeric));
            Assert.AreEqual(true, "$1,332.24".IsChars(CharTypes.NumericPunctuated));
            Assert.AreEqual(true, @"'""'“”".IsChars(CharTypes.Quotes));
            Assert.AreEqual(false, @"Quote".IsChars(CharTypes.Quotes));
            Assert.AreEqual(true, "!!?".IsChars(CharTypes.Punctuation));
            Assert.AreEqual(true, "_-".IsChars(CharTypes.OtherPunctuation));
            Assert.AreEqual(false, "_-!".IsChars(CharTypes.OtherPunctuation));
            Assert.AreEqual(true, "*/+-x".IsChars(CharTypes.MathSymbols));
            Assert.AreEqual(false, "*\\+-".IsChars(CharTypes.MathSymbols));
            Assert.AreEqual(true, ")(".IsChars(CharTypes.Parenthesis));
            Assert.AreEqual(true, "][".IsChars(CharTypes.SquareBrackets));
            Assert.AreEqual(true, "><".IsChars(CharTypes.SharpBrackets));
            Assert.AreEqual(true, "}{".IsChars(CharTypes.CurlyBraces));
            Assert.AreEqual(true, ")(}{][><".IsChars(CharTypes.Brackets));

            Assert.AreEqual(true, "1a2bC4D".IsChars(CharTypes.AlphaNumeric));
            Assert.AreEqual(true, "aA\t12\v\r\n!!_?-*/".IsChars(CharTypes.Password));
            Assert.AreEqual(false, "aA\t12\v\r\n!!_?-*/'[".IsChars(CharTypes.Password));
            Assert.AreEqual(true, "aA\t12\v\r\n!!_?-*/'[".IsChars(CharTypes.All));
        }

        [TestMethod]
        public void UnquotedIndexOf_NormalString()
        {
            var expected = 22;
            var normalString = "This is the alphabet: abcdefghijklmno, lol jk, thats too much";
            var actual = normalString.UnquotedIndexOf("abcd");
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void UnquotedIndexOf_SneakyTick()
        {
            var expected = 26;
            var normalString = @"Steven's Alphabet: ijklmnoabcdefgh plus ""more"" lol";
            var actual = normalString.UnquotedIndexOf("abcd");
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void UnquotedIndexOf_QuotedString_OutsideDoubleQuotes()
        {
            var expected = 51;
            var normalString = @"Then he Murmured, ""Hello my friend"" so I lawld out my butt.";
            var actual = normalString.UnquotedIndexOf("my");
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void UnquotedIndexOf_QuotedString_StartIndex()
        {
            var expected = 51;
            var normalString = @"Then he Murmured, ""Hello my friend"" so I lawld out my butt.";
            var actual = normalString.UnquotedIndexOf("my", 40);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void UnquotedIndexOf_QuotedString_CustomDelimiters()
        {
            var expected = 53;
            var normalString = @"Then he Murmured, ""Hello #my friend"" so I lawld# out my butt.";
            var actual = normalString.UnquotedIndexOf("my", 0, '#');
            Assert.AreEqual(expected, actual);
        }
    }
}