﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;

namespace JinnDev.Utilities.Utilities.UnitTests
{
    [TestClass]
    public class MockObject
    {
        [TestMethod]
        public void DefaultFactory_NewObjectWithDefaultProperties()
        {
            var sut = new MockObject<MockableObject>();
            var mockObject = sut.GetMockObject();
            Assert.AreEqual(mockObject.Address, null);
        }

        [TestMethod]
        public void ImplementedFactory_NewObjectWithStaticProperties()
        {
            var sut = new MockableFactory();
            var mockObject = sut.GetMockObject();
            Assert.AreEqual(mockObject.FirstName, null);
            Assert.AreEqual(mockObject.Address, "12345 NW 67th Street");

            mockObject = sut.GetMockObject();
            Assert.AreEqual(mockObject.FirstName, null);
            Assert.AreEqual(mockObject.Address, "76543 NW 21st Street");

            mockObject = sut.GetMockObject();
            Assert.AreEqual(mockObject.FirstName, null);
            Assert.AreEqual(mockObject.Address, "12345 NW 67th Street");
        }

        [TestMethod]
        public void ImplementedFactory_NewObjectWithLambdaProperties()
        {
            var sut = new MockableFactory();
            var mockObject = sut.GetMockObject();
            Assert.AreEqual(mockObject.FirstName, null);
            Assert.AreEqual(mockObject.LastName, "Fletcher");

            mockObject = sut.GetMockObject();
            Assert.AreEqual(mockObject.FirstName, null);
            Assert.AreEqual(mockObject.LastName, "Morrell");

            mockObject = sut.GetMockObject();
            Assert.AreEqual(mockObject.FirstName, null);
            Assert.AreEqual(mockObject.LastName, "Carter");

            mockObject = sut.GetMockObject();
            Assert.AreEqual(mockObject.FirstName, null);
            Assert.AreEqual(mockObject.LastName, "Howard");

            mockObject = sut.GetMockObject();
            Assert.AreEqual(mockObject.FirstName, null);
            Assert.AreEqual(mockObject.LastName, "Fletcher");
        }
    }

    public class MockableFactory : MockObject<MockableObject>
    {
        private int _i;

        public override Func<object> GetLambdaForProperty(string propertyName)
        {
            switch (propertyName)
            {
                case nameof(MockableObject.LastName): return LastNameGenerator;
            }

            return null;
        }

        public override List<object> GetOptionsForProperty(string propertyName)
        {
            if (propertyName == nameof(MockableObject.Address))
                return new List<object> { "12345 NW 67th Street", "76543 NW 21st Street" };

            return null;
        }

        private object LastNameGenerator()
        {
            switch(_i)
            {
                case 0: _i++; return "Fletcher";
                case 1: _i++; return "Morrell";
                case 2: _i++; return "Carter";
                default: _i = 0; return "Howard";
            }
        }
    }

    public class MockableObject
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Address { get; set; }
        public DateTime BirthDate { get; set; }
        public int Age { get; set; }
    }
}