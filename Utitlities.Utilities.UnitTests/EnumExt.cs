﻿using JinnDev.Utilities.Utilities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.ComponentModel.DataAnnotations;

namespace JinnDev.Utilities.Utilities.UnitTests
{
    [TestClass]
    public class EnumExt
    {
        [TestMethod]
        public void PlainToString()
        {
            var enumText = TestEnum.ValueOne.ToString();
            Assert.AreEqual("ValueOne", enumText);
        }

        [TestMethod]
        public void ParseEnumName()
        {
            var enumText = TestEnum.ValueOne.EnumValueToString();
            Assert.AreEqual("Value One", enumText);
        }

        [TestMethod]
        public void GetEnumValues()
        {
            var enumValues = typeof(TestEnum).EnumList();
            Assert.AreEqual(2, enumValues.Count);
            Assert.AreEqual(10, enumValues[0].Key);
            Assert.AreEqual("Value One", enumValues[0].Value);
            Assert.AreEqual(20, enumValues[1].Key);
            Assert.AreEqual("Second Value", enumValues[1].Value);
        }
    }

    public enum TestEnum
    {
        ValueOne = 10,
        [Display(Name = "Second Value")]
        ValueTwo = 20
    }
}