﻿using JinnDev.Utilities.Utilities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Utilities.Utilities.UnitTests
{
    [TestClass]
    public class AsyncExt
    {
        [TestMethod]
        public async Task SelectAsync()
        {
            var lawl = new List<int> { 1, 2, 3, 4 };
            var result = (await lawl.SelectAsync(x => GetSomethingAsync(x))).ToList();
            Assert.AreEqual(lawl.Count, result.Count);
        }

        [TestMethod]
        public async Task SelectManyAsync()
        {
            var lawl = new List<List<int>> { new List<int> { 1, 2, 3, 4 }, new List<int> { 1, 2, 3, 4 } };
            var result = (await lawl.SelectManyAsync(x => x.SelectAsync(y => GetSomethingAsync(y)))).ToList();
            Assert.AreEqual(lawl.SelectMany(x => x).Count(), result.Count);
        }

        [TestMethod]
        public async Task WhereAsync()
        {
            var lawl = new List<int> { 1, 2, 3, 4 };
            var result = (await lawl.WhereAsync(x => IsEvenAsync(x))).ToList();
            Assert.AreEqual(lawl.Count / 2, result.Count);
        }

        [TestMethod]
        public async Task DictionarySelectAsync()
        {
            var lawl = new Dictionary<int, string> { { 1, "one" }, { 2, "two" }, { 3, "three" }, { 4, "four" } };
            var result = (await lawl.SelectAsync(x => GetSomethingAsync(x.Key))).ToList();
            Assert.AreEqual(lawl.Count, result.Count);
        }

        [TestMethod]
        public async Task DictionarySelectManyAsync()
        {
            var lawl = new List<Dictionary<int, string>> { 
                new Dictionary<int, string> { { 1, "one" }, { 2, "two" }, { 3, "three" }, { 4, "four" } },
                new Dictionary<int, string> { { 1, "one" }, { 2, "two" }, { 3, "three" }, { 4, "four" } } 
            };
            var result = (await lawl.SelectManyAsync(x => x.SelectAsync(y => GetSomethingAsync(y.Key)))).ToList();
            Assert.AreEqual(lawl.SelectMany(x => x).Count(), result.Count);
        }

        [TestMethod]
        public async Task DictionaryWhereAsync()
        {
            var lawl = new Dictionary<int, string> { { 1, "one" }, { 2, "two" }, { 3, "three" }, { 4, "four" } };
            var result = (await lawl.WhereAsync(x => IsEvenAsync(x.Key))).ToList();
            Assert.AreEqual(lawl.Count / 2, result.Count);
        }

        public Task<string> GetSomethingAsync(int input)
            => Task.FromResult(input.ToString());

        public Task<bool> IsEvenAsync(int input)
            => Task.FromResult(input % 2 == 0);
    }
}