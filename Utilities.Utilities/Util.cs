﻿using JinnDev.Utilities.Utilities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace JinnDev.Utilities.Utilities
{
    public static partial class Ext
    {
        public static string BOX { get => "allBoxedUp"; }

        public static int BoxDigitCount { get; set; } = 5;

        public static int BoxLen { get => BOX.Length + BoxDigitCount; }

        public static void AddOrUpdate<T>(this Dictionary<string, T> dct, string key, T value)
        {
            if (dct.ContainsKey(key)) dct[key] = value;
            else dct.Add(key, value);
        }

        public static void AddOrAppend<T>(this Dictionary<string, List<T>> dct, string key, T value)
        {
            if (dct.ContainsKey(key)) dct[key].Add(value);
            else dct.Add(key, new List<T> { value });
        }

        public static void AddOnce<T>(this Dictionary<string, T> dct, string key, T value)
        {
            if (!dct.ContainsKey(key)) dct.Add(key, value);
        }

        public static void AddOnce<T>(this List<T> lst, T value)
        {
            if (!lst.Contains(value)) lst.Add(value);
        }

        public static bool IsNullOrEmpty<T>(this IEnumerable<T> enumerable)
            => enumerable == null ? true : (enumerable as ICollection<T>) != null ? (enumerable as ICollection<T>).Count < 1 : !enumerable.Any();

        [Obsolete("Use GetOrDefault instead.")]
        public static U Get<T, U>(this Dictionary<T, U> dict, T key) where U : class
        {
            U val = default(U);
            var success = dict.TryGetValue(key, out val);
            if (success) return val;

            return val;
        }

        public static U GetOrDefault<T, U>(this Dictionary<T, U> dict, T key) where U : class
        {
            var success = dict.TryGetValue(key, out U val);
            if (success) return val;
            return default(U);
        }

        public static U GetOrDefault<T, U>(this Dictionary<T, U> dict, T key, U defaultVal)
        {
            var success = dict.TryGetValue(key, out U val);
            if (success) return val;
            return defaultVal;
        }

        private static void MakeGroupsSafe2(char openChar, char closeChar, ref string text, ref List<string> boxes)
        {
            if (!text.Contains(openChar))
            {
                return;
            }

            if (text.IndexOf("ÔÔ") >= 0)
            {
                throw new Exception("Can't use ÔÔ");
            }

            text = text.Replace("->", "ÔÔ");
            var safeString = text;
            var lastIndex = 0;
            while (true)
            {
                var oIndex = safeString.IndexOf(openChar, lastIndex);
                if (oIndex < 0)
                {
                    break;
                }

                var cIndex = safeString.IndexOf(closeChar, oIndex);
                if (cIndex < 0)
                {
                    break;
                }

                var tmpSafeString = safeString.Substring(oIndex + 1, cIndex - oIndex - 1);
                if (openChar == '<' && tmpSafeString.ContainsAnyI("&&", "||"))
                {
                    lastIndex = oIndex + 1;
                    continue;
                }

                var rgxParen = new Regex(RegexHelpers.RgxDynamic(openChar, closeChar) + @"(?<!\" + openChar + BOX + @"\d+\" + closeChar + @")");
                var parenArea = rgxParen.Match(safeString, lastIndex);
                var parenText = parenArea.Groups[0].ToString();
                if (string.IsNullOrWhiteSpace(parenText))
                {
                    break;
                }

                var parenLength = 1;
                if (!(openChar == '<' && parenText.ContainsAnyI("&&", "||")) && !string.IsNullOrWhiteSpace(parenText.Trim(openChar, closeChar)))
                {
                    var textText = text.Substring(parenArea.Index, parenArea.Length);
                    var replacement = openChar + BOX + boxes.Count.ToString().PadLeft(BoxDigitCount, '0') + closeChar;
                    boxes.Add(textText.Substring(1, textText.Length - 2).Replace("ÔÔ", "->"));
                    safeString = safeString.ReplaceFirstI(parenText, replacement);
                    text = text.ReplaceFirstI(textText, replacement);
                    parenLength = replacement.Length;
                }

                lastIndex = parenArea.Index + parenLength;
            }

            text = text.Replace("ÔÔ", "->");
        }
    }
}