﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace JinnDev.Utilities.Utilities
{
    public class MockObject<T> where T : new()
    {
        private Dictionary<string, int> PropertyStaticOrdinal { get; set; } = new Dictionary<string, int>();
        public Dictionary<string, List<object>> PropertyStaticOptions { get; set; } = new Dictionary<string, List<object>>();
        public Dictionary<string, Func<object>> PropertyLambdaOptions { get; set; } = new Dictionary<string, Func<object>>();

        public MockObject()
        {
            var properties = typeof(T).GetProperties();
            foreach (var prop in properties.Where(x => x.CanWrite))
            {
                if (PropertyStaticOptions.ContainsKey(prop.Name)) continue;
                if (PropertyLambdaOptions.ContainsKey(prop.Name)) continue;
                var staticOptions = GetOptionsForProperty(prop.Name);
                if (staticOptions != null && staticOptions.Count > 0)
                {
                    PropertyStaticOptions.Add(prop.Name, staticOptions);
                }
                else
                {
                    var lambdaOptions = GetLambdaForProperty(prop.Name);
                    if (lambdaOptions != null)
                        PropertyLambdaOptions.Add(prop.Name, lambdaOptions);
                }
            }
        }

        public virtual List<object> GetOptionsForProperty(string propertyName) => default;

        public virtual Func<object> GetLambdaForProperty(string propertyName) => default;

        public T GetMockObject(params object[] constructorParams)
        {
            T obj = default;
            if (constructorParams == null || constructorParams.Count() == 0)
                obj = new T();
            else
                obj = (T)Activator.CreateInstance(typeof(T), constructorParams);

            foreach (var prop in PropertyStaticOptions)
            {
                if (!PropertyStaticOrdinal.ContainsKey(prop.Key))
                    PropertyStaticOrdinal.Add(prop.Key, -1);
                var index = PropertyStaticOrdinal[prop.Key] + 1;
                if (index > prop.Value.Count - 1) index = 0;
                PropertyStaticOrdinal[prop.Key] = index;
                var randomSelection = prop.Value[index];
                var propInfo = obj.GetType().GetProperty(prop.Key, BindingFlags.Public | BindingFlags.Instance);
                propInfo.SetValue(obj, randomSelection, null);
            }
            foreach (var prop in PropertyLambdaOptions)
            {
                var randomSelection = prop.Value();
                var propInfo = obj.GetType().GetProperty(prop.Key, BindingFlags.Public | BindingFlags.Instance);
                propInfo.SetValue(obj, randomSelection, null);
            }
            return obj;
        }
    }
}