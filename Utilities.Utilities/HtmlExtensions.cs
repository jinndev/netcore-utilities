﻿using System.Collections.Generic;
using System.Linq;
using System;
using System.Text;

namespace JinnDev.Utilities.Utilities
{
    public static class HtmlExtensions
    {
        public static HtmlObject Parse(this string rawHTML)
        {
            var o = new StringParsingHolder(rawHTML);
            var allHtmlObjects = new List<HtmlObject>();
            var preText = o.Cased.Substring(0, o.Lower.IndexOf('<'));
            o.Splice(o.Lower.IndexOf('<'));

            while (o.Cased.Length > 0)
            {
                var workingSharp = o.Lower.IndexOf('<');
                if (workingSharp < 0) break;

                var innerPreText = o.Cased.Substring(0, workingSharp);
                //if (innerPreText.ToLower().Contains("bedrock")) System.Diagnostics.Debugger.Break();
                o.Splice(workingSharp);

                Tuple<string, List<string>> tag;
                if (o.Lower.StartsWith("<!--"))
                    tag = Tuple.Create(o.Cased.Substring(0, o.Lower.IndexOf("-->") + 3), new List<string>());
                else
                    tag = o.Cased.SaveQuotedAreas2(null, ">");

                var thisObj = ParseTag(tag.Item1, o.CurrentIndex, tag.Item2);
                thisObj.PreText = innerPreText;

                allHtmlObjects.Add(thisObj);
                o.Splice(thisObj.OuterHTML.Length - 4);

                if ((thisObj.IsScriptTag || thisObj.IsStyleTag) && thisObj.IsOpeningTag)
                {
                    var endTag = "</" + thisObj.TagName + ">";
                    var innerScript = o.Cased.SaveQuotedAreas2(null, "</" + thisObj.TagName + ">");
                    thisObj.InnerText = innerScript.Item1.Substring(0, innerScript.Item1.Length - endTag.Length).Unbox(innerScript.Item2);
                    var endObj = ParseTag(endTag, o.CurrentIndex + innerScript.Item1.Length, innerScript.Item2);
                    thisObj.ClosingTag = endObj;

                    allHtmlObjects.Add(endObj);
                    o.Splice(thisObj.InnerText.Length + endObj.OuterHTML.Length - 4);
                }
            }
            var result = new HtmlObject(0) { InnerObjects = allHtmlObjects, PreText = preText };//o.Cased
            if (!string.IsNullOrWhiteSpace(o.Cased)) System.Diagnostics.Debugger.Break();

            return result;
        }

        public static HtmlObject Organize(HtmlObject parseHTML)
        {
            var organized = new List<HtmlObject>();
            var openTagStack = new Stack<HtmlObject>();

            for (var i = 0; i < parseHTML.InnerObjects.Count; i++)
            {
                var obj = parseHTML.InnerObjects[i];
                if (openTagStack.Count == 0 && obj.IsClosingTag && !obj.IsOpeningTag) System.Diagnostics.Debugger.Break();
                if (obj.IsOpeningTag)
                {
                    if (obj.IsClosingTag)
                    {
                        if (openTagStack.Count > 0) AssignChild(openTagStack, obj);
                        else organized.Add(obj);
                    }
                    else
                    {
                        openTagStack.Push(obj);
                    }
                }
                else if (obj.IsClosingTag)
                {
                    while (openTagStack.Peek().TagName.ToLower() != obj.TagName.ToLower())
                    {
                        // unclosed tag found
                        if (openTagStack.Peek().TagName.ToLower() != "img" && openTagStack.Peek().TagName.ToLower() != "link")
                            System.Diagnostics.Debugger.Break();
                        var orphaned = openTagStack.Pop();

                        if (openTagStack.Count > 0) AssignChild(openTagStack, orphaned);
                        else organized.Add(orphaned);
                    }
                    if (openTagStack.Peek().ClosingTag != null && openTagStack.Peek().ClosingTag != obj)
                        System.Diagnostics.Debugger.Break();

                    if (openTagStack.Count > 0)
                    {
                        openTagStack.Peek().ClosingTag = obj;
                        openTagStack.Peek().InnerText += obj.PreText;
                        obj.PreText = null;
                        var popped = openTagStack.Pop();

                        if (openTagStack.Count > 0) AssignChild(openTagStack, popped);
                        else organized.Add(popped);
                    }
                    else System.Diagnostics.Debugger.Break();
                }
                else if (obj.IsCommentTag)
                {
                    if (openTagStack.Count > 0) AssignChild(openTagStack, obj);
                    else organized.Add(obj);
                }
                else
                {
                    System.Diagnostics.Debugger.Break();
                }
            }

            if (openTagStack.Count > 0) System.Diagnostics.Debugger.Break();

            var oldObjs = parseHTML.InnerObjects;
            parseHTML.InnerObjects = null;
            var result = parseHTML.Clone();
            parseHTML.InnerObjects = oldObjs;

            result.InnerObjects = organized;
            return result;
        }

        private static void AssignChild(Stack<HtmlObject> openTagStack, HtmlObject obj)
        {
            obj.Parent = openTagStack.Peek();
            obj.Parent.InnerObjects.Add(obj);
            if (!string.IsNullOrWhiteSpace(obj.PreText))
            {
                obj.Parent.InnerText += obj.PreText;
                obj.PreText = null;
            }
        }

        public static HtmlObject ParseTag(this string oneHtmlTag, int indexOnPage, List<string> boxes)
        {
            var obj = new HtmlObject(indexOnPage);
            obj.Attributes = new List<HtmlAttribute>();
            if (!oneHtmlTag.StartsWith("<") || !oneHtmlTag.EndsWith(">")) throw new Exception("Html Tags start with < and end with >... yours doesn't");
            if (oneHtmlTag.StartsWith("<!--"))
            {
                obj.RawTagText = oneHtmlTag;
                obj.IsCommentTag = true;
                return obj;
            }
            else if (oneHtmlTag.StartsWith("<script")) { }

            obj.RawTagText = boxes == null ? oneHtmlTag : oneHtmlTag.Unbox(boxes);
            oneHtmlTag = oneHtmlTag.Trim('<', '>', ' ');

            obj.IsOpeningTag = !oneHtmlTag.StartsWith("/");
            if (oneHtmlTag.EndsWith("/") || !obj.IsOpeningTag) obj.IsClosingTag = true;
            oneHtmlTag = oneHtmlTag.Trim('/', ' ');

            var attributes = "";
            if (oneHtmlTag.Contains(" "))
            {
                obj.TagName = oneHtmlTag.Substring(0, oneHtmlTag.IndexOf(" "));
                if (obj.TagName.StartsWith("!")) obj.IsClosingTag = true;
                attributes = oneHtmlTag.Substring(obj.TagName.Length + 1).Trim();
            }
            else
            {
                obj.TagName = oneHtmlTag;
            }

            if (!string.IsNullOrWhiteSpace(attributes))
            {
                obj.Attributes = ParseAttributes(attributes);
                obj.Attributes.ForEach(x => x.Parent = obj);
            }

            if (boxes != null)
            {
                obj.Attributes.ForEach(x => x.Value = x.Value.Unbox(boxes));
                obj.RawTagText = obj.RawTagText.Unbox(boxes);
            }

            return obj;
        }

        public static string WriteHTML(HtmlObject organizedHTML)
        {
            if (organizedHTML == null) return "";
            if (organizedHTML.InnerObjects.IsNullOrEmpty()) return organizedHTML.OuterHTML;

            var sb = new StringBuilder();
            var htmlStack = new Stack<HtmlObject>(organizedHTML.Clone().InnerObjects.AsEnumerable().Reverse());
            HtmlObject currentParent = null;
            var workingObj = htmlStack.Peek();
            while (workingObj != null)
            {
                if (!string.IsNullOrWhiteSpace(workingObj.PreText))
                {
                    sb.Append("\r\n" + workingObj.PreText.Trim(' ', '\r', '\n', '\t'));
                    workingObj.PreText = "";
                }
                sb.AppendLine("\r\n" + workingObj.RawTagText.Trim(' ', '\r', '\n', '\t') + "\r\n");
                workingObj.RawTagText = "";
                if (!workingObj.InnerObjects.IsNullOrEmpty())
                {
                    currentParent = workingObj;
                    workingObj.InnerObjects.AsEnumerable().Reverse().ToList().ForEach(x => htmlStack.Push(x));
                    workingObj.InnerObjects = null;
                    workingObj = htmlStack.Peek();
                }
                else
                {
                    sb.Append("\r\n" + workingObj.OuterHTML + "\r\n");
                    htmlStack.Pop();
                    if (htmlStack.Any()) workingObj = htmlStack.Peek();
                    else workingObj = null;
                }
            }

            return sb.ToString()
                .Replace("\r\n\r\n", "\r\n")
                .Replace("\r\n\r\n", "\r\n")
                .Replace("\r\n\r\n", "\r\n")
                .Replace("\r\n\r\n", "\r\n")
                .Replace("\r\n\r\n", "\r\n")
                .Replace("\r\n\r\n", "\r\n")
                .Replace("\r\n\r\n", "\r\n")
                .Replace("\r\n\r\n", "\r\n").Trim(' ', '\r', '\n', '\t');
        }

        public static List<HtmlAttribute> ParseAttributes(string htmlTagInnerAttributeContent)
        {
            var attrs = new List<HtmlAttribute>();
            HtmlAttribute attr = null;
            var beginNew = true;
            bool gettingName = false, gettingEquals = false, gettingValue = false;
            char? lastChar = null;
            foreach (var c in htmlTagInnerAttributeContent)
            {
                if (beginNew)
                {
                    if (c == ' ') continue;
                    attr = new HtmlAttribute();
                    beginNew = false;
                    gettingName = true;
                    gettingEquals = false;
                    gettingValue = false;

                    if (lastChar != null)
                    {
                        attr.Name += lastChar;
                        lastChar = null;
                    }
                }

                if (gettingName)
                {
                    if (c == ' ')
                    {
                        gettingName = false;
                        gettingEquals = true;
                    }
                    else if (c == '=')
                    {
                        gettingName = false;
                        gettingValue = true;
                    }
                    else if (char.IsLetter(c) || char.IsNumber(c) || c == '-')
                    {
                        attr.Name += c;
                    }
                    else if (c == '\'' || c == '"')
                    {
                        gettingName = false;
                        gettingValue = true;
                        attr.Delimiter = c;
                    }
                    else System.Diagnostics.Debugger.Break();
                }
                else if (gettingEquals)
                {
                    if (c == ' ') { }
                    else if (c == '=')
                    {
                        gettingEquals = false;
                        gettingValue = true;
                    }
                    else if (char.IsLetter(c) || char.IsNumber(c))
                    {
                        lastChar = c;
                        attr.Value = attr.Name;
                        beginNew = true;
                    }
                    else if (c == '\'' || c == '"')
                    {
                        gettingName = false;
                        gettingValue = true;
                        attr.Delimiter = c;
                    }
                    else System.Diagnostics.Debugger.Break();
                }
                else if (gettingValue)
                {
                    if (attr.Delimiter == null)
                    {
                        if (!string.IsNullOrWhiteSpace(attr.Value)) System.Diagnostics.Debugger.Break();
                        if (c == '\'' || c == '"')
                        {
                            attr.Delimiter = c;
                        }
                        else if (c == ' ') { }
                        else if (char.IsLetter(c) || char.IsNumber(c))
                        {
                            attr.Delimiter = '\0';
                            attr.Value += c;
                        }
                        else System.Diagnostics.Debugger.Break();
                    }
                    else
                    {
                        if (c == attr.Delimiter || (attr.Delimiter == '\0' && c == ' '))
                        {
                            beginNew = true;
                        }
                        else if (c != attr.Delimiter)
                        {
                            attr.Value += c;
                        }
                        else System.Diagnostics.Debugger.Break();
                    }
                }

                if (beginNew) attrs.Add(attr.Clone());
            }

            if (!beginNew)
            {
                if (string.IsNullOrWhiteSpace(attr.Value) && attr.Delimiter == null)
                    attr.Value = attr.Name;
                attrs.Add(attr.Clone());
            }

            return attrs;
        }

        public static IEnumerable<HtmlObject> WithText(this HtmlObject html, string withText)
        {
            //if (html.IndexOnPage == 13469) System.Diagnostics.Debugger.Break();
            if (html == null || html.InnerObjects.IsNullOrEmpty()) return Enumerable.Empty<HtmlObject>();
            var result = html.InnerObjects.Where(x => x.HasText(withText));
            var children = html.InnerObjects.SelectMany(x => x.WithText(withText));
            result = result.Concat(children);
            return result;
        }

        public static IEnumerable<HtmlObject> Children(this HtmlObject html, Func<HtmlObject, bool> predicate)
        {
            if (html == null || html.InnerObjects.IsNullOrEmpty()) return Enumerable.Empty<HtmlObject>();
            var result = html.InnerObjects.Where(predicate);
            var children = html.InnerObjects.SelectMany(x => x.Children(predicate));
            result = result.Concat(children);
            return result;
        }

        public static List<HtmlObject> Parents(this HtmlObject html, Func<HtmlObject, bool> predicate)
        {
            var e = new List<HtmlObject>();
            if (html == null || html.Parent == null) return e;
            if (predicate(html)) e.Add(html);
            e.AddRange(html.Parent.Parents(predicate));
            return e.ToList();
        }
    }
}