﻿using System;

namespace JinnDev.Utilities.Monad
{
    public class Maybe : MaybeBase
    {
        public static Maybe Success() => new Maybe { HasValue = true, };
        public static Maybe Success(string message) => new Maybe { HasValue = true, Message = message, };

        public static Maybe Failure() => new Maybe();
        public static Maybe Failure(string message) => new Maybe { Message = message, };
        public static Maybe Failure(Exception ex) => new Maybe { Exception = ex, Message = ex.Message };
        public static Maybe Failure(Exception ex, string message) => new Maybe { Exception = ex, Message = message };

        public static Maybe<T> Empty<T>() => new Maybe<T>();
        public static Maybe<T> Empty<T>(Exception ex) => new Maybe<T> { Exception = ex };
        public static Maybe<T> Empty<T>(string message) => new Maybe<T> { Message = message };
        public static Maybe<T> Empty<T>(Exception ex, string message) => new Maybe<T> { Exception = ex, Message = message };
    }
}