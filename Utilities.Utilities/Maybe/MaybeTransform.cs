﻿using System;

namespace JinnDev.Utilities.Monad
{
    public static class MaybeTransform
    {
        public static Maybe NonGeneric<T>(this Maybe<T> maybeT)
            => maybeT.HasValue ? Maybe.Success(maybeT.Message) : Maybe.Failure(maybeT.Exception, maybeT.Message);

        public static Maybe<T> AsGeneric<T>(this Maybe maybe)
            => maybe.HasValue ? new Maybe<T> { HasValue = true, Message = maybe.Message } : Maybe.Empty<T>(maybe.Exception, maybe.Message);

        public static Maybe<O> ChangeGeneric<I, O>(this Maybe<I> maybe, Func<I, O> converter)
        {
            var result = converter(maybe.Value).ToMaybe();
            result.Exception = maybe.Exception;
            result.Message = maybe.Message;
            result.HasValue = maybe.HasValue;
            return result;
        }

        public static Maybe<T> ToMaybe<T>(this T value) => new Maybe<T>(value);
        public static Maybe<T> ToMaybe<T>(this T value, string message) => new Maybe<T>(value, message);
    }
}