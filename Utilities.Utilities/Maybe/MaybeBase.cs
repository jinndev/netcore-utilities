﻿using System;

namespace JinnDev.Utilities.Monad
{
    public abstract class MaybeBase
    {
        private string _message;
        public Exception Exception { get; set; }
        public string Message
        {
            get
            {
                if (string.IsNullOrWhiteSpace(_message)) return Exception?.Message;
                return _message;
            }
            set => _message = value;
        }
        public bool IsExceptionState { get => Exception != null; }
        public bool HasValue { get; set; }
    }
}