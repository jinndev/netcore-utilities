﻿using System;
using System.Collections.Generic;

namespace JinnDev.Utilities.Utilities
{
    public class PagingModel
    {
        public int SkipRowCount { get; set; }
        public int TakePageSize { get; set; }
        public string FuzzySearchString { get; set; }
        public string OrderByColName { get; set; }
        public bool OrderByAscending { get; set; }
        public bool IncludeDeleted { get; set; }
        public List<KeyValuePair<string, string>> AdditionalParameters { get; set; }
    }

    public class PagedResult<T> : PagingModel where T : class
    {
        public int TotalRowCount { get; set; }

        public int CurrentPage { get => (SkipRowCount / TakePageSize) + 1; }
        public int TotalPageCount { get => (int)Math.Ceiling((double)TotalRowCount / TakePageSize); }
        public int FirstRowOnPage { get => ((CurrentPage - 1) * TakePageSize) + 1; }
        public int LastRowOnPage { get => Math.Min(CurrentPage * TakePageSize, TotalRowCount); }

        public IList<T> Results { get; set; }

        public PagedResult()
        {
            Results = new List<T>();
        }

        public static PagedResult<T> FromBase(PagingModel baseX)
        {
            return new PagedResult<T>
            {
                FuzzySearchString = baseX.FuzzySearchString,
                OrderByAscending = baseX.OrderByAscending,
                OrderByColName = baseX.OrderByColName,
                SkipRowCount = baseX.SkipRowCount,
                TakePageSize = baseX.TakePageSize,
                IncludeDeleted = baseX.IncludeDeleted,
                AdditionalParameters = baseX.AdditionalParameters
            };
        }
    }

    public class SortModel
    {
        public string ColName { get; set; }
        public bool Asc { get; set; } = true;
        public bool Desc { get => !Asc; set => Asc = !value; }
    }
}