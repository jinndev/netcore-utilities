﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace JinnDev.Utilities.Utilities
{
    public class RegexHelpers
    {
        public static string RegexSimpleParens { get => @"(?>\((?<inParens>[^)]*)\))"; }

        // c = comma, o = optional, r = required, m = multiple, lb = lookbehind
        public static string Comma { get => @"\s*,\s*"; }

        public static string RequiredCast { get => @"(?:\(\s*" + OptMultModifiers + @"\w+\s*\*?\s*\)\s*)"; }

        public static string OptionalCast { get => @"(?:\(\s*" + OptMultModifiers + @"\w+\s*\*?\s*\)\s*)?"; }

        public static string Terminator { get => @"\s*(?:;|$)"; }

        public static string DotArrow { get => @"(?:\s*(?:\.|::|->)\s*)"; }

        public static string OptionalPointer { get => @"(?:\*|&)*"; }

        public static string Ternary { get => @"^[^?]+\?[^:]+:"; }

        public static string MathOperation { get => @"(?:(?<!\+)\+(?!\+)|(?<!\-|\*)\-(?![->])|(?<!(?:[&/*^]|(?<![-+])[-+]|\^|\*|\()\s*)\*(?!\*|&|\))|\^|/|%|(?<![-&]\s*)\&(?!&))"; }

        public static string RequiredName { get => @"(?i)(?:(?<!\w)\w[\w\d]*(?![\w\d]))"; }

        public static string DoubleQuote { get => @"(?<!(?<!(?<!(?<!\\)\\)\\)\\)"""; }

        public static string SingleQuote { get => @"(?<!(?<!(?<!(?<!\\)\\)\\)\\)'"; }

        public static string AllQuotes { get => @"(?>" + DoubleQuote + @"([^""]*"")+?(?<=" + DoubleQuote + @")|" + SingleQuote + @"([^']*')+?(?<=" + SingleQuote + @"))"; }

        public static string ComparisonOperator { get => @"(?:==|!=|>=|\<=|(?<!-|\>|" + Ext.BOX + @"\d+)\>(?!\>)|(?<!\<)\<(?!\<|" + Ext.BOX + "))"; }

        public static string AssignmentOperators { get => @"(?:\s*[-^+|&/*]?=(?!=)(?<!(?:=|<|>|!)=)(?:\s*\+(?!\+))?\s*)"; }

        public static string ScopeResolution { get => @"(?:" + RequiredName + @"(?:" + DotArrow + RequiredName + @")*)"; }

        public static string OptionalMultIndex { get => @"(?:\s*\[\s*(?<indexValue>\s*(?:\d+|" + AllQuotes + @"|" + ScopeResolution + @"(?:\(\s*(?:" + Ext.BOX + @"\d+)?\s*\))?(?:\s*\+\+|\s*--)?)\s*[-+]?\s*)*\s*\]\s*)*"; }

        public static string OptionalCastIndex { get => @"(\(\s*\w+\s*\*?\s*\)\s*" + OptionalMultIndex + @"\s*)?"; }

        public static string CommaMultScopeResolve { get => @"(?:" + ScopeResolution + @"(?:" + Comma + ScopeResolution + @")*)"; }

        public static string AngleGenerics { get => @"(?:\s*\<" + CommaMultScopeResolve + @"\>\s*)"; }

        public static string OptionalGenerics { get => @"(?:" + AngleGenerics + @")?"; }

        public static string OptMultGenericScopeResolve { get => @"(?:" + ScopeResolution + OptionalGenerics + DotArrow + @"?)*"; }

        public static string OptionalDeprecatedNotation { get => @"(\s+_deprecated_attribute\d)?"; }

        public static string RequiredMethodName { get => @"(?xi)(?<methodFQDN>" + OptionalPointer + OptMultGenericScopeResolve + @"(?<methodName>" + ScopeResolution + OptionalGenerics + @")" + OptionalMultIndex + @")"; }

        public static string RequiredPropertyName { get => @"(?<propertyName>" + OptionalPointer + RequiredName + OptionalMultIndex + @")"; }

        public static string RequiredPropertyNames { get => @"(?i)(?<propertyNames>(?:" + RequiredMethodName + @")?(?:" + OptionalPointer + RequiredName + OptionalMultIndex + @"(?:" + DotArrow + OptionalPointer + RequiredName + OptionalMultIndex + @")*))"; }

        public static string RequiredMethodNameOrCall { get => RequiredMethodName + @"\s*" + RgxParens + @"?"; }

        public static string DigitQuoteOrMethod { get => @"\s*(?:0x\d+|-?\d+(?:\.\d+)?|\d*(?:\.\d+)+|" + AllQuotes + @"|" + RequiredPropertyNames + @"|" + RequiredMethodNameOrCall + @"(?:" + DotArrow + RequiredMethodNameOrCall + @")*)\s*"; }

        public static string LookBehindButNotTheseMethods { get => @"(?i)(?<!(?<!" + DotArrow + @")\b(switch|for|while|if|catch|goto)\b\s*)"; }

        public static string BooleanOperators
        {
            get
            {
                if (string.IsNullOrWhiteSpace(_w))
                {
                    _w = @"(?:" + string.Join("|", _wisers.Select(x => x.Replace("|", @"\|"))) + @")";
                }

                return _w;
            }
        }

        public static string OptMultModifiers
        {
            get
            {
                if (string.IsNullOrWhiteSpace(_omod))
                {
                    _omod = @"(?<modifier>(?:" + string.Join("|", _modifiers) + @")\s+)*";
                }

                return _omod;
            }
        }

        public static string OptMultSpecialTypes
        {
            get
            {
                if (string.IsNullOrWhiteSpace(_ost))
                {
                    _ost = @"(?:\*?(?:" + string.Join("|", _specials) + @")\s+)*";
                }

                return _ost;
            }
        }

        public static string RequiredReturnType
        {
            get
            {
                if (string.IsNullOrWhiteSpace(_rrt))
                {
                    _rrt = @"(?xi)(?<returnFQDN>(::)?(?:(?:(?:inline|short)\s+)?(?<returnType>long(?:\s+(?:int|long)+)?|(?:std::|system(::\w+)*)?(?:::dynamicarray|::staticarray|::smallstring|vector|map|set)" + AngleGenerics + @"(::iterator)?|" + OptionalPointer + ScopeResolution + @")" + OptionalPointer + @")\s+)(?<!(?:__property|" + string.Join("|", _specials) + @")\s+)";
                }

                return _rrt;
            }
        }

        private static List<string> _modifiers = new List<string> { "PACKAGE", "VID_WINDOW", "gbl", "gcpp_static", "gcpp_type", "dynamic", "static", "const(_vtbl)?", "unsigned", "lcl", "virtual", "clsid", "friend", "union", "out", "in", "enum", "struct" }; // <-- Yes, struct.  It is a modifier of sorts for a function's return type.

        private static List<string> _specials = new List<string> { "__RPC_USER", "__RPC_FAR", "DECLSPEC_DRECORD", "gcpp_proto", "GCPP_CLASS_TYPE", "gcpp_c?fun", "ctyp", "_rtlentry", "_expfunc", "pascal", "virtual", "_?cdecl", "__stdcall", "__fastcall" };

        private static List<string> _wisers = new List<string> { "&&", "||", "|", @"(?<!==\s*)&" };

        private static string _w;

        private static string _omod;

        private static string _ost;

        private static string _rrt;

        private static string _rgxc;

        private static string _rgxp;

        private static string _rgxTemplate = @"(?>(?<d>[left])(?(d)([right](?<-d>)|(?<d>[left])|[^leftright]+)+?(?(d)(?!))))";

        public static string RgxParens
        {
            get
            {
                if (string.IsNullOrWhiteSpace(_rgxp))
                {
                    _rgxp = RgxDynamic('(', ')');
                }

                return _rgxp;
            }
        }

        public static string RgxCurlys
        {
            get
            {
                if (string.IsNullOrWhiteSpace(_rgxc))
                {
                    _rgxc = RgxDynamic('{', '}');
                }

                return _rgxc;
            }
        }

        public static string RgxDynamic(char left, char right)
        {
            if (left == right)
            {
                throw new Exception("This is for opposing markers, like ( and ) or < and >, not for quotes or similar markers.");
            }

            var lStr = left.ToString();
            var rStr = right.ToString();
            if (lStr == "[")
            {
                lStr = "\\" + lStr;
                rStr = "\\" + rStr;
            }

            return _rgxTemplate.Replace("left", lStr).Replace("right", rStr);
        }
    }
}