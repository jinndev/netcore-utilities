﻿using JinnDev.Utilities.Monad;
using System.Threading.Tasks;

namespace JinnDev.Utilities.Utilities.Interfaces
{
    public interface ICrudDomain<T> where T : class
    {
        Task<Maybe<int>> CreateModel(T model);
        Task<Maybe<T>> ReadModel(int id);
        Task<Maybe<PagedResult<T>>> QueryAll(PagingModel pagingOptions);
        Task<Maybe> UpdateModel(T model);
        Task<Maybe> DeleteModel(int id);
    }
}