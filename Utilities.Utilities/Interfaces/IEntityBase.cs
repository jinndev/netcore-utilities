﻿using System;

namespace JinnDev.Utilities.Utilities.Interfaces
{
    public interface IEntityBase
    {
        int Id { get; set; }
        DateTime? CreatedDate { get; set; }
        DateTime? LastModified { get; set; }
        bool IsDeleted { get; set; }
    }
}