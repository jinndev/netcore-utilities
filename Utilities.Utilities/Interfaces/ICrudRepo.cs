﻿using JinnDev.Utilities.Monad;
using System.Threading.Tasks;

namespace JinnDev.Utilities.Utilities.Interfaces
{
    public interface ICrudRepo<T> where T : class, IEntityBase
    {
        Task<Maybe<int>> CreateEntity(T model);
        Task<Maybe<T>> ReadEntity(int id);
        Task<Maybe<PagedResult<T>>> QueryAll(PagingModel pagingOptions);
        Task<Maybe> UpdateEntity(T model);
        Task<Maybe> DeleteEntity(int id);
        Task<Maybe> HardDeleteEntity(int id);
    }
}