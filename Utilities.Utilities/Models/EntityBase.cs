﻿using JinnDev.Utilities.Utilities.Interfaces;
using System;

namespace JinnDev.Utilities.Utilities.Models
{
    public abstract class EntityBase<T> : IEntityBase where T : new()
    {
        public int Id { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? LastModified { get; set; }
        public bool IsDeleted { get; set; }

        protected abstract void Clone(ref T newModel, bool goDeep);

        public T DeepClone()
        {
            T clone = NewClone();
            Clone(ref clone, true);
            return clone;
        }

        public T ShallowClone()
        {
            var clone = NewClone();
            Clone(ref clone, false);
            return clone;
        }

        public T With(Action<T> assignment, bool goDeep = false)
        {
            var clone = NewClone();
            Clone(ref clone, goDeep);
            assignment?.Invoke(clone);
            return clone;
        }

        private T NewClone()
        {
            var clone = new T();
            ((IEntityBase)clone).Id = Id;
            ((IEntityBase)clone).IsDeleted = IsDeleted;
            ((IEntityBase)clone).LastModified = LastModified;
            ((IEntityBase)clone).CreatedDate = CreatedDate;
            return clone;
        }
    }
}