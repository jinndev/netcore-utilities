﻿namespace JinnDev.Utilities.Utilities.Models
{
    public abstract class DomainModel<T> where T : new()
    {
        public T DeepClone()
        {
            var clone = new T();
            Clone(ref clone, true);
            return clone;
        }

        public T ShallowClone()
        {
            var clone = new T();
            Clone(ref clone, false);
            return clone;
        }

        public T With(System.Action<T> assignment, bool goDeep = false)
        {
            var clone = new T();
            Clone(ref clone, goDeep);
            assignment?.Invoke(clone);
            return clone;
        }

        protected abstract void Clone(ref T newModel, bool goDeep);
    }
}