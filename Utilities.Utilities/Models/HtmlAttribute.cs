﻿namespace JinnDev.Utilities.Utilities
{
    public class HtmlAttribute
    {
        public string Name { get; set; } = "";
        public string Value { get; set; } = "";
        public char? Delimiter { get; set; } = null;
        public HtmlObject Parent { get; set; }

        public HtmlAttribute Clone()
        {
            return new HtmlAttribute
            {
                Name = Name,
                Value = Value,
                Delimiter = Delimiter
            };
        }
    }
}