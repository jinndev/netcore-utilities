﻿namespace JinnDev.Utilities.Utilities
{
    public class StringParsingHolder
    {
        public int CurrentIndex { get; set; }
        public string Original { get; set; }
        public string Cased { get; set; }
        public string Lower { get; set; }

        public StringParsingHolder(string rawHTML)
        {
            Original = rawHTML;
            Cased = rawHTML;
            Lower = rawHTML.ToLower();
        }

        public void Splice(int offFront)
        {
            CurrentIndex += offFront;
            Cased = Cased.Substring(offFront);
            Lower = Lower.Substring(offFront);
        }

        public override string ToString() => Cased;
    }
}