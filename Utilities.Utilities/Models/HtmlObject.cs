﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace JinnDev.Utilities.Utilities
{
    public class HtmlObject
    {
        public int IndexOnPage { get; set; }
        public string PreText { get; set; }
        public string TagName { get; set; } = "";
        public string RawTagText { get; set; } = "";
        public string InnerText { get; set; } = "";
        public string InnerPlainText { get; set; } = "";
        public bool IsOpeningTag { get; set; }
        public bool IsClosingTag { get; set; }
        public bool IsCommentTag { get; set; }
        public bool IsScriptTag { get => TagName.ToLower() == "script"; }
        public bool IsStyleTag { get => TagName.ToLower() == "style"; }
        public HtmlObject ClosingTag { get; set; }
        public HtmlObject Parent { get; set; }

        public List<HtmlAttribute> Attributes { get; set; }
        public List<HtmlObject> InnerObjects { get; set; } = new List<HtmlObject>();
        public string OuterHTML { get => RawTagText.Trim(' ', '\r', '\n', '\t') + "\r\n" + InnerText.Trim(' ', '\r', '\n', '\t') + "\r\n" + (ClosingTag == null ? "" : ClosingTag.RawTagText).Trim(' ', '\r', '\n', '\t'); }


        public HtmlObject() { }

        public HtmlObject(int indexOnPage)
        {
            IndexOnPage = indexOnPage;
        }

        public bool HasText(string withText)
        {
            return OuterHTML.ToLower().Contains(withText.ToLower());
        }

        public HtmlObject Clone()
        {
            var ret = new HtmlObject(IndexOnPage)
            {
                TagName = TagName,
                IsOpeningTag = IsOpeningTag,
                IsClosingTag = IsClosingTag,
                IsCommentTag = IsCommentTag,
                InnerText = InnerText,
                RawTagText = RawTagText,
                PreText = PreText,
                IndexOnPage = IndexOnPage
            };

            if (Attributes != null) ret.Attributes = Attributes.Select(x => x.Clone()).ToList();
            if (InnerObjects != null) ret.InnerObjects = InnerObjects.Select(x => x.Clone()).ToList();
            if (ClosingTag != null) ret.ClosingTag = ClosingTag.Clone();

            return ret;
        }

        public override string ToString() => OuterHTML;
    }
}