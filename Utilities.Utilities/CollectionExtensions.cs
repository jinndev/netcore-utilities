﻿using System;
using System.Collections.Specialized;
using System.Linq;
using System.Reflection;

namespace JinnDev.Utilities.Utilities
{
    public static partial class Ext
    {
        [Obsolete("Please use ContainsKey() instead.")]
        public static bool Contains(this NameValueCollection nvc, string containsWhat)
            => nvc.AllKeys.Any(x => x.ToLower() == containsWhat.ToLower());

        public static bool ContainsKey(this NameValueCollection nvc, string key)
            => nvc.AllKeys.Any(x => x.ToLower() == key.ToLower());

        public static void AddOrUpdate(this NameValueCollection headers, string key, string value)
        {
            headers.GetType()
                .GetProperty("IsReadOnly", BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.IgnoreCase | BindingFlags.FlattenHierarchy)
                .SetValue(headers, false, null);

            if (headers.ContainsKey(key)) headers[key] = value;
            else headers.Add(key, value);
        }
    }
}