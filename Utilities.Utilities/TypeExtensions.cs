﻿using System;

namespace JinnDev.Utilities.Utilities
{
    public static partial class Ext
    {
        public static bool IsNumeric(this Type type)
        {
            if (type == null) throw new ArgumentNullException("type");

            switch (Type.GetTypeCode(type))
            {
                case TypeCode.Byte:
                case TypeCode.Decimal:
                case TypeCode.Double:
                case TypeCode.Int16:
                case TypeCode.Int32:
                case TypeCode.Int64:
                case TypeCode.SByte:
                case TypeCode.Single:
                case TypeCode.UInt16:
                case TypeCode.UInt32:
                case TypeCode.UInt64:
                    return true;
                case TypeCode.Object:
                    if (type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Nullable<>))
                        return Nullable.GetUnderlyingType(type).IsNumeric();
                    return false;
            }
            return false;
        }

        public static bool IsDate(this Type type)
        {
            if (type == null) return false;

            if (typeof(DateTimeOffset).IsAssignableFrom(type)
            || typeof(DateTime).IsAssignableFrom(type)
            || typeof(TimeSpan).IsAssignableFrom(type)) return true;

            if (type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Nullable<>))
                return Nullable.GetUnderlyingType(type).IsDate();

            return false;
        }
    }
}