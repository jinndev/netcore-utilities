﻿using JinnDev.Utilities.Monad;
using JinnDev.Utilities.Utilities.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JinnDev.Utilities.Utilities.Implementations
{
    public abstract class CrudService<T, U> : ICrudDomain<T> where U : class, IEntityBase where T : class
    {
        protected ICrudRepo<U> _crudRepo;

        public CrudService(ICrudRepo<U> repo)
        {
            _crudRepo = repo;
        }

        public Task<Maybe<int>> CreateModel(T model)
            => _crudRepo.CreateEntity(ToEntity(model));

        public async Task<Maybe<T>> ReadModel(int id)
            => (await _crudRepo.ReadEntity(id)).ChangeGeneric(x => ToModel(x));

        public async Task<Maybe<PagedResult<T>>> QueryAll(PagingModel pagingOptions)
        {
            var entityResult = await _crudRepo.QueryAll(pagingOptions);
            if (!entityResult.HasValue) return entityResult.NonGeneric().AsGeneric<PagedResult<T>>();

            return new PagedResult<T>
            {
                FuzzySearchString = entityResult.Value.FuzzySearchString,
                IncludeDeleted = entityResult.Value.IncludeDeleted,
                OrderByAscending = entityResult.Value.OrderByAscending,
                OrderByColName = entityResult.Value.OrderByColName,
                SkipRowCount = entityResult.Value.SkipRowCount,
                TakePageSize = entityResult.Value.TakePageSize,
                TotalRowCount = entityResult.Value.TotalRowCount,
                AdditionalParameters = entityResult.Value.AdditionalParameters,
                Results = entityResult.Value.Results.Select(x => ToModel(x)).ToList()
            }.ToMaybe();
        }

        public Task<Maybe> UpdateModel(T model)
            => _crudRepo.UpdateEntity(ToEntity(model));

        public Task<Maybe> DeleteModel(int id)
            => _crudRepo.DeleteEntity(id);

        public abstract T ToModel(U entity);

        public abstract U ToEntity(T model);
    }
}