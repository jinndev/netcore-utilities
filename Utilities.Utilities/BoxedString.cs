﻿using JinnDev.Utilities.Utilities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace JinnDev.Utilities.Utilities
{
    public static partial class Ext
    {
        /// <summary>
        /// Find the Index of a substring, but not counting instances within Quoted Areas.  Defaults to use Double Ticks and Single Ticks
        /// </summary>
        public static int UnquotedIndexOf(this string text, string value, int startIndex = 0, params char[] parms)
        {
            if (parms.Length == 0) parms = new char[] { '"', '\'' };
            var chars = parms.Distinct().ToList();
            if (string.IsNullOrWhiteSpace(text)
                || string.IsNullOrWhiteSpace(value)
                || startIndex + value.Length > text.Length) return -1;
            if (!text.Any(x => parms.Contains(x))) return text.IndexOf(value);

            int matchCount = -1;
            var inCharIdx = -1;
            for (var i = startIndex; i < text.Length; i++)
            {
                char thisChar = text[i];
                if (inCharIdx >= 0)
                {
                    if (thisChar == chars[inCharIdx]) inCharIdx = -1;
                }
                else if (chars.Any(x => x == thisChar)) inCharIdx = chars.FindIndex(x => x == thisChar);
                else if (inCharIdx < 0)
                {
                    var matchNextChar = TryMatchChar(thisChar, value, ref matchCount);
                    if (!matchNextChar && matchCount >= 0) matchCount = -1;
                    else if (matchCount + 1 == value.Length) return i - matchCount;
                }

                if (i == text.Length - 1 && inCharIdx >= 0)
                {
                    i = text.LastIndexOf(chars[inCharIdx]);
                    inCharIdx = -1;
                }
            }

            return -1;
        }

        private static bool TryMatchChar(char thisChar, string value, ref int matchCount)
        {
            if (matchCount + 2 > value.Length) System.Diagnostics.Debugger.Break();
            if (value[matchCount + 1] != thisChar) return false;
            matchCount++;
            return true;
        }

        /// <summary>
        /// Replaces text between quoted areas with Underscores
        /// </summary>
        public static string MakeQuotedAreasSafe(this string text)
        {
            int startIndex = 0;
            var replacements = new List<string>();
            if (!string.IsNullOrWhiteSpace(text) && (text.Contains(@"""") || text.Contains("'")))
            {
                while (true)
                {
                    if (startIndex > text.Length) break;

                    var quotedArea = Regex.Match(text.Substring(startIndex), RegexHelpers.AllQuotes);
                    var fixedArea = quotedArea.Groups[0].ToString();

                    if (string.IsNullOrWhiteSpace(fixedArea)) break;

                    var quoteType = fixedArea[0];

                    if (fixedArea.Length > 2 && !fixedArea.ContainsI(BOX))
                        text = text.ReplaceFirstI(quotedArea.Groups[0].ToString(), quoteType + string.Empty.PadLeft(fixedArea.Length - 2, '_') + quoteType, startIndex);

                    startIndex = quotedArea.Index + startIndex + fixedArea.Length;
                }
            }

            return text;
        }

        public static string Unbox(this string text, List<string> boxes)
        {
            text = text.Replace("cstr999", "c_str()");
            if (boxes == null || boxes.Count == 0 || text.IndexOf(BOX) < 0)
            {
                return text;
            }

            var whileCount = 0;
            while (true)
            {
                whileCount++;
                if (whileCount > 4000) break;

                text = text.Replace("cstr999", "c_str()");
                var boxIndex = text.IndexOf(BOX);
                if (boxIndex < 0)
                {
                    break;
                }

                var strBoxNum = text.Substring(boxIndex + BOX.Length, BoxDigitCount);
                var boxNum = int.Parse(strBoxNum);
                text = text.Replace(BOX + strBoxNum, boxes[boxNum]);
            }

            return text;
        }

        public static BoxedString PackBox(this string text, List<string> currentBoxes = null)
        {
            if (currentBoxes == null)
            {
                currentBoxes = new List<string>();
            }

            var originalText = text;
            var boxes = new List<string>();
            if (currentBoxes.Count > 0)
            {
                boxes.AddRange(currentBoxes);
            }

            int quoteBoxCount = 0;
            if (currentBoxes.Count == 0)
            {
                text = text.Replace(".c_str()", ".cstr999");
                var quoteSafe = text.SaveQuotedAreas(boxes);
                text = quoteSafe.Item1;
                boxes = quoteSafe.Item2;
                quoteBoxCount = quoteSafe.Item2.Count;
            }

            MakeGroupsSafe2('{', '}', ref text, ref boxes);
            MakeGroupsSafe2('(', ')', ref text, ref boxes);
            MakeGroupsSafe2('[', ']', ref text, ref boxes);
            MakeGroupsSafe2('<', '>', ref text, ref boxes);

            if (currentBoxes.Count > 0)
            {
                var updateIndex = boxes.IndexOf(originalText);
                if (updateIndex < 0)
                {
                    throw new Exception("wtf");
                }

                boxes[updateIndex] = text;
            }

            var packedTighter = new List<string>();
            packedTighter.AddRange(boxes);
            foreach (var item in boxes.Skip(quoteBoxCount + currentBoxes.Count))
            {
                var box = item.PackBox(packedTighter);
                packedTighter = box.Boxes;
            }

            boxes = packedTighter;

            return new BoxedString { OriginalString = originalText, CleanedString = text, Boxes = boxes };
        }

        public static string Peek(this BoxedString box, string stringToPeek)
        {
            stringToPeek = stringToPeek.Replace("cstr999", "c_str()");
            if (string.IsNullOrWhiteSpace(stringToPeek))
            {
                return string.Empty;
            }

            var boxNumIndexes = new Dictionary<int, string>();
            var boxNameIndexes = stringToPeek.IndexesOf(BOX);
            foreach (var boxNameIndex in boxNameIndexes)
            {
                var boxNumIndex = stringToPeek.Substring(boxNameIndex + BOX.Length, BoxDigitCount);
                var boxNum = int.Parse(boxNumIndex);
                boxNumIndexes.Add(boxNum, boxNumIndex);
            }

            foreach (var boxNumIndex in boxNumIndexes)
            {
                stringToPeek = stringToPeek.Replace(BOX + boxNumIndex.Value, box.Boxes[boxNumIndex.Key]);
            }

            return stringToPeek;
        }

        public static Tuple<string, List<string>> SaveQuotedAreas(this string text, List<string> currentBoxes = null)
        {
            int startIndex = 0;
            var replacements = new List<string>();
            if (currentBoxes != null && currentBoxes.Count > 0)
            {
                replacements.AddRange(currentBoxes);
            }

            if (!string.IsNullOrWhiteSpace(text) && (text.Contains(@"""") || text.Contains("'")))
            {
                while (true)
                {
                    if (startIndex > text.Length)
                    {
                        break;
                    }

                    var quotedArea = Regex.Match(text.Substring(startIndex), RegexHelpers.AllQuotes);
                    var fixedArea = quotedArea.Groups[0].ToString();
                    if (string.IsNullOrWhiteSpace(fixedArea))
                    {
                        break;
                    }

                    var fixedAreaLen = fixedArea.Length;
                    var quoteType = fixedArea[0];
                    if (fixedAreaLen > 2 && !fixedArea.ContainsI(BOX) && !string.IsNullOrWhiteSpace(fixedArea.Trim(quoteType)))
                    {
                        text = text.ReplaceFirstI(quotedArea.Groups[0].ToString(), quoteType + BOX + replacements.Count.ToString().PadLeft(BoxDigitCount, '0') + quoteType, startIndex);
                        replacements.Add(fixedArea.Substring(1, fixedAreaLen - 2));
                        fixedAreaLen = BoxLen + 2;
                    }

                    startIndex = quotedArea.Index + startIndex + fixedAreaLen;
                }
            }

            return new Tuple<string, List<string>>(text, replacements);
        }

        public static Tuple<string, List<string>> SaveQuotedAreas2(this string text, List<string> currentBoxes = null, string stopAtString = null, params char[] parms)
        {
            if (parms.Length == 0) parms = new char[] { '"', '\'' };
            var chars = parms.Distinct().ToList();
            if (string.IsNullOrWhiteSpace(text) || !text.Any(x => parms.Contains(x)))
            {
                if (string.IsNullOrWhiteSpace(stopAtString) || string.IsNullOrWhiteSpace(text))
                    return new Tuple<string, List<string>>(text, new List<string>());
                var len = text.IndexOf(stopAtString) + 1;
                return new Tuple<string, List<string>>(text.Substring(0, len), new List<string>());
            }

            currentBoxes = currentBoxes ?? new List<string>();
            var result = new StringBuilder();
            var tempQuotedResult = new StringBuilder();
            bool checkUpToString = !string.IsNullOrWhiteSpace(stopAtString);
            int matchCount = -1;
            var inCharIdx = -1;
            for (var i = 0; i < text.Length; i++)
            {
                char thisChar = text[i];
                if (inCharIdx >= 0)
                {
                    if (thisChar == chars[inCharIdx])
                    {
                        inCharIdx = -1;
                        var replacement = thisChar + BOX + currentBoxes.Count.ToString().PadLeft(BoxDigitCount, '0') + thisChar;
                        result.Append(replacement);
                        currentBoxes.Add(tempQuotedResult.ToString());
                        tempQuotedResult.Clear();
                    }
                    else tempQuotedResult.Append(thisChar);
                }
                else if (chars.Any(x => x == thisChar)) inCharIdx = chars.FindIndex(x => x == thisChar);
                else if (inCharIdx < 0)
                {
                    result.Append(thisChar);
                    if (checkUpToString)
                    {
                        var matchNextChar = TryMatchChar(thisChar, stopAtString, ref matchCount);
                        if (!matchNextChar && matchCount >= 0) matchCount = -1;
                        else if (matchCount + 1 == stopAtString.Length) break;
                    }
                }

                if (i == text.Length - 1 && inCharIdx >= 0)
                {
                    i = text.LastIndexOf(chars[inCharIdx]);
                    inCharIdx = -1;
                    tempQuotedResult.Clear();
                }
            }

            return new Tuple<string, List<string>>(result.ToString(), currentBoxes);
        }
    }
}